#!/bin/bash

echo NEW2

# Do the thing that Drone doesn't do automatically.
export PATH=$PATH:/home/ubuntu/.gem/ruby/1.9.1/bin
gem install --user-install scss-lint

# Install Bundler.
gem install bundler

# Install bower
npm install -g -q bower

# Do the actual project-specific bootstraping.
source ./bootstrap.sh
