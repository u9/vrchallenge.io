'use strict'

describe 'Service: Environment', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Environment = {}
  beforeEach inject (_Environment_) ->
    Environment = _Environment_

  it 'should do something', ->
    expect(!!Environment).toBe true
