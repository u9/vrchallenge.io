'use strict'

describe 'Service: Access', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Access = {}
  beforeEach inject (_Access_) ->
    Access = _Access_

  it 'should do something', ->
    expect(!!Access).toBe true
