'use strict'

describe 'Service: Version', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Version = {}
  beforeEach inject (_Version_) ->
    Version = _Version_


  it 'should do something', ->
    expect(!!Version).toBe true
