'use strict'

describe 'Service: Preloader', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Preloader = {}
  beforeEach inject (_Preloader_) ->
    Preloader = _Preloader_

  it 'should do something', ->
    expect(!!Preloader).toBe true
