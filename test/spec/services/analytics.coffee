'use strict'

describe 'Service: analytics', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Analytics = {}
  beforeEach inject (_Analytics_) ->
    Analytics = _Analytics_

  it 'should do something', ->
    expect(!!Analytics).toBe true
