'use strict'

describe 'Service: subscription', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Subscription = {}
  beforeEach inject (_Subscription_) ->
    Subscription = _Subscription_

  it 'should do something', ->
    expect(!!Subscription).toBe true
