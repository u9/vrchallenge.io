'use strict'

describe 'Service: Touch', ->

  # load the service's module
  beforeEach module 'vrchallengeioApp'

  # instantiate service
  Touch = {}
  beforeEach inject (_Touch_) ->
    Touch = _Touch_

  it 'should do something', ->
    expect(!!Touch).toBe true
