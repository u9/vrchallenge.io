'use strict'

describe 'Filter: partnerClass', ->

  # load the filter's module
  beforeEach module 'vrchallengeioApp'

  # initialize a new instance of the filter before each test
  partnerClass = {}
  beforeEach inject ($filter) ->
    partnerClass = $filter 'partnerClass'

  it 'should convert partner class with prefix set', ->
    text = '01_angularjs'
    expect(partnerClass text, 'h').toBe ('_partner_h' + text)

  it 'should not convert partner class without prefix set', ->
    text = '01_angularjs'
    expect(partnerClass text).toBe ('')
