'use strict'

describe 'Filter: trust', ->

  # load the filter's module
  beforeEach module 'vrchallengeioApp'

  # initialize a new instance of the filter before each test
  trust = {}
  beforeEach inject ($filter) ->
    trust = $filter 'trust'
