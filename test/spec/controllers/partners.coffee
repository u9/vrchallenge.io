'use strict'

describe 'Controller: PartnersCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  PartnersCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PartnersCtrl = $controller 'PartnersCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!PartnersCtrl).toBe true
