'use strict'

describe 'Controller: OffPiotrkowskaCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  OffPiotrkowskaCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    OffPiotrkowskaCtrl = $controller 'OffPiotrkowskaCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!OffPiotrkowskaCtrl).toBe true
