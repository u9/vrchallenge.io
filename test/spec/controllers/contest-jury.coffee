'use strict'

describe 'Controller: ContestJuryCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContestJuryCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContestJuryCtrl = $controller 'ContestJuryCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
