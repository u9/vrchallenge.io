'use strict'

describe 'Controller: ProgrammeCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ProgrammeCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ProgrammeCtrl = $controller 'ProgrammeCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ProgrammeCtrl).toBe true
