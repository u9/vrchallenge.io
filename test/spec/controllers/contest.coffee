'use strict'

describe 'Controller: ContestCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContestCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContestCtrl = $controller 'ContestCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ContestCtrl).toBe true
