'use strict'

describe 'Controller: AboutUnit9Ctrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  AboutUnit9Ctrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AboutUnit9Ctrl = $controller 'AboutUnit9Ctrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
