'use strict'

describe 'Controller: ContestPrizesCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContestPrizesCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContestPrizesCtrl = $controller 'ContestPrizesCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ContestPrizesCtrl).toBe true
