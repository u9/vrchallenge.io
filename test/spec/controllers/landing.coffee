'use strict'

describe 'Controller: LandingCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  LandingCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    LandingCtrl = $controller 'LandingCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!LandingCtrl).toBe true
