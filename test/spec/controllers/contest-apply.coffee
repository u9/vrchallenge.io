'use strict'

describe 'Controller: ContestApplyCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContestApplyCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContestApplyCtrl = $controller 'ContestApplyCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ContestApplyCtrl).toBe true
