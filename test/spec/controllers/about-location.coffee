'use strict'

describe 'Controller: AboutLocationCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  AboutLocationCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AboutLocationCtrl = $controller 'AboutLocationCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
