'use strict'

describe 'Controller: AboutFestivalCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  AboutFestivalCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AboutFestivalCtrl = $controller 'AboutFestivalCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
