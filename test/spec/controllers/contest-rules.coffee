'use strict'

describe 'Controller: ContestRulesCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContestRulesCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContestRulesCtrl = $controller 'ContestRulesCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ContestRulesCtrl).toBe true
