'use strict'

describe 'Controller: ContestWorksCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContestWorksCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContestWorksCtrl = $controller 'ContestWorksCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ContestWorksCtrl).toBe true
