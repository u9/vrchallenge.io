'use strict'

describe 'Controller: ContactCtrl', ->

  # load the controller's module
  beforeEach module 'vrchallengeioApp'

  ContactCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContactCtrl = $controller 'ContactCtrl', {
      $scope: scope
    }

  it 'should exist', ->
    expect(!!ContactCtrl).toBe true
