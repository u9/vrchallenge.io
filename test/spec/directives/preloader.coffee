'use strict'

describe 'Directive: preloader', ->

  # load the directive's module
  beforeEach module 'vrchallengeioApp'

  # override translation
  beforeEach module 'translateNoop'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  # TODO: currently not testable.
  # it 'should contain a section element', inject ($compile) ->
  #   element = angular.element '<div preloader></div>'
  #   element = $compile(element) scope
  #   scope.$digest()
  #   expect(element.find('section').length).not.toBe 0
