'use strict'

describe 'Directive: ngDynamicController', ->

  # load the directive's module
  beforeEach module 'vrchallengeioApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<ng-dynamic-controller></ng-dynamic-controller>'
    element = $compile(element) scope
    expect(!!element).toBe true
