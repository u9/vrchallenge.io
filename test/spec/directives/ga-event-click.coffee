'use strict'

describe 'Directive: gaEventClick', ->

  # load the directive's module
  beforeEach module 'vrchallengeioApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
