'use strict'

describe 'Directive: backgroundEffects', ->

  # load the directive's module
  beforeEach module 'vrchallengeioApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<div background-effects></div>'
    element = $compile(element) scope
    expect(!!element).toBe true
