/**
 * @author Maciej Zasada <maciej@unit9.com>
*/

var gulp = require('gulp'),
  gutil = require('gulp-util'),
  path = require('path'),
  runSequence = require('run-sequence'),
  wiredep = require('wiredep').stream,
  nodeUtils = require('./node-libs/utils.js'),
  pngquant = require('imagemin-pngquant'),
  $ = require('gulp-load-plugins')(),

  appConfig = {
    moduleName: 'vrchallengeioApp',
    tmp: '.tmp',
    app: require('./bower.json').appPath || 'app',
    dist: 'website'
  },
  nof = './gulpfile.js';  // file for gulp.src dummies.

gulp.task('clean', function () {
  return gulp.src([
      appConfig.tmp + '/*',
      appConfig.dist + '/*'
    ], {read: false})
    .pipe($.rimraf({force: true})).on('error', gutil.log);
});


gulp.task('version', function () {
  var version = nodeUtils.getVersion(),
    versionObjString = '# @@version-begin\n' +
    '    @info =\n' +
    '      major: ' + version.major + '\n' +
    '      minor: ' + version.minor + '\n' +
    '      patch: ' + version.patch + '\n' +
    '    @date = \'' + new Date().toString() + '\'\n' +
    '    # @@version-end';

  return gulp.src(appConfig.app + '/scripts/**/*.coffee')
    .pipe($.replace(/# @@version-begin[^]+# @@version-end/g, versionObjString))
    .pipe(gulp.dest(appConfig.app + '/scripts'));
});


gulp.task('html', function () {
  return gulp.src(appConfig.app + '/**/*.html')
    .pipe($.connect.reload());
});


gulp.task('coffee', function () {
  return gulp.src(appConfig.app + '/scripts/**/*.coffee')
    .pipe($.newer(appConfig.tmp + '/scripts'))
    .pipe($.coffeelint())
    .pipe($.coffeelint.reporter())
    .pipe($.sourcemaps.init())
    .pipe($.coffee({

    }))
    .on('error', gutil.log)
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(appConfig.tmp + '/scripts'))
    .pipe($.connect.reload())
    // .pipe($.notify({
    //   title: 'Coffee',
    //   message: 'Compilation success',
    //   onLast: true
    // }));
});


gulp.task('compass', function () {
  return gulp.src(appConfig.app + '/styles/**/*.scss')
    .pipe($.newer(appConfig.tmp + '/styles'))
    //.pipe($.scssLint({'config': '.scsslintrc.yml'}))
    .pipe($.compass({
      project: path.resolve(appConfig.app),
      sass: 'styles',
      css: '../' + appConfig.tmp + '/styles',
      image: 'images',
      javascripts: 'scripts',
      font: 'styles/fonts',
      importPath: 'bower_components',
      http_path: '/',
      generated_images_path: 'images',
      relative: false,
      assetCacheBuster: false,
      raw: 'Sass::Script::Number.precision = 10\n',
      style: 'nested',
      sourcemap: true,
      debug: false,
      time: true,
      require: [
        './styles/base/listfiles.rb'
      ]
    }))
    .pipe($.sourcemaps.init())
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(appConfig.tmp + '/styles'))
    .pipe($.connect.reload())
    // .pipe($.notify({
    //   title: 'Compass',
    //   message: 'Compilation success',
    //   onLast: true
    // }));
});


gulp.task('scss-lint', function () {
  return gulp.src(appConfig.app + '/styles/**/*.scss')
    .pipe($.scssLint({'config': '.scsslintrc.yml'}))
    .pipe($.scssLint.failReporter());
});


gulp.task('nghtml2js-partials', function () {
  return gulp.src([appConfig.app + '/partials/**/*.html'])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.ngHtml2js({
      moduleName: appConfig.moduleName,
      prefix: 'partials/'
    }))
    .pipe($.concat('partials.js'))
    .pipe(gulp.dest(appConfig.tmp + '/scripts'))
    .pipe($.connect.reload());
});


gulp.task('nghtml2js-views', function () {
  return gulp.src([appConfig.app + '/views/**/*.html'])
  .pipe($.minifyHtml({
    empty: true,
    spare: true,
    quotes: true
  }))
  .pipe($.ngHtml2js({
    moduleName: appConfig.moduleName,
    prefix: 'views/'
  }))
  .pipe($.concat('views.js'))
  .pipe(gulp.dest(appConfig.tmp + '/scripts'))
  .pipe($.connect.reload());
});


gulp.task('nghtml2js', ['nghtml2js-partials', 'nghtml2js-views']);


gulp.task('compass-image-paths', function () {
  return gulp.src(appConfig.tmp + '/styles/**/*.css')
    .pipe($.replace(/\/images/g, '../images'))
    .pipe(gulp.dest(appConfig.tmp + '/styles'));
});


gulp.task('copy-languages', function () {
  return gulp.src(appConfig.app + '/languages/**/*.json')
    .pipe(gulp.dest(appConfig.dist + '/languages'));
});


gulp.task('copy-data', function () {
  return gulp.src(appConfig.app + '/data/**/*.json')
  .pipe(gulp.dest(appConfig.dist + '/data'));
});


gulp.task('copy-documents', function () {
  return gulp.src(appConfig.app + '/documents/**/*.*')
  .pipe(gulp.dest(appConfig.dist + '/documents'));
});


gulp.task('copy-sound', function () {
  return gulp.src(appConfig.app + '/sounds/**/*')
    .pipe(gulp.dest(appConfig.dist + '/sounds'));
});


gulp.task('copy-txt', function () {
  return gulp.src(appConfig.app + '/**/*.txt')
  .pipe(gulp.dest(appConfig.dist));
});


gulp.task('copy-fonts', function () {
  return gulp.src(appConfig.app + '/styles/fonts/**/*')
  .pipe(gulp.dest(appConfig.dist + '/styles/fonts'));
});


gulp.task('copy-htaccess', function () {
  return gulp.src(appConfig.app + '/.htaccess')
  .pipe(gulp.dest(appConfig.dist + '/'));
});


gulp.task('copy-favicon', function () {
  return gulp.src(appConfig.app + '/favicon.png')
  .pipe(gulp.dest(appConfig.dist + '/'));
});


gulp.task('copy', [
  'copy-languages',
  'copy-data',
  'copy-documents',
  'copy-txt',
  'copy-fonts',
  'copy-sound',
  'copy-htaccess',
  'copy-favicon'
]);


gulp.task('ngmin', function () {
  return gulp.src(appConfig.tmp + '/scripts/**/*.js')
    .pipe($.ngmin({dynamic: false}))
    .pipe(gulp.dest(appConfig.tmp + '/scripts'));
});


gulp.task('usemin', function () {
  return gulp.src(appConfig.app + '/*.html')
    .pipe($.usemin({
      // css: [$.minifyCss(), 'concat', $.rev()],
      // html: [$.minifyHtml({empty: true, spare: true, quotes: true})],
      // js: [$.uglify(), $.rev()],
      // options: {
      //   assetsDirs: [appConfig.dist, appConfig.dist + '/images']
      // }
    }))
    .pipe(gulp.dest(appConfig.dist));
});


gulp.task('imagemin', function () {
  return gulp.src(appConfig.app + '/images/**/*')
    .pipe($.newer(appConfig.dist + '/images'))
    .pipe($.imagemin({
      progressive: true,
      optimizationLevel: 3,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(appConfig.dist + '/images'));
});


gulp.task('optimise', ['usemin', 'imagemin']);


gulp.task('bump', function () {
  gulp.src('./*.json')
    .pipe($.bump({type:'minor'}))
    .pipe(gulp.dest('./'));
});


gulp.task('bump-patch', function () {
  gulp.src('./*.json')
  .pipe($.bump({type:'patch'}))
  .pipe(gulp.dest('./'));
});


gulp.task('bower', function () {
  return gulp.src(appConfig.app + '/index.html')
    .pipe(wiredep({
      ignorePath:  /\.\.\//,
      fileTypes: {
        sass: {
          ignorePath: /(\.\.\/){1,2}bower_components\//
        }
      }
    }))
    .pipe(gulp.dest(appConfig.app));
});


gulp.task('test-prepare', function () {
  return gulp.src('./test/**/*.coffee')
    .pipe($.newer(appConfig.tmp + '/test'))
    .pipe($.coffeelint())
    .pipe($.coffeelint.reporter())
    .pipe($.coffee({

    }))
    .on('error', gutil.log)
    .pipe(gulp.dest(appConfig.tmp + '/test'))
});


gulp.task('test', ['test-prepare', 'coffee'], function () {
  return gulp.src([
      'bower_components/jquery/dist/jquery.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/angular-animate/angular-animate.js',
      'bower_components/angular-cookies/angular-cookies.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-touch/angular-touch.js',
      'bower_components/angular-translate/angular-translate.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
      'bower_components/createjs-preloadjs/lib/preloadjs-0.6.0.combined.js',
      'libs/**/*.js',
      appConfig.tmp + '/scripts/app.js',
      appConfig.tmp + '/scripts/**/*.js',
      appConfig.tmp + '/test/**/*.js'
    ])
    .pipe($.karma({
      configFile: appConfig.tmp + '/test/karma.conf.js',
      action: 'run'
    }))
    .on('error', function(err) {
      // Make sure failed tests cause gulp to exit non-zero
      throw err;
    });
});


gulp.task('watch', function () {
  gulp.watch('bower.json', ['bower']);
  gulp.watch(appConfig.app + '/index.html', ['html']);
  gulp.watch(appConfig.app + '/partials/**/*.html', ['nghtml2js-partials']);
  gulp.watch(appConfig.app + '/views/**/*.html', ['nghtml2js-views']);
  gulp.watch(appConfig.app + '/scripts/**/*.coffee', ['coffee']);
  gulp.watch(appConfig.app + '/styles/**/*.scss', ['compass']);
  gulp.watch('./test/**/*.coffee', ['test']);
});


gulp.task('connect', function () {
  return $.connect.server({
    port: 9000,
    livereload: {
      port: 35729
    },
    middleware: function (connect, opt) {
      return [
        connect.static('.tmp'),
        connect().use(
          '/bower_components',
          connect.static('./bower_components')
        ),
        connect().use(
          '/libs',
          connect.static('./libs')
        ),
        require('connect-livereload')(),
        connect.static(appConfig.app)
      ];
    }
  });
});


gulp.task('open', function () {
  return gulp.src(nof).pipe($.open('', {url: 'http://localhost:9000'}));
});


gulp.task('build', function (cb) {
  runSequence(['bower', 'coffee', 'compass', 'nghtml2js'], 'compass-image-paths', 'test', 'version', 'coffee', cb);
});


gulp.task('build-bump', function (cb) {
  runSequence(['bower', 'coffee', 'compass', 'nghtml2js'], 'compass-image-paths', 'test', 'bump', 'version', 'coffee', cb);
});


gulp.task('build-bump-patch', function (cb) {
  runSequence(['bower', 'coffee', 'compass', 'nghtml2js'], 'compass-image-paths', 'test', 'bump-patch', 'version', 'coffee', cb);
});


gulp.task('serve', function (cb) {
  runSequence('clean', 'build', ['watch', 'connect'], 'open', cb);
});


gulp.task('default', function (cb) {
  runSequence('clean', 'build-bump', 'ngmin', 'copy', 'optimise', cb);
});


gulp.task('patch', function (cb) {
  runSequence('clean', 'build-bump-patch', 'ngmin', 'copy', 'optimise', cb);
});
