require 'logger'

module Sass::Script::Functions

    def listfiles(path, omitExtension)

        log = Logger.new('assets.log')
        log.level = Logger::DEBUG

        extensionOmitter = ''

        log.debug 'lookup dir: ' + File.dirname(__FILE__) + path.value

        if omitExtension.value
            extensionOmitter = '.*'
        end

        return Sass::Script::List.new(
            Dir.glob(File.dirname(__FILE__) + path.value).map! { |x| Sass::Script::String.new(File.basename(x, extensionOmitter)) },
            :comma
        )

    end

end
