'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:ngDynamicController
 # @description
 # # ngDynamicController
###
angular.module 'vrchallengeioApp'
  .directive 'ngDynamicController', ($compile) ->
    restrict: 'A'
    scope: true
    link: (scope, element, attrs) ->
      controllerName = scope.$eval attrs.ngDynamicController
      element.removeAttr 'ng-dynamic-controller'
      element.attr 'ng-controller', controllerName
      $compile(element)(scope)
