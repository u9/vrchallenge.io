'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:gaEventClick
 # @description
 # # gaEventClick
###
angular.module 'vrchallengeioApp'
  .directive 'gaEventClick', ->
    restrict: 'A'
    scope:
      'ga-category': '@'
      'ga-action': '@'
      'ga-label': '@'
      'ga-value': '@'
    link: (scope, element, attrs) ->

      console.log '************ ga', scope['ga-category'], scope['ga-action'],
        scope['ga-label'], scope['ga-value']

      element.$on 'click', ->
        console.log 'clicked!'

      return
