'use strict'

###*
 # @ngdoc directive
 # @name mobileApp.directive:preloader
 # @description
 # # preloader
###
angular.module('vrchallengeioApp')
  .directive('preloader', ($timeout, Preloader, Environment) ->

    MIN_LOADING_TIME = if Environment.name == 'local' then 0 else 3000
    HIDING_TIME = 1000

    templateUrl: 'partials/preloader.html'
    restrict: 'A'

    link: (scope, element, attrs) ->
      scope.preloaded = false
      scope.loaded = false
      loadingStartTime = new Date().getTime()

      Preloader.preloadInitial().then () ->
        scope.preloaded = true
        Preloader.loadMain().then () ->
          loadingTime = new Date().getTime() - loadingStartTime
          $timeout () ->
            scope.loaded = true
            $timeout () ->
              scope.hidden = true
              element.remove()
            , HIDING_TIME
          , Math.max(MIN_LOADING_TIME - loadingTime, 0)
          return
        return

      element
  )
