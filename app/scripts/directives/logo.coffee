'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:logo
 # @description
 # # logo
###
angular.module 'vrchallengeioApp'
  .directive 'logo', ($rootScope, $state) ->
    restrict: 'A'
    templateUrl: 'partials/logo.html'
    link: (scope, element, attrs) ->

      scope.logo =
        shown: false


      $rootScope.$on '$stateChangeSuccess', ->
        scope.logo.shown = $state.current.name != 'landing'


      return
