'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:vrcCheckbox
 # @description
 # # vrcCheckbox
###
angular.module 'vrchallengeioApp'
  .directive 'vrcCheckbox', ->
    restrict: 'A'
    templateUrl: 'partials/vrc-checkbox.html'
    scope:
      checked: '='
    link: (scope, element, attrs) ->

      scope.toggle = ->
        scope.checked = !scope.checked
        return

      return
