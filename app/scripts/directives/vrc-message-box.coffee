'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:vrcMessageBox
 # @description
 # # vrcMessageBox
###
angular.module 'vrchallengeioApp'
  .directive 'vrcMessageBox', ($location) ->
    restrict: 'A'
    templateUrl: 'partials/vrc-message-box.html'
    link: (scope, element, attrs) ->

      c = window.location.search.indexOf('action=confirm-contestant') != -1

      scope.messageBox =
        isOpen: c

      scope.close = ->
        console.log 'closing'
        scope.messageBox.isOpen = false
        $location.search 'action', null

      return
