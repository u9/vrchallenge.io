'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:backgroundEffects
 # @description
 # # backgroundEffects
###
angular.module 'vrchallengeioApp'
  .directive 'backgroundEffects', ($rootScope, $state) ->
    restrict: 'A'
    templateUrl: 'partials/background-effects.html'
    link: (scope, element, attrs) ->

      COLOR = 0x00ffbd
      DUST_PIXEL_SIZE = 3
      DUST_SIZE =
        x: 1
        y: 1
        z: 1
      DUST_AMOUNT = 1000
      DUST_OFFSET =
        x: 0
        y: 0
        z: -200
      DUST_SPREAD =
        x: 1000
        y: 300
        z: 600

      EASING =
        position: 0.0015
        rotation: 0.0015

      TO_DEGREES = Math.PI / 180

      MAX_PARALLAX_ROTATION =
        x: 5
        y: 5

      rendererMesh = null
      rendererDust = null
      cameraMesh = null
      cameraDust = null
      sceneMesh = null
      sceneDust = null
      sphere = null
      dustContainer = null
      lightMesh = null
      lightDust = null
      lastFrameTime = -1
      destPosition =
        x: 0
        y: 0
        z: 0
      destRotation =
        x: 0
        y: 0
        z: 0
      destParallaxRotation =
        x: 0
        y: 0
        z: 0


      init = ->
        initScene()
        initCamera()
        initRenderer()
        initLights()
        initModels()
        bindEvents()
        onResize()
        animate()
        return


      isWebGLSupported = ->
        try
          canvas = document.createElement 'canvas'
          test1 = !!window.WebGLRenderingContext
          test2 = !!canvas.getContext('webgl')
          test3 = !!canvas.getContext('experimental-webgl')
          return test1 && (test2 || test3)
        catch e
          return false
        return false


      initScene = ->
        sceneMesh = new THREE.Scene
        sceneDust = new THREE.Scene
        return


      initCamera = ->
        cameraMesh = new THREE.PerspectiveCamera
        cameraDust = new THREE.PerspectiveCamera
        sceneMesh.add cameraMesh
        sceneDust.add cameraDust
        cameraMesh.position.z = 300
        cameraDust.position.z = 300
        return


      initRenderer = ->
        if isWebGLSupported()
          rendererMesh = new THREE.WebGLRenderer {alpha: true,
          antialiasing: true}
          rendererDust = new THREE.WebGLRenderer {alpha: true,
          antialiasing: true}
        else
          rendererMesh = new THREE.CanvasRenderer {alpha: true,
          antialiasing: true}
          rendererDust = new THREE.CanvasRenderer {alpha: true,
          antialiasing: true}
        rendererMesh.setClearColor 0, 0
        rendererDust.setClearColor 0, 0
        element.append rendererDust.domElement
        element.append rendererMesh.domElement
        rendererMesh.domElement.style.position = 'absolute'
        rendererDust.domElement.style.position = 'absolute'
        return


      initLights = ->
        lightMesh = new THREE.PointLight 0xffffff, 1, 5000
        lightMesh.position.x = 0
        lightMesh.position.y = 0
        lightMesh.position.z = 500
        sceneMesh.add lightMesh
        lightDust = new THREE.PointLight 0xffffff, 1, 5000
        lightDust.position.x = 0
        lightDust.position.y = 0
        lightDust.position.z = 500
        sceneDust.add lightDust
        return


      initModels = ->
        # Mesh
        sphere = new THREE.Mesh(
          new THREE.SphereGeometry(200, 4, 4),
          new THREE.MeshLambertMaterial({
            wireframe: true
            wireframeLinewidth: 5
            color: COLOR
            emissive: 0x1f3b3b
          })
        )
        sphere.position.x = 0
        sphere.position.y = 0
        sceneMesh.add sphere

        # Dust
        dustContainer = new THREE.Object3D
        dustContainer.rotation.x = 45
        for i in [0..DUST_AMOUNT]
          dust = new THREE.Mesh(
            new THREE.BoxGeometry(DUST_SIZE.x, DUST_SIZE.y, DUST_SIZE.z),
            new THREE.MeshLambertMaterial({
              color: COLOR
              emissive: 0x1f3b3b
            })
          )
          dust.position.x = (Math.pow(Math.random(), 2) - 0.5) * DUST_SPREAD.x +
            DUST_OFFSET.x
          dust.position.y = (Math.pow(Math.random(), 2) - 0.5) * DUST_SPREAD.y +
            DUST_OFFSET.y
          dust.position.z = (Math.pow(Math.random(), 2) - 0.5) * DUST_SPREAD.z +
            DUST_OFFSET.z
          dustContainer.add dust
        sceneDust.add dustContainer
        return


      bindEvents = ->
        $rootScope.$on '$stateChangeSuccess', onStateChange
        window.addEventListener 'resize', onResize
        if 'ondeviceorientation' of window && 'ontouchstart' of window
          window.addEventListener 'deviceorientation', onDeviceOrientation
        else
          window.addEventListener 'mousemove', onMouseMove
        return


      updateMesh = (dt) ->
        sphere.position.x += (destPosition.x - sphere.position.x) *
        EASING.position * dt
        sphere.position.y += (destPosition.y - sphere.position.y) *
        EASING.position * dt
        sphere.position.z += (destPosition.z - sphere.position.z) *
        EASING.position * dt

        destRotationComplete =
          x: destRotation.x + destParallaxRotation.x
          y: destRotation.y + destParallaxRotation.y
          z: destRotation.z + destParallaxRotation.z

        sphere.rotation.x += (destRotationComplete.x * TO_DEGREES -
        sphere.rotation.x) * EASING.rotation * dt
        sphere.rotation.y += (destRotationComplete.y * TO_DEGREES -
        sphere.rotation.y) * EASING.rotation * dt
        sphere.rotation.z += (destRotationComplete.z * TO_DEGREES -
        sphere.rotation.z) * EASING.rotation * dt
        return


      updateDust = (dt) ->
        cameraDust.position.x = cameraMesh.position.x
        cameraDust.position.y = cameraMesh.position.y
        cameraDust.position.z = cameraMesh.position.z

        dustContainer.position.x = sphere.position.x
        dustContainer.position.y = sphere.position.y
        dustContainer.position.z = sphere.position.z

        dustContainer.rotation.x = sphere.rotation.x
        dustContainer.rotation.y = sphere.rotation.y
        dustContainer.rotation.z = sphere.rotation.z
        return


      update = (dt) ->
        updateMesh dt
        updateDust dt
        return


      render = ->
        rendererDust.render sceneDust, cameraDust
        rendererMesh.render sceneMesh, cameraMesh
        return


      animate = ->
        requestAnimationFrame animate
        now = if window.performance then window.performance.now()
        else new Date().getTime()
        update(now - lastFrameTime)
        render()
        lastFrameTime = now
        return


      onStateChange = ->
        destPosition.x = $state.current.data.backgroundEffects.position.x
        destPosition.y = $state.current.data.backgroundEffects.position.y
        destPosition.z = $state.current.data.backgroundEffects.position.z

        destRotation.x = $state.current.data.backgroundEffects.rotation.x
        destRotation.y = $state.current.data.backgroundEffects.rotation.y
        destRotation.z = $state.current.data.backgroundEffects.rotation.z
        return


      onMouseMove = (e) ->
        destParallaxRotation.x = MAX_PARALLAX_ROTATION.x * (e.pageY -
        window.innerHeight * 0.5) / (window.innerHeight * 0.5)
        destParallaxRotation.y = MAX_PARALLAX_ROTATION.y * (e.pageX -
        window.innerWidth * 0.5) / (window.innerWidth * 0.5)
        return


      onDeviceOrientation = (e) ->
        destParallaxRotation.x = MAX_PARALLAX_ROTATION.x * e.beta / 30
        destParallaxRotation.y = MAX_PARALLAX_ROTATION.y * e.gamma / 30
        return


      onResize = () ->
        width = window.innerWidth
        height = window.innerHeight
        rendererMesh.setSize width, height
        rendererDust.setSize width, height
        cameraMesh.aspect = width / height
        cameraMesh.updateProjectionMatrix()
        cameraDust.aspect = width / height
        cameraDust.updateProjectionMatrix()

        return


      init()

      return
