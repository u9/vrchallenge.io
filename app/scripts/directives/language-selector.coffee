'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:languageSelector
 # @description
 # # languageSelector
###
angular.module 'vrchallengeioApp'
  .directive 'languageSelector', ($translate, $interval) ->
    restrict: 'A'
    templateUrl: 'partials/language-selector.html'

    controller: ($scope, $element, $attrs, $transclude) ->
      changePromise = null

      $translate.use 'pl'

      $scope.languageSelector =
        current: $translate.use()
        languages: [
          {name: 'POL', code: 'pl'}
          {name: 'ENG', code: 'en'}
        ]

      $scope.selectLanguage = (code) ->
        $translate.use code
        $translate.use code
        $scope.languageSelector.current = $translate.use()
        return

      # Wait for the language to be loaded.
      if $scope.languageSelector.current == undefined
        changePromise = $interval ->
          if $translate.use() != undefined
            $scope.languageSelector.current = $translate.use()
            $interval.cancel changePromise
          return
        , 200

      return

    link: (scope, element, attrs) ->
      return
