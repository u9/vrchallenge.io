'use strict'

###*
 # @ngdoc directive
 # @name vrchallengeioApp.directive:menu
 # @description
 # # menu
###
angular.module 'vrchallengeioApp'
.directive 'menu', ($rootScope, $state, $timeout) ->
  restrict: 'A'
  replace: true
  templateUrl: 'partials/menu.html'
  link: (scope, element, attrs) ->

    HIGHLIGHT_TIME = 1000

    init = ->
      initScope()
      bindEvents()
      return


    initScope = ->
      scope.isOpen = false
      scope.menu =
        states: getMenuStates()
      scope.close = close
      scope.toggle = toggle
      scope.isActiveState = isActiveState
      return


    getMenuStates = ->
      allStates = $state.get()
      menuStates = []
      for state in allStates
        if state.name != '' && state.data.showInMenu
          menuStates.push state
      menuStates


    bindEvents = ->
      $rootScope.$on '$stateChangeStart', onStateChange
      scope.$on 'menu.highlight', highlight
      return

    isActiveState = (stateName) ->
      $state.includes stateName


    highlight = ->
      open()
      # $timeout close, HIGHLIGHT_TIME
      return


    open = ->
      scope.isOpen = true


    close = ->
      scope.isOpen = false


    toggle = ->
      scope.isOpen = !scope.isOpen
      false


    onStateChange = () ->
      close()
      false

    init()

    return
