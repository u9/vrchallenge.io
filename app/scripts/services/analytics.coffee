'use strict'

###*
 # @ngdoc service
 # @name vrchallengeioApp.analytics
 # @description
 # # analytics
 # Service in the vrchallengeioApp.
###
angular.module 'vrchallengeioApp'
  .service 'Analytics', ->

    GOOGLE_ACCOUNT_ID = 'UA-60369781-1'


    @init = ->
      ((i, s, o, g, r, a, m) ->
        i['GoogleAnalyticsObject'] = r
        i[r] = i[r] or ->
          (i[r].q = i[r].q or []).push arguments
          return

        i[r].l = 1 * new Date
        a = s.createElement(o)
        m = s.getElementsByTagName(o)[0]
        a.async = 1
        a.src = g
        m.parentNode.insertBefore a, m
        console.log 'Google Analytics initialised', GOOGLE_ACCOUNT_ID
        return
      ) window, document, 'script', '//www.google-analytics.com/analytics.js',
      'ga'
      ga 'create', GOOGLE_ACCOUNT_ID, 'auto'
      ga 'send', 'pageview'
      return


    @trackPageview = (page) ->
      ga 'send', 'pageview', page
      return


    @trackEvent = (category, action, label, value=1) ->
      ga 'send', 'event', category, action, label, value
      return


    @
