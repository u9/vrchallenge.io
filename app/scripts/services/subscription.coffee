'use strict'

###*
 # @ngdoc service
 # @name vrchallengeioApp.subscription
 # @description
 # # subscription
 # Service in the vrchallengeioApp.
###
angular.module 'vrchallengeioApp'
  .service 'Subscription', ($http) ->

    API_URL = 'http://vrchallenge.io/lists/?p=subscribe&id={ID}'

    TYPE_DATA =
      newsletter:
        formId: 1
        lists: [2]
      contestant:
        formId: 2
        lists: [2, 3]

    @TYPE_NEWSLETTER = 'newsletter'
    @TYPE_CONTESTANT = 'contestant'


    @make = (type, email) ->
      typeData = TYPE_DATA[type]
      if !typeData
        throw new Error 'invalid subscription type'

      email = email.toLowerCase()

      data =
        email: email
        htmlemail: 1
        VerificationCodeX: ''
        subscribe: 'Subscribe'

      for listId in typeData.lists
        console.log 'subscribing to list', listId
        data["list[#{listId}]"] = 'signup'

      console.log 'subscription data', data

      $http {
        method: 'POST'
        url: API_URL.replace '{ID}', typeData.formId
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        transformRequest: (obj) ->
          str = []
          for p of obj
            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
          return str.join('&')
        data: data
      }


    @
