'use strict'

###*
 # @ngdoc service
 # @name mobileApp.access
 # @description
 # # access
 # Service in the mobileApp.
###
angular.module('vrchallengeioApp')
  .service 'Access', ($q, $state, Environment) ->

    ###*
     * The state to go to if a requested state is not allowed.
     * @type {String}
    ###
    FALLBACK_STATE = 'landing'

    ###*
     * For some reason redirecting to a fallback state sometimes doesn't work.
     * We use a native browser redirect in such case.
     * @type {String}
    ###
    FALLBACK_URL = '/'

    ###*
     * Default allowed states.
     * @type {Array}
    ###
    DEFAULT_ALLOWED_STATES = [
      '*' # TODO: make the service more reliable. For now allow all routes.
    ]

    ###*
     * If true, new states will be automatically granted when using $state.go.
     * @type {Boolean}
    ###
    AUTO_GRANT_WHEN_STATE_GO = true

    ###*
     * List of granted states.
     * @type {Array}
    ###
    granted = DEFAULT_ALLOWED_STATES.concat []


    ###*
     * $state.go override.
    ###
    if AUTO_GRANT_WHEN_STATE_GO
      originalStateGo = $state.go
      self = @
      $state.go = (state) ->
        self.grant state
        originalStateGo.apply this, arguments


    ###*
     * Requires access to a specific state and returns a promise that will be
     * resolved based on whether the state is accessible or not.
     * @param  {String} state The state being required.
     * @return {Promise}      The promise to be resolved based on whether the
     * state is accessible or not.
    ###
    @require = (state) ->
      deferred = $q.defer()
      if @has state || state in DEFAULT_ALLOWED_STATES
        deferred.resolve()
      else
        deferred.reject()
        window.location.href = FALLBACK_URL
      deferred.promise


    ###*
     * Checks whether access to a specific step is allowed.
     * @param  {String}  state State to which access check is requested.
     * @return {Boolean}       [description]
    ###
    @has = (state) -> granted.indexOf(state) != -1 || granted.indexOf('*') != -1


    ###*
     * Grants access to a specific state.
     * @param  {String} state State to which the access is being granted.
    ###
    @grant = (state) ->
      if !@has state
        granted.push state
      return


    ###*
     * Restricts access to a specific state. If undefined, restricts access to
     * all states.
     * @param  {String} state State to which access is being restricted.
    ###
    @restrict = (state) ->
      if state
        index = granted.indexOf state
        if index != -1
          granted.splice index, 1
      else
        granted = []
      return


    ###*
     * Restricts allowed states to the default ones.
    ###
    @reset = () ->
      granted = DEFAULT_ALLOWED_STATES.concat []
      return

    @
