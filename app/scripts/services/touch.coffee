'use strict'

###*
 # @ngdoc service
 # @name mobileApp.touch
 # @description
 # # touch
 # Service in the mobileApp.
###
angular.module('vrchallengeioApp')
  .service 'Touch', ->

    @isTouch = () -> !!('ontouchstart' of window)

    @preventOverscroll = () ->
      $(window).on 'touchmove', (event) ->
        event.preventDefault()
        return
      return

    @
