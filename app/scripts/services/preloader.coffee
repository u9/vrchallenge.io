'use strict'

###*
 # @ngdoc service
 # @name mobileApp.preloader
 # @description
 # # preloader
 # Service in the mobileApp.
###
angular.module('vrchallengeioApp')
  .service 'Preloader', ($q) ->
    # Constants

    # Private
    loader = null
    isLoaded = false
    hasStartedLoading = false
    loadingPromise = null

    # Public
    @hasStartedLoading = () -> hasStartedLoading


    @isLoaded = () -> isLoaded


    @preloadInitial = () ->
      deferred = $q.defer()
      console.log 'preloading initial'
      queue = generateLoadQueue()
      queue.on 'complete', () ->
        console.log 'initial loaded'
        deferred.resolve()
        return
      queue.loadManifest generateInitialManifest()
      return deferred.promise


    @loadMain = () ->
      if hasStartedLoading
        return loadingPromise
      console.log 'loading main'
      hasStartedLoading = true
      deferred = $q.defer()
      queue = generateLoadQueue()
      queue.on 'complete', () ->
        console.log 'main loaded'
        isLoaded = true
        deferred.resolve()
        return
      try
        queue.loadManifest generateMainManifest()
      catch e
        console.log 'error', e
      loadingPromise = deferred.promise
      return loadingPromise


    generateLoadQueue = (manifest) ->
      queue = new createjs.LoadQueue true, '', true
      return queue


    generateInitialManifest = () ->
      getAssetUrls 'essential', [
        # 'some-jpeg-because-not-in-spritesheet.jpg'
      ]
      .concat [
        # getAssetsPackageUrl 'essential'
      ]


    generateMainManifest = () ->
      getAssetUrls 'main', [
        # 'some-jpeg-because-not-in-spritesheet.jpg'
      ]
      .concat [
        getAssetsPackageUrl 'main'
      ]


    getResolution = () ->
      return if $('.detection .display-retina').is(':visible') then '2x'
      else '1x'


    getPlatform = () ->
      if $('.detection .desktop').is(':visible')
        return 'desktop'
      else if $('.detection .tablet').is(':visible')
        return 'tablet'
      return 'mobile'


    getAssetUrls = (packageName, assetNames) ->
      return assetNames.map (assetName) ->
        return getAssetUrl packageName, assetName


    getAssetUrl = (packageName, assetName) ->
      return 'images/' + packageName + '/' + getPlatform() + '/' + packageName +
      '-' + getPlatform() + '-' + getResolution() + '/' + assetName


    getAssetsPackageUrl = (packageName) ->
      self = @
      rule = null
      mapUrl = null
      maxj = null
      for i in [0 .. document.styleSheets.length]
        if mapUrl
          break
        if document.styleSheets[i]
          if document.styleSheets[i].cssRules
            maxj = document.styleSheets[i].cssRules.length
          else
            maxj = document.styleSheets[i].rules.length
          for j in [0 .. maxj]
            if document.styleSheets[i].cssRules
              rule = document.styleSheets[i].cssRules[j]
            else
              rule = document.styleSheets[i].rules[j]
            if rule && rule.selectorText == '.detection .url-' + packageName
              if rule.cssText
                mapUrl = rule.cssText.match('url(.*)')[0]
              else
                mapUrl = rule.style.content
              mapUrl = mapUrl.substring(mapUrl.indexOf('img/'),
              mapUrl.indexOf(')')).replace('\'', '').replace('"', '')
              break
      match = if mapUrl then mapUrl.match(/url\((.+)/) else null
      if match
        mapUrl = match[1]
        if mapUrl.indexOf('..') == 0
          mapUrl = mapUrl.substring(2)
        return mapUrl
      return null


    @ # Return service instance
