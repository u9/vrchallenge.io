'use strict'

###*
 # @ngdoc service
 # @name mobileApp.version
 # @description
 # # version
 # Service in the mobileApp.
###
angular.module('vrchallengeioApp')
  .service 'Version', ->

    # @@version-begin
    @info =
      major: 0
      minor: 55
      patch: 0
    @date = 'Mon Jun 01 2015 20:00:42 GMT+0000 (UTC)'
    # @@version-end

    @
