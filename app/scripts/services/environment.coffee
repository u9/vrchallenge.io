'use strict'

###*
 # @ngdoc service
 # @name mobileApp.environment
 # @description
 # # environment
 # Service in the mobileApp.
###
angular.module('vrchallengeioApp')
  .service 'Environment', ->

    name = 'prod'
    switch window.location.hostname
      when 'localhost' then name = 'local'
      when '0.0.0.0' then name = 'local'
      when '127.0.0.1' then name = 'local'

    @name = name

    @isLocal = () -> @name == 'local'

    @
