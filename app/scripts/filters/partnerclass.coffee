'use strict'

###*
 # @ngdoc filter
 # @name vrchallengeioApp.filter:partnerClass
 # @function
 # @description
 # # partnerClass
 # Filter in the vrchallengeioApp.
###
angular.module('vrchallengeioApp')
  .filter 'partnerClass', ->

    GLOBAL_PREFIX = "_partner_"

    (input, prefix) ->

      if !prefix?
        return ""

      return "#{GLOBAL_PREFIX}#{prefix}#{input}"
