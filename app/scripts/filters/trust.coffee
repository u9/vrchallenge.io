'use strict'

###*
 # @ngdoc filter
 # @name vrchallengeioApp.filter:trust
 # @function
 # @description
 # # trust
 # Filter in the vrchallengeioApp.
###
angular.module 'vrchallengeioApp'
  .filter 'trust', ($sce) ->
    (input) ->
      $sce.trustAsHtml input
