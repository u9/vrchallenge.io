'use strict'

###*
 # @ngdoc overview
 # @name mobileApp
 # @description
 # # mobileApp
 #
 # Main module of the application.
###
app = angular.module('vrchallengeioApp', [
  'ngAnimate'
  'ngCookies'
  'ngSanitize'
  'ngTouch'
  'pascalprecht.translate'
  'ui.router'
])


app.config ($translateProvider, $urlRouterProvider, $stateProvider,
$locationProvider) ->

  # Set up translations.
  $translateProvider.preferredLanguage 'pl'
  $translateProvider.useStaticFilesLoader {
    prefix: 'languages/'
    suffix: '.json'
  }

  # Set up routes.
  isProd = window.location.hostname.indexOf('vrchallenge.io') != -1
  $locationProvider.html5Mode isProd
  $urlRouterProvider.otherwise '/'

  # Prevent certain routes.
  $urlRouterProvider.when '/about', '/about/festival'
  $urlRouterProvider.when '/contest', '/contest/rules'

  # Define routes.
  $stateProvider

    .state 'landing',
      url: '/'
      views:
        main:
          templateUrl: 'views/landing.html'
          controller: 'LandingCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'landing']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: true
        id: 0
        backgroundEffects:
          position:
            x: -80
            y: -100
            z: 0
          rotation:
            x: 10
            y: 20
            z: 10

    .state 'about',
      url: '/about'
      views:
        main:
          templateUrl: 'views/about.html'
          controller: 'AboutCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'about']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false
        backgroundEffects:
          position:
            x: 100
            y: 60
            z: -100
          rotation:
            x: 45
            y: 30
            z: 0

    .state 'about.festival',
      url: '/festival'
      views:
        about:
          templateUrl: 'views/about-festival.html'
          controller: 'AboutFestivalCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'about.festival']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        id: 1
        showInMenu: true

    .state 'about.unit9',
      url: '/unit9'
      views:
        about:
          templateUrl: 'views/about-unit9.html'
          controller: 'AboutUnit9Ctrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'about.unit9']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false

    .state 'about.location',
      url: '/location'
      views:
        about:
          templateUrl: 'views/about-location.html'
          controller: 'AboutLocationCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'about.location']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false

    .state 'contest',
      url: '/contest'
      views:
        main:
          templateUrl: 'views/contest.html'
          controller: 'ContestCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contest']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false
        backgroundEffects:
          position:
            x: 100
            y: 180
            z: -60
          rotation:
            x: -45
            y: -30
            z: 0

    .state 'contest.rules',
      url: '/rules'
      views:
        contest:
          templateUrl: 'views/contest-rules.html'
          controller: 'ContestRulesCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contest.rules']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        id: 2
        showInMenu: true

    .state 'contest.prizes',
      url: '/prizes'
      views:
        contest:
          templateUrl: 'views/contest-prizes.html'
          controller: 'ContestPrizesCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contest.prizes']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false

    .state 'contest.apply',
      url: '/apply'
      views:
        contest:
          templateUrl: 'views/contest-apply.html'
          controller: 'ContestApplyCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contest.apply']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false

    .state 'contest.works',
      url: '/works'
      views:
        contest:
          templateUrl: 'views/contest-works.html'
          controller: 'ContestWorksCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contest.works']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false

    .state 'contest.jury',
      url: '/jury'
      views:
        contest:
          templateUrl: 'views/contest-jury.html'
          controller: 'ContestJuryCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contest.jury']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: false

    .state 'programme',
      url: '/programme'
      views:
        main:
          templateUrl: 'views/programme.html'
          controller: 'ProgrammeCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'programme']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        id: 3
        showInMenu: true
        backgroundEffects:
          position:
            x: 0
            y: 0
            z: 0
          rotation:
            x: 10
            y: 0
            z: 0

    .state 'partners',
      url: '/partners'
      views:
        main:
          templateUrl: 'views/partners.html'
          controller: 'PartnersCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'partners']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        id: 4
        showInMenu: true
        backgroundEffects:
          position:
            x: -50
            y: 50
            z: -50
          rotation:
            x: -30
            y: 30
            z: 90

    .state 'contact',
      url: '/contact'
      views:
        main:
          templateUrl: 'views/contact.html'
          controller: 'ContactCtrl'
      resolve:
        access: ['Access', (Access) -> Access.require 'contact']
        preload: ['Preloader', (Preloader) -> Preloader.loadMain()]
      data:
        showInMenu: true
        id: 5
        backgroundEffects:
          position:
            x: -100
            y: -70
            z: 0
          rotation:
            x: 30
            y: -45
            z: -5


# When the app runs.
app.run ($rootScope, $timeout, $location,
         $window, $state, Version, Touch, Analytics) ->
  console.log 'Version: ', Version.info, 'Build date:', Version.date
  Analytics.init()
  # Touch.preventOverscroll()

  $rootScope.transition =
    isBackward: false

  $rootScope.$on '$stateChangeSuccess', ->
    path = $location.path()
    console.log '[GA] [pageview]', path
    Analytics.trackPageview path
    pageName = $location.path()
    console.log '[TODO] [GA]', JSON.stringify {pageName: pageName,
    event:'pageView'}
    # TODO: implement tracking as you wish.
    # $window.dataLayer.push?({pageName: pageName, event:'pageView'})

  $rootScope.$on '$stateChangeStart', (event, to, toParams, from) ->
    if !$rootScope.shouldChangeStateNow
      event.preventDefault()
      $rootScope.transition.isBackward =
        from.data? and to.data.id < from.data.id
      $rootScope.shouldChangeStateNow = true
      $timeout () ->
        $state.go to, {}, toParams
      , 100
    else
      $rootScope.shouldChangeStateNow = false
    true

