'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:AboutCtrl
 # @description
 # # AboutCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'AboutCtrl', ($scope) ->
