'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ContestJuryCtrl
 # @description
 # # ContestJuryCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ContestJuryCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
