'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ContestApplyCtrl
 # @description
 # # ContestApplyCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ContestApplyCtrl', ($scope, $timeout, Subscription) ->

    BLINK_TIMES = 3
    BLINK_TIME_ON = 200
    BLINK_TIME_OFF = 200

    $scope.user =
      email: undefined
      regulationsAccepted: false
      invalidEmail: false
      invalidAccept: false
      formSending: false
      formSent: false


    $scope.init = ->
      $scope.user.formSending = false
      $scope.user.formSent = false


    $scope.submit = (e) ->
      if $scope.user.formSending
        return

      if validate()
        $scope.user.formSending = true
        promise = Subscription.make Subscription.TYPE_CONTESTANT,
          $scope.user.email
        promise.then ->
          $scope.user.formSent = true
      return


    blink = (field, index = 1) ->
      if index > BLINK_TIMES
        return
      $scope.user[field] = true
      $timeout ->
        $scope.user[field] = false
      , BLINK_TIME_ON
      $timeout ->
        blink field, ++index
      , BLINK_TIME_ON + BLINK_TIME_OFF
      return


    validate = ->
      if !$scope.user.email
        blink 'invalidEmail'
      if !$scope.user.regulationsAccepted
        blink 'invalidAccept'
      !!$scope.user.email && $scope.user.regulationsAccepted
