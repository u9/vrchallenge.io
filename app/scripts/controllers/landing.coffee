'use strict'

###*
 # @ngdoc function
 # @name mobileApp.controller:CallCtrl
 # @description
 # # CallCtrl
 # Controller of the mobileApp
###
angular.module('vrchallengeioApp')
  .controller 'LandingCtrl', ($scope) ->

    $scope.highlightMenu = ->
      $scope.$emit 'menu.highlight'
      return
