'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ProgrammeCtrl
 # @description
 # # ProgrammeCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ProgrammeCtrl', ($scope) ->
