'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:AboutLocationCtrl
 # @description
 # # AboutLocationCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'AboutLocationCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
