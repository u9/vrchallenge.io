'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ContestRulesCtrl
 # @description
 # # ContestRulesCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ContestRulesCtrl', ($scope) ->
