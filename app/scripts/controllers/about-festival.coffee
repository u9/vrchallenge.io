'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:AboutFestivalCtrl
 # @description
 # # AboutFestivalCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'AboutFestivalCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
