'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ContactCtrl
 # @description
 # # ContactCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ContactCtrl', ($scope) ->
