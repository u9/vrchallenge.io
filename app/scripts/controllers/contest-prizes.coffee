'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ContestPrizesCtrl
 # @description
 # # ContestPrizesCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ContestPrizesCtrl', ($scope) ->
