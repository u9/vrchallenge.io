'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:PartnersCtrl
 # @description
 # # PartnersCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
.controller 'PartnersCtrl', ($scope) ->

  $scope.partners =

    honour: [
      '01_prezydent-miasta'
      '02_marszalek'
    ]

    media: [
      '01_gazeta'
      '02_tvp3'
      '09_eska'
      '08_mediarun'
      '04_pixel'
      '05_antyweb'
      '06_techplayer'
      '07_nowy-marketing'
      '03_mlodzi'
      '10_marketinglink'
    ]

    partners: [
      '01_politechnika',
      {
        name: '03_lodz-kreuje'
        url: "http://www.lodz.pl/"
      },
      '04_larr',
      '05_gamedev',
      '06_off',
      '07_dom',
      '08_derendarz'
    ]

    sponsors: [
      '04_microsoft'
      '02_vrizzmo'
      '01_techland'
      '03_businesslink'
    ]
