'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:ContestCtrl
 # @description
 # # ContestCtrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'ContestCtrl', ($scope) ->
