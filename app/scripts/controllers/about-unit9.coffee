'use strict'

###*
 # @ngdoc function
 # @name vrchallengeioApp.controller:AboutUnit9Ctrl
 # @description
 # # AboutUnit9Ctrl
 # Controller of the vrchallengeioApp
###
angular.module 'vrchallengeioApp'
  .controller 'AboutUnit9Ctrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
