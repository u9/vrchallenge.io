#!/bin/bash

# Update apt-get.
apt-get update

# Install dependencies.
apt-get install build-essential -y
apt-get install build-essential openssl libssl-dev curl -y
apt-get install git-core -y
apt-get install build-essential chrpath libssl-dev libxft-dev -y

# Install nvm
groupadd unit9
usermod -a -G unit9 vagrant
git clone https://github.com/creationix/nvm.git /opt/nvm
mkdir /usr/local/nvm
mkdir /usr/local/node
chown -R root:unit9 /usr/local/nvm
chmod -R 775 /usr/local/nvm
chown -R root:unit9 /usr/local/node
chmod -R 775 /usr/local/node
cat >/etc/profile.d/nvm.sh <<EOL
export NVM_DIR=/usr/local/nvm
source /opt/nvm/nvm.sh
nvm use 0.12
EOL
source /etc/profile.d/nvm.sh

# Install node.js via nvm
nvm install 0.12
nvm alias default stable
echo 'export PATH="./node_modules/.bin:$PATH"' >> /home/vagrant/.profile

# Install bower
npm install -g bower

# Install ruby.
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -L https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
rvm requirements
rvm install ruby
rvm use ruby --default
rvm rubygems current

# Install Bundler.
gem install bundler

# Do the actual project-specific bootstraping.
cd /vagrant
source ./bootstrap.sh
