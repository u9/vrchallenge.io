(function () {
  'use strict';
  /**
    * @ngdoc overview
    * @name mobileApp
    * @description
    * # mobileApp
    *
    * Main module of the application.
   */
  var app;
  app = angular.module('vrchallengeioApp', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngTouch',
    'pascalprecht.translate',
    'ui.router'
  ]);
  app.config([
    '$translateProvider',
    '$urlRouterProvider',
    '$stateProvider',
    '$locationProvider',
    function ($translateProvider, $urlRouterProvider, $stateProvider, $locationProvider) {
      var isProd;
      $translateProvider.preferredLanguage('pl');
      $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
      });
      isProd = window.location.hostname.indexOf('vrchallenge.io') !== -1;
      $locationProvider.html5Mode(isProd);
      $urlRouterProvider.otherwise('/');
      $urlRouterProvider.when('/about', '/about/festival');
      $urlRouterProvider.when('/contest', '/contest/rules');
      return $stateProvider.state('landing', {
        url: '/',
        views: {
          main: {
            templateUrl: 'views/landing.html',
            controller: 'LandingCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('landing');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          showInMenu: true,
          id: 0,
          backgroundEffects: {
            position: {
              x: -80,
              y: -100,
              z: 0
            },
            rotation: {
              x: 10,
              y: 20,
              z: 10
            }
          }
        }
      }).state('about', {
        url: '/about',
        views: {
          main: {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('about');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          showInMenu: false,
          backgroundEffects: {
            position: {
              x: 100,
              y: 60,
              z: -100
            },
            rotation: {
              x: 45,
              y: 30,
              z: 0
            }
          }
        }
      }).state('about.festival', {
        url: '/festival',
        views: {
          about: {
            templateUrl: 'views/about-festival.html',
            controller: 'AboutFestivalCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('about.festival');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          id: 1,
          showInMenu: true
        }
      }).state('about.unit9', {
        url: '/unit9',
        views: {
          about: {
            templateUrl: 'views/about-unit9.html',
            controller: 'AboutUnit9Ctrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('about.unit9');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: { showInMenu: false }
      }).state('about.location', {
        url: '/location',
        views: {
          about: {
            templateUrl: 'views/about-location.html',
            controller: 'AboutLocationCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('about.location');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: { showInMenu: false }
      }).state('contest', {
        url: '/contest',
        views: {
          main: {
            templateUrl: 'views/contest.html',
            controller: 'ContestCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contest');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          showInMenu: false,
          backgroundEffects: {
            position: {
              x: 100,
              y: 180,
              z: -60
            },
            rotation: {
              x: -45,
              y: -30,
              z: 0
            }
          }
        }
      }).state('contest.rules', {
        url: '/rules',
        views: {
          contest: {
            templateUrl: 'views/contest-rules.html',
            controller: 'ContestRulesCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contest.rules');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          id: 2,
          showInMenu: true
        }
      }).state('contest.prizes', {
        url: '/prizes',
        views: {
          contest: {
            templateUrl: 'views/contest-prizes.html',
            controller: 'ContestPrizesCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contest.prizes');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: { showInMenu: false }
      }).state('contest.apply', {
        url: '/apply',
        views: {
          contest: {
            templateUrl: 'views/contest-apply.html',
            controller: 'ContestApplyCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contest.apply');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: { showInMenu: false }
      }).state('contest.works', {
        url: '/works',
        views: {
          contest: {
            templateUrl: 'views/contest-works.html',
            controller: 'ContestWorksCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contest.works');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: { showInMenu: false }
      }).state('contest.jury', {
        url: '/jury',
        views: {
          contest: {
            templateUrl: 'views/contest-jury.html',
            controller: 'ContestJuryCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contest.jury');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: { showInMenu: false }
      }).state('programme', {
        url: '/programme',
        views: {
          main: {
            templateUrl: 'views/programme.html',
            controller: 'ProgrammeCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('programme');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          id: 3,
          showInMenu: true,
          backgroundEffects: {
            position: {
              x: 0,
              y: 0,
              z: 0
            },
            rotation: {
              x: 10,
              y: 0,
              z: 0
            }
          }
        }
      }).state('partners', {
        url: '/partners',
        views: {
          main: {
            templateUrl: 'views/partners.html',
            controller: 'PartnersCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('partners');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          id: 4,
          showInMenu: true,
          backgroundEffects: {
            position: {
              x: -50,
              y: 50,
              z: -50
            },
            rotation: {
              x: -30,
              y: 30,
              z: 90
            }
          }
        }
      }).state('contact', {
        url: '/contact',
        views: {
          main: {
            templateUrl: 'views/contact.html',
            controller: 'ContactCtrl'
          }
        },
        resolve: {
          access: [
            'Access',
            function (Access) {
              return Access.require('contact');
            }
          ],
          preload: [
            'Preloader',
            function (Preloader) {
              return Preloader.loadMain();
            }
          ]
        },
        data: {
          showInMenu: true,
          id: 5,
          backgroundEffects: {
            position: {
              x: -100,
              y: -70,
              z: 0
            },
            rotation: {
              x: 30,
              y: -45,
              z: -5
            }
          }
        }
      });
    }
  ]);
  app.run([
    '$rootScope',
    '$timeout',
    '$location',
    '$window',
    '$state',
    'Version',
    'Touch',
    'Analytics',
    function ($rootScope, $timeout, $location, $window, $state, Version, Touch, Analytics) {
      console.log('Version: ', Version.info, 'Build date:', Version.date);
      Analytics.init();
      $rootScope.transition = { isBackward: false };
      $rootScope.$on('$stateChangeSuccess', function () {
        var pageName, path;
        path = $location.path();
        console.log('[GA] [pageview]', path);
        Analytics.trackPageview(path);
        pageName = $location.path();
        return console.log('[TODO] [GA]', JSON.stringify({
          pageName: pageName,
          event: 'pageView'
        }));
      });
      return $rootScope.$on('$stateChangeStart', function (event, to, toParams, from) {
        if (!$rootScope.shouldChangeStateNow) {
          event.preventDefault();
          $rootScope.transition.isBackward = from.data != null && to.data.id < from.data.id;
          $rootScope.shouldChangeStateNow = true;
          $timeout(function () {
            return $state.go(to, {}, toParams);
          }, 100);
        } else {
          $rootScope.shouldChangeStateNow = false;
        }
        return true;
      });
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7Ozs7S0FGQTtBQUFBLE1BQUEsR0FBQTs7QUFBQSxFQVVBLEdBQUEsR0FBTSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLEVBQW1DLENBQ3ZDLFdBRHVDLEVBRXZDLFdBRnVDLEVBR3ZDLFlBSHVDLEVBSXZDLFNBSnVDLEVBS3ZDLHdCQUx1QyxFQU12QyxXQU51QyxDQUFuQyxDQVZOLENBQUE7O0FBQUEsRUFvQkEsR0FBRyxDQUFDLE1BQUosQ0FBVyxTQUFDLGtCQUFELEVBQXFCLGtCQUFyQixFQUF5QyxjQUF6QyxFQUNYLGlCQURXLEdBQUE7QUFJVCxRQUFBLE1BQUE7QUFBQSxJQUFBLGtCQUFrQixDQUFDLGlCQUFuQixDQUFxQyxJQUFyQyxDQUFBLENBQUE7QUFBQSxJQUNBLGtCQUFrQixDQUFDLG9CQUFuQixDQUF3QztBQUFBLE1BQ3RDLE1BQUEsRUFBUSxZQUQ4QjtBQUFBLE1BRXRDLE1BQUEsRUFBUSxPQUY4QjtLQUF4QyxDQURBLENBQUE7QUFBQSxJQU9BLE1BQUEsR0FBUyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUF6QixDQUFpQyxnQkFBakMsQ0FBQSxLQUFzRCxDQUFBLENBUC9ELENBQUE7QUFBQSxJQVFBLGlCQUFpQixDQUFDLFNBQWxCLENBQTRCLE1BQTVCLENBUkEsQ0FBQTtBQUFBLElBU0Esa0JBQWtCLENBQUMsU0FBbkIsQ0FBNkIsR0FBN0IsQ0FUQSxDQUFBO0FBQUEsSUFZQSxrQkFBa0IsQ0FBQyxJQUFuQixDQUF3QixRQUF4QixFQUFrQyxpQkFBbEMsQ0FaQSxDQUFBO0FBQUEsSUFhQSxrQkFBa0IsQ0FBQyxJQUFuQixDQUF3QixVQUF4QixFQUFvQyxnQkFBcEMsQ0FiQSxDQUFBO1dBZ0JBLGNBRUUsQ0FBQyxLQUZILENBRVMsU0FGVCxFQUdJO0FBQUEsTUFBQSxHQUFBLEVBQUssR0FBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxJQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSxvQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGFBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLFNBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLFVBQUEsRUFBWSxJQUFaO0FBQUEsUUFDQSxFQUFBLEVBQUksQ0FESjtBQUFBLFFBRUEsaUJBQUEsRUFDRTtBQUFBLFVBQUEsUUFBQSxFQUNFO0FBQUEsWUFBQSxDQUFBLEVBQUcsQ0FBQSxFQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsQ0FBQSxHQURIO0FBQUEsWUFFQSxDQUFBLEVBQUcsQ0FGSDtXQURGO0FBQUEsVUFJQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxFQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsRUFESDtBQUFBLFlBRUEsQ0FBQSxFQUFHLEVBRkg7V0FMRjtTQUhGO09BVEY7S0FISixDQXdCRSxDQUFDLEtBeEJILENBd0JTLE9BeEJULEVBeUJJO0FBQUEsTUFBQSxHQUFBLEVBQUssUUFBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxJQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSxrQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLFdBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLE9BQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLFVBQUEsRUFBWSxLQUFaO0FBQUEsUUFDQSxpQkFBQSxFQUNFO0FBQUEsVUFBQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxHQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsRUFESDtBQUFBLFlBRUEsQ0FBQSxFQUFHLENBQUEsR0FGSDtXQURGO0FBQUEsVUFJQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxFQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsRUFESDtBQUFBLFlBRUEsQ0FBQSxFQUFHLENBRkg7V0FMRjtTQUZGO09BVEY7S0F6QkosQ0E2Q0UsQ0FBQyxLQTdDSCxDQTZDUyxnQkE3Q1QsRUE4Q0k7QUFBQSxNQUFBLEdBQUEsRUFBSyxXQUFMO0FBQUEsTUFDQSxLQUFBLEVBQ0U7QUFBQSxRQUFBLEtBQUEsRUFDRTtBQUFBLFVBQUEsV0FBQSxFQUFhLDJCQUFiO0FBQUEsVUFDQSxVQUFBLEVBQVksbUJBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLGdCQUFmLEVBQVo7VUFBQSxDQUFYO1NBQVI7QUFBQSxRQUNBLE9BQUEsRUFBUztVQUFDLFdBQUQsRUFBYyxTQUFDLFNBQUQsR0FBQTttQkFBZSxTQUFTLENBQUMsUUFBVixDQUFBLEVBQWY7VUFBQSxDQUFkO1NBRFQ7T0FORjtBQUFBLE1BUUEsSUFBQSxFQUNFO0FBQUEsUUFBQSxFQUFBLEVBQUksQ0FBSjtBQUFBLFFBQ0EsVUFBQSxFQUFZLElBRFo7T0FURjtLQTlDSixDQTBERSxDQUFDLEtBMURILENBMERTLGFBMURULEVBMkRJO0FBQUEsTUFBQSxHQUFBLEVBQUssUUFBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxLQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSx3QkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGdCQURaO1NBREY7T0FGRjtBQUFBLE1BS0EsT0FBQSxFQUNFO0FBQUEsUUFBQSxNQUFBLEVBQVE7VUFBQyxRQUFELEVBQVcsU0FBQyxNQUFELEdBQUE7bUJBQVksTUFBTSxDQUFDLE9BQVAsQ0FBZSxhQUFmLEVBQVo7VUFBQSxDQUFYO1NBQVI7QUFBQSxRQUNBLE9BQUEsRUFBUztVQUFDLFdBQUQsRUFBYyxTQUFDLFNBQUQsR0FBQTttQkFBZSxTQUFTLENBQUMsUUFBVixDQUFBLEVBQWY7VUFBQSxDQUFkO1NBRFQ7T0FORjtBQUFBLE1BUUEsSUFBQSxFQUNFO0FBQUEsUUFBQSxVQUFBLEVBQVksS0FBWjtPQVRGO0tBM0RKLENBc0VFLENBQUMsS0F0RUgsQ0FzRVMsZ0JBdEVULEVBdUVJO0FBQUEsTUFBQSxHQUFBLEVBQUssV0FBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxLQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSwyQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLG1CQURaO1NBREY7T0FGRjtBQUFBLE1BS0EsT0FBQSxFQUNFO0FBQUEsUUFBQSxNQUFBLEVBQVE7VUFBQyxRQUFELEVBQVcsU0FBQyxNQUFELEdBQUE7bUJBQVksTUFBTSxDQUFDLE9BQVAsQ0FBZSxnQkFBZixFQUFaO1VBQUEsQ0FBWDtTQUFSO0FBQUEsUUFDQSxPQUFBLEVBQVM7VUFBQyxXQUFELEVBQWMsU0FBQyxTQUFELEdBQUE7bUJBQWUsU0FBUyxDQUFDLFFBQVYsQ0FBQSxFQUFmO1VBQUEsQ0FBZDtTQURUO09BTkY7QUFBQSxNQVFBLElBQUEsRUFDRTtBQUFBLFFBQUEsVUFBQSxFQUFZLEtBQVo7T0FURjtLQXZFSixDQWtGRSxDQUFDLEtBbEZILENBa0ZTLFNBbEZULEVBbUZJO0FBQUEsTUFBQSxHQUFBLEVBQUssVUFBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxJQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSxvQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGFBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLFNBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLFVBQUEsRUFBWSxLQUFaO0FBQUEsUUFDQSxpQkFBQSxFQUNFO0FBQUEsVUFBQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxHQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsR0FESDtBQUFBLFlBRUEsQ0FBQSxFQUFHLENBQUEsRUFGSDtXQURGO0FBQUEsVUFJQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxDQUFBLEVBQUg7QUFBQSxZQUNBLENBQUEsRUFBRyxDQUFBLEVBREg7QUFBQSxZQUVBLENBQUEsRUFBRyxDQUZIO1dBTEY7U0FGRjtPQVRGO0tBbkZKLENBdUdFLENBQUMsS0F2R0gsQ0F1R1MsZUF2R1QsRUF3R0k7QUFBQSxNQUFBLEdBQUEsRUFBSyxRQUFMO0FBQUEsTUFDQSxLQUFBLEVBQ0U7QUFBQSxRQUFBLE9BQUEsRUFDRTtBQUFBLFVBQUEsV0FBQSxFQUFhLDBCQUFiO0FBQUEsVUFDQSxVQUFBLEVBQVksa0JBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLGVBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLEVBQUEsRUFBSSxDQUFKO0FBQUEsUUFDQSxVQUFBLEVBQVksSUFEWjtPQVRGO0tBeEdKLENBb0hFLENBQUMsS0FwSEgsQ0FvSFMsZ0JBcEhULEVBcUhJO0FBQUEsTUFBQSxHQUFBLEVBQUssU0FBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxPQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSwyQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLG1CQURaO1NBREY7T0FGRjtBQUFBLE1BS0EsT0FBQSxFQUNFO0FBQUEsUUFBQSxNQUFBLEVBQVE7VUFBQyxRQUFELEVBQVcsU0FBQyxNQUFELEdBQUE7bUJBQVksTUFBTSxDQUFDLE9BQVAsQ0FBZSxnQkFBZixFQUFaO1VBQUEsQ0FBWDtTQUFSO0FBQUEsUUFDQSxPQUFBLEVBQVM7VUFBQyxXQUFELEVBQWMsU0FBQyxTQUFELEdBQUE7bUJBQWUsU0FBUyxDQUFDLFFBQVYsQ0FBQSxFQUFmO1VBQUEsQ0FBZDtTQURUO09BTkY7QUFBQSxNQVFBLElBQUEsRUFDRTtBQUFBLFFBQUEsVUFBQSxFQUFZLEtBQVo7T0FURjtLQXJISixDQWdJRSxDQUFDLEtBaElILENBZ0lTLGVBaElULEVBaUlJO0FBQUEsTUFBQSxHQUFBLEVBQUssUUFBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxPQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSwwQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGtCQURaO1NBREY7T0FGRjtBQUFBLE1BS0EsT0FBQSxFQUNFO0FBQUEsUUFBQSxNQUFBLEVBQVE7VUFBQyxRQUFELEVBQVcsU0FBQyxNQUFELEdBQUE7bUJBQVksTUFBTSxDQUFDLE9BQVAsQ0FBZSxlQUFmLEVBQVo7VUFBQSxDQUFYO1NBQVI7QUFBQSxRQUNBLE9BQUEsRUFBUztVQUFDLFdBQUQsRUFBYyxTQUFDLFNBQUQsR0FBQTttQkFBZSxTQUFTLENBQUMsUUFBVixDQUFBLEVBQWY7VUFBQSxDQUFkO1NBRFQ7T0FORjtBQUFBLE1BUUEsSUFBQSxFQUNFO0FBQUEsUUFBQSxVQUFBLEVBQVksS0FBWjtPQVRGO0tBaklKLENBNElFLENBQUMsS0E1SUgsQ0E0SVMsZUE1SVQsRUE2SUk7QUFBQSxNQUFBLEdBQUEsRUFBSyxRQUFMO0FBQUEsTUFDQSxLQUFBLEVBQ0U7QUFBQSxRQUFBLE9BQUEsRUFDRTtBQUFBLFVBQUEsV0FBQSxFQUFhLDBCQUFiO0FBQUEsVUFDQSxVQUFBLEVBQVksa0JBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLGVBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLFVBQUEsRUFBWSxLQUFaO09BVEY7S0E3SUosQ0F3SkUsQ0FBQyxLQXhKSCxDQXdKUyxjQXhKVCxFQXlKSTtBQUFBLE1BQUEsR0FBQSxFQUFLLE9BQUw7QUFBQSxNQUNBLEtBQUEsRUFDRTtBQUFBLFFBQUEsT0FBQSxFQUNFO0FBQUEsVUFBQSxXQUFBLEVBQWEseUJBQWI7QUFBQSxVQUNBLFVBQUEsRUFBWSxpQkFEWjtTQURGO09BRkY7QUFBQSxNQUtBLE9BQUEsRUFDRTtBQUFBLFFBQUEsTUFBQSxFQUFRO1VBQUMsUUFBRCxFQUFXLFNBQUMsTUFBRCxHQUFBO21CQUFZLE1BQU0sQ0FBQyxPQUFQLENBQWUsY0FBZixFQUFaO1VBQUEsQ0FBWDtTQUFSO0FBQUEsUUFDQSxPQUFBLEVBQVM7VUFBQyxXQUFELEVBQWMsU0FBQyxTQUFELEdBQUE7bUJBQWUsU0FBUyxDQUFDLFFBQVYsQ0FBQSxFQUFmO1VBQUEsQ0FBZDtTQURUO09BTkY7QUFBQSxNQVFBLElBQUEsRUFDRTtBQUFBLFFBQUEsVUFBQSxFQUFZLEtBQVo7T0FURjtLQXpKSixDQW9LRSxDQUFDLEtBcEtILENBb0tTLFdBcEtULEVBcUtJO0FBQUEsTUFBQSxHQUFBLEVBQUssWUFBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxJQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSxzQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGVBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLFdBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLEVBQUEsRUFBSSxDQUFKO0FBQUEsUUFDQSxVQUFBLEVBQVksSUFEWjtBQUFBLFFBRUEsaUJBQUEsRUFDRTtBQUFBLFVBQUEsUUFBQSxFQUNFO0FBQUEsWUFBQSxDQUFBLEVBQUcsQ0FBSDtBQUFBLFlBQ0EsQ0FBQSxFQUFHLENBREg7QUFBQSxZQUVBLENBQUEsRUFBRyxDQUZIO1dBREY7QUFBQSxVQUlBLFFBQUEsRUFDRTtBQUFBLFlBQUEsQ0FBQSxFQUFHLEVBQUg7QUFBQSxZQUNBLENBQUEsRUFBRyxDQURIO0FBQUEsWUFFQSxDQUFBLEVBQUcsQ0FGSDtXQUxGO1NBSEY7T0FURjtLQXJLSixDQTBMRSxDQUFDLEtBMUxILENBMExTLFVBMUxULEVBMkxJO0FBQUEsTUFBQSxHQUFBLEVBQUssV0FBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxJQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSxxQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGNBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLFVBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLEVBQUEsRUFBSSxDQUFKO0FBQUEsUUFDQSxVQUFBLEVBQVksSUFEWjtBQUFBLFFBRUEsaUJBQUEsRUFDRTtBQUFBLFVBQUEsUUFBQSxFQUNFO0FBQUEsWUFBQSxDQUFBLEVBQUcsQ0FBQSxFQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsRUFESDtBQUFBLFlBRUEsQ0FBQSxFQUFHLENBQUEsRUFGSDtXQURGO0FBQUEsVUFJQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxDQUFBLEVBQUg7QUFBQSxZQUNBLENBQUEsRUFBRyxFQURIO0FBQUEsWUFFQSxDQUFBLEVBQUcsRUFGSDtXQUxGO1NBSEY7T0FURjtLQTNMSixDQWdORSxDQUFDLEtBaE5ILENBZ05TLFNBaE5ULEVBaU5JO0FBQUEsTUFBQSxHQUFBLEVBQUssVUFBTDtBQUFBLE1BQ0EsS0FBQSxFQUNFO0FBQUEsUUFBQSxJQUFBLEVBQ0U7QUFBQSxVQUFBLFdBQUEsRUFBYSxvQkFBYjtBQUFBLFVBQ0EsVUFBQSxFQUFZLGFBRFo7U0FERjtPQUZGO0FBQUEsTUFLQSxPQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUTtVQUFDLFFBQUQsRUFBVyxTQUFDLE1BQUQsR0FBQTttQkFBWSxNQUFNLENBQUMsT0FBUCxDQUFlLFNBQWYsRUFBWjtVQUFBLENBQVg7U0FBUjtBQUFBLFFBQ0EsT0FBQSxFQUFTO1VBQUMsV0FBRCxFQUFjLFNBQUMsU0FBRCxHQUFBO21CQUFlLFNBQVMsQ0FBQyxRQUFWLENBQUEsRUFBZjtVQUFBLENBQWQ7U0FEVDtPQU5GO0FBQUEsTUFRQSxJQUFBLEVBQ0U7QUFBQSxRQUFBLFVBQUEsRUFBWSxJQUFaO0FBQUEsUUFDQSxFQUFBLEVBQUksQ0FESjtBQUFBLFFBRUEsaUJBQUEsRUFDRTtBQUFBLFVBQUEsUUFBQSxFQUNFO0FBQUEsWUFBQSxDQUFBLEVBQUcsQ0FBQSxHQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsQ0FBQSxFQURIO0FBQUEsWUFFQSxDQUFBLEVBQUcsQ0FGSDtXQURGO0FBQUEsVUFJQSxRQUFBLEVBQ0U7QUFBQSxZQUFBLENBQUEsRUFBRyxFQUFIO0FBQUEsWUFDQSxDQUFBLEVBQUcsQ0FBQSxFQURIO0FBQUEsWUFFQSxDQUFBLEVBQUcsQ0FBQSxDQUZIO1dBTEY7U0FIRjtPQVRGO0tBak5KLEVBcEJTO0VBQUEsQ0FBWCxDQXBCQSxDQUFBOztBQUFBLEVBZ1JBLEdBQUcsQ0FBQyxHQUFKLENBQVEsU0FBQyxVQUFELEVBQWEsUUFBYixFQUF1QixTQUF2QixFQUNDLE9BREQsRUFDVSxNQURWLEVBQ2tCLE9BRGxCLEVBQzJCLEtBRDNCLEVBQ2tDLFNBRGxDLEdBQUE7QUFFTixJQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksV0FBWixFQUF5QixPQUFPLENBQUMsSUFBakMsRUFBdUMsYUFBdkMsRUFBc0QsT0FBTyxDQUFDLElBQTlELENBQUEsQ0FBQTtBQUFBLElBQ0EsU0FBUyxDQUFDLElBQVYsQ0FBQSxDQURBLENBQUE7QUFBQSxJQUlBLFVBQVUsQ0FBQyxVQUFYLEdBQ0U7QUFBQSxNQUFBLFVBQUEsRUFBWSxLQUFaO0tBTEYsQ0FBQTtBQUFBLElBT0EsVUFBVSxDQUFDLEdBQVgsQ0FBZSxxQkFBZixFQUFzQyxTQUFBLEdBQUE7QUFDcEMsVUFBQSxjQUFBO0FBQUEsTUFBQSxJQUFBLEdBQU8sU0FBUyxDQUFDLElBQVYsQ0FBQSxDQUFQLENBQUE7QUFBQSxNQUNBLE9BQU8sQ0FBQyxHQUFSLENBQVksaUJBQVosRUFBK0IsSUFBL0IsQ0FEQSxDQUFBO0FBQUEsTUFFQSxTQUFTLENBQUMsYUFBVixDQUF3QixJQUF4QixDQUZBLENBQUE7QUFBQSxNQUdBLFFBQUEsR0FBVyxTQUFTLENBQUMsSUFBVixDQUFBLENBSFgsQ0FBQTthQUlBLE9BQU8sQ0FBQyxHQUFSLENBQVksYUFBWixFQUEyQixJQUFJLENBQUMsU0FBTCxDQUFlO0FBQUEsUUFBQyxRQUFBLEVBQVUsUUFBWDtBQUFBLFFBQzFDLEtBQUEsRUFBTSxVQURvQztPQUFmLENBQTNCLEVBTG9DO0lBQUEsQ0FBdEMsQ0FQQSxDQUFBO1dBaUJBLFVBQVUsQ0FBQyxHQUFYLENBQWUsbUJBQWYsRUFBb0MsU0FBQyxLQUFELEVBQVEsRUFBUixFQUFZLFFBQVosRUFBc0IsSUFBdEIsR0FBQTtBQUNsQyxNQUFBLElBQUcsQ0FBQSxVQUFXLENBQUMsb0JBQWY7QUFDRSxRQUFBLEtBQUssQ0FBQyxjQUFOLENBQUEsQ0FBQSxDQUFBO0FBQUEsUUFDQSxVQUFVLENBQUMsVUFBVSxDQUFDLFVBQXRCLEdBQ0UsbUJBQUEsSUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQVIsR0FBYSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBRnhDLENBQUE7QUFBQSxRQUdBLFVBQVUsQ0FBQyxvQkFBWCxHQUFrQyxJQUhsQyxDQUFBO0FBQUEsUUFJQSxRQUFBLENBQVMsU0FBQSxHQUFBO2lCQUNQLE1BQU0sQ0FBQyxFQUFQLENBQVUsRUFBVixFQUFjLEVBQWQsRUFBa0IsUUFBbEIsRUFETztRQUFBLENBQVQsRUFFRSxHQUZGLENBSkEsQ0FERjtPQUFBLE1BQUE7QUFTRSxRQUFBLFVBQVUsQ0FBQyxvQkFBWCxHQUFrQyxLQUFsQyxDQVRGO09BQUE7YUFVQSxLQVhrQztJQUFBLENBQXBDLEVBbkJNO0VBQUEsQ0FBUixDQWhSQSxDQUFBO0FBQUEiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBvdmVydmlld1xuICMgQG5hbWUgbW9iaWxlQXBwXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgbW9iaWxlQXBwXG4gI1xuICMgTWFpbiBtb2R1bGUgb2YgdGhlIGFwcGxpY2F0aW9uLlxuIyMjXG5hcHAgPSBhbmd1bGFyLm1vZHVsZSgndnJjaGFsbGVuZ2Vpb0FwcCcsIFtcbiAgJ25nQW5pbWF0ZSdcbiAgJ25nQ29va2llcydcbiAgJ25nU2FuaXRpemUnXG4gICduZ1RvdWNoJ1xuICAncGFzY2FscHJlY2h0LnRyYW5zbGF0ZSdcbiAgJ3VpLnJvdXRlcidcbl0pXG5cblxuYXBwLmNvbmZpZyAoJHRyYW5zbGF0ZVByb3ZpZGVyLCAkdXJsUm91dGVyUHJvdmlkZXIsICRzdGF0ZVByb3ZpZGVyLFxuJGxvY2F0aW9uUHJvdmlkZXIpIC0+XG5cbiAgIyBTZXQgdXAgdHJhbnNsYXRpb25zLlxuICAkdHJhbnNsYXRlUHJvdmlkZXIucHJlZmVycmVkTGFuZ3VhZ2UgJ3BsJ1xuICAkdHJhbnNsYXRlUHJvdmlkZXIudXNlU3RhdGljRmlsZXNMb2FkZXIge1xuICAgIHByZWZpeDogJ2xhbmd1YWdlcy8nXG4gICAgc3VmZml4OiAnLmpzb24nXG4gIH1cblxuICAjIFNldCB1cCByb3V0ZXMuXG4gIGlzUHJvZCA9IHdpbmRvdy5sb2NhdGlvbi5ob3N0bmFtZS5pbmRleE9mKCd2cmNoYWxsZW5nZS5pbycpICE9IC0xXG4gICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1TW9kZSBpc1Byb2RcbiAgJHVybFJvdXRlclByb3ZpZGVyLm90aGVyd2lzZSAnLydcblxuICAjIFByZXZlbnQgY2VydGFpbiByb3V0ZXMuXG4gICR1cmxSb3V0ZXJQcm92aWRlci53aGVuICcvYWJvdXQnLCAnL2Fib3V0L2Zlc3RpdmFsJ1xuICAkdXJsUm91dGVyUHJvdmlkZXIud2hlbiAnL2NvbnRlc3QnLCAnL2NvbnRlc3QvcnVsZXMnXG5cbiAgIyBEZWZpbmUgcm91dGVzLlxuICAkc3RhdGVQcm92aWRlclxuXG4gICAgLnN0YXRlICdsYW5kaW5nJyxcbiAgICAgIHVybDogJy8nXG4gICAgICB2aWV3czpcbiAgICAgICAgbWFpbjpcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3ZpZXdzL2xhbmRpbmcuaHRtbCdcbiAgICAgICAgICBjb250cm9sbGVyOiAnTGFuZGluZ0N0cmwnXG4gICAgICByZXNvbHZlOlxuICAgICAgICBhY2Nlc3M6IFsnQWNjZXNzJywgKEFjY2VzcykgLT4gQWNjZXNzLnJlcXVpcmUgJ2xhbmRpbmcnXVxuICAgICAgICBwcmVsb2FkOiBbJ1ByZWxvYWRlcicsIChQcmVsb2FkZXIpIC0+IFByZWxvYWRlci5sb2FkTWFpbigpXVxuICAgICAgZGF0YTpcbiAgICAgICAgc2hvd0luTWVudTogdHJ1ZVxuICAgICAgICBpZDogMFxuICAgICAgICBiYWNrZ3JvdW5kRWZmZWN0czpcbiAgICAgICAgICBwb3NpdGlvbjpcbiAgICAgICAgICAgIHg6IC04MFxuICAgICAgICAgICAgeTogLTEwMFxuICAgICAgICAgICAgejogMFxuICAgICAgICAgIHJvdGF0aW9uOlxuICAgICAgICAgICAgeDogMTBcbiAgICAgICAgICAgIHk6IDIwXG4gICAgICAgICAgICB6OiAxMFxuXG4gICAgLnN0YXRlICdhYm91dCcsXG4gICAgICB1cmw6ICcvYWJvdXQnXG4gICAgICB2aWV3czpcbiAgICAgICAgbWFpbjpcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3ZpZXdzL2Fib3V0Lmh0bWwnXG4gICAgICAgICAgY29udHJvbGxlcjogJ0Fib3V0Q3RybCdcbiAgICAgIHJlc29sdmU6XG4gICAgICAgIGFjY2VzczogWydBY2Nlc3MnLCAoQWNjZXNzKSAtPiBBY2Nlc3MucmVxdWlyZSAnYWJvdXQnXVxuICAgICAgICBwcmVsb2FkOiBbJ1ByZWxvYWRlcicsIChQcmVsb2FkZXIpIC0+IFByZWxvYWRlci5sb2FkTWFpbigpXVxuICAgICAgZGF0YTpcbiAgICAgICAgc2hvd0luTWVudTogZmFsc2VcbiAgICAgICAgYmFja2dyb3VuZEVmZmVjdHM6XG4gICAgICAgICAgcG9zaXRpb246XG4gICAgICAgICAgICB4OiAxMDBcbiAgICAgICAgICAgIHk6IDYwXG4gICAgICAgICAgICB6OiAtMTAwXG4gICAgICAgICAgcm90YXRpb246XG4gICAgICAgICAgICB4OiA0NVxuICAgICAgICAgICAgeTogMzBcbiAgICAgICAgICAgIHo6IDBcblxuICAgIC5zdGF0ZSAnYWJvdXQuZmVzdGl2YWwnLFxuICAgICAgdXJsOiAnL2Zlc3RpdmFsJ1xuICAgICAgdmlld3M6XG4gICAgICAgIGFib3V0OlxuICAgICAgICAgIHRlbXBsYXRlVXJsOiAndmlld3MvYWJvdXQtZmVzdGl2YWwuaHRtbCdcbiAgICAgICAgICBjb250cm9sbGVyOiAnQWJvdXRGZXN0aXZhbEN0cmwnXG4gICAgICByZXNvbHZlOlxuICAgICAgICBhY2Nlc3M6IFsnQWNjZXNzJywgKEFjY2VzcykgLT4gQWNjZXNzLnJlcXVpcmUgJ2Fib3V0LmZlc3RpdmFsJ11cbiAgICAgICAgcHJlbG9hZDogWydQcmVsb2FkZXInLCAoUHJlbG9hZGVyKSAtPiBQcmVsb2FkZXIubG9hZE1haW4oKV1cbiAgICAgIGRhdGE6XG4gICAgICAgIGlkOiAxXG4gICAgICAgIHNob3dJbk1lbnU6IHRydWVcblxuICAgIC5zdGF0ZSAnYWJvdXQudW5pdDknLFxuICAgICAgdXJsOiAnL3VuaXQ5J1xuICAgICAgdmlld3M6XG4gICAgICAgIGFib3V0OlxuICAgICAgICAgIHRlbXBsYXRlVXJsOiAndmlld3MvYWJvdXQtdW5pdDkuaHRtbCdcbiAgICAgICAgICBjb250cm9sbGVyOiAnQWJvdXRVbml0OUN0cmwnXG4gICAgICByZXNvbHZlOlxuICAgICAgICBhY2Nlc3M6IFsnQWNjZXNzJywgKEFjY2VzcykgLT4gQWNjZXNzLnJlcXVpcmUgJ2Fib3V0LnVuaXQ5J11cbiAgICAgICAgcHJlbG9hZDogWydQcmVsb2FkZXInLCAoUHJlbG9hZGVyKSAtPiBQcmVsb2FkZXIubG9hZE1haW4oKV1cbiAgICAgIGRhdGE6XG4gICAgICAgIHNob3dJbk1lbnU6IGZhbHNlXG5cbiAgICAuc3RhdGUgJ2Fib3V0LmxvY2F0aW9uJyxcbiAgICAgIHVybDogJy9sb2NhdGlvbidcbiAgICAgIHZpZXdzOlxuICAgICAgICBhYm91dDpcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3ZpZXdzL2Fib3V0LWxvY2F0aW9uLmh0bWwnXG4gICAgICAgICAgY29udHJvbGxlcjogJ0Fib3V0TG9jYXRpb25DdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdhYm91dC5sb2NhdGlvbiddXG4gICAgICAgIHByZWxvYWQ6IFsnUHJlbG9hZGVyJywgKFByZWxvYWRlcikgLT4gUHJlbG9hZGVyLmxvYWRNYWluKCldXG4gICAgICBkYXRhOlxuICAgICAgICBzaG93SW5NZW51OiBmYWxzZVxuXG4gICAgLnN0YXRlICdjb250ZXN0JyxcbiAgICAgIHVybDogJy9jb250ZXN0J1xuICAgICAgdmlld3M6XG4gICAgICAgIG1haW46XG4gICAgICAgICAgdGVtcGxhdGVVcmw6ICd2aWV3cy9jb250ZXN0Lmh0bWwnXG4gICAgICAgICAgY29udHJvbGxlcjogJ0NvbnRlc3RDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdjb250ZXN0J11cbiAgICAgICAgcHJlbG9hZDogWydQcmVsb2FkZXInLCAoUHJlbG9hZGVyKSAtPiBQcmVsb2FkZXIubG9hZE1haW4oKV1cbiAgICAgIGRhdGE6XG4gICAgICAgIHNob3dJbk1lbnU6IGZhbHNlXG4gICAgICAgIGJhY2tncm91bmRFZmZlY3RzOlxuICAgICAgICAgIHBvc2l0aW9uOlxuICAgICAgICAgICAgeDogMTAwXG4gICAgICAgICAgICB5OiAxODBcbiAgICAgICAgICAgIHo6IC02MFxuICAgICAgICAgIHJvdGF0aW9uOlxuICAgICAgICAgICAgeDogLTQ1XG4gICAgICAgICAgICB5OiAtMzBcbiAgICAgICAgICAgIHo6IDBcblxuICAgIC5zdGF0ZSAnY29udGVzdC5ydWxlcycsXG4gICAgICB1cmw6ICcvcnVsZXMnXG4gICAgICB2aWV3czpcbiAgICAgICAgY29udGVzdDpcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3ZpZXdzL2NvbnRlc3QtcnVsZXMuaHRtbCdcbiAgICAgICAgICBjb250cm9sbGVyOiAnQ29udGVzdFJ1bGVzQ3RybCdcbiAgICAgIHJlc29sdmU6XG4gICAgICAgIGFjY2VzczogWydBY2Nlc3MnLCAoQWNjZXNzKSAtPiBBY2Nlc3MucmVxdWlyZSAnY29udGVzdC5ydWxlcyddXG4gICAgICAgIHByZWxvYWQ6IFsnUHJlbG9hZGVyJywgKFByZWxvYWRlcikgLT4gUHJlbG9hZGVyLmxvYWRNYWluKCldXG4gICAgICBkYXRhOlxuICAgICAgICBpZDogMlxuICAgICAgICBzaG93SW5NZW51OiB0cnVlXG5cbiAgICAuc3RhdGUgJ2NvbnRlc3QucHJpemVzJyxcbiAgICAgIHVybDogJy9wcml6ZXMnXG4gICAgICB2aWV3czpcbiAgICAgICAgY29udGVzdDpcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3ZpZXdzL2NvbnRlc3QtcHJpemVzLmh0bWwnXG4gICAgICAgICAgY29udHJvbGxlcjogJ0NvbnRlc3RQcml6ZXNDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdjb250ZXN0LnByaXplcyddXG4gICAgICAgIHByZWxvYWQ6IFsnUHJlbG9hZGVyJywgKFByZWxvYWRlcikgLT4gUHJlbG9hZGVyLmxvYWRNYWluKCldXG4gICAgICBkYXRhOlxuICAgICAgICBzaG93SW5NZW51OiBmYWxzZVxuXG4gICAgLnN0YXRlICdjb250ZXN0LmFwcGx5JyxcbiAgICAgIHVybDogJy9hcHBseSdcbiAgICAgIHZpZXdzOlxuICAgICAgICBjb250ZXN0OlxuICAgICAgICAgIHRlbXBsYXRlVXJsOiAndmlld3MvY29udGVzdC1hcHBseS5odG1sJ1xuICAgICAgICAgIGNvbnRyb2xsZXI6ICdDb250ZXN0QXBwbHlDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdjb250ZXN0LmFwcGx5J11cbiAgICAgICAgcHJlbG9hZDogWydQcmVsb2FkZXInLCAoUHJlbG9hZGVyKSAtPiBQcmVsb2FkZXIubG9hZE1haW4oKV1cbiAgICAgIGRhdGE6XG4gICAgICAgIHNob3dJbk1lbnU6IGZhbHNlXG5cbiAgICAuc3RhdGUgJ2NvbnRlc3Qud29ya3MnLFxuICAgICAgdXJsOiAnL3dvcmtzJ1xuICAgICAgdmlld3M6XG4gICAgICAgIGNvbnRlc3Q6XG4gICAgICAgICAgdGVtcGxhdGVVcmw6ICd2aWV3cy9jb250ZXN0LXdvcmtzLmh0bWwnXG4gICAgICAgICAgY29udHJvbGxlcjogJ0NvbnRlc3RXb3Jrc0N0cmwnXG4gICAgICByZXNvbHZlOlxuICAgICAgICBhY2Nlc3M6IFsnQWNjZXNzJywgKEFjY2VzcykgLT4gQWNjZXNzLnJlcXVpcmUgJ2NvbnRlc3Qud29ya3MnXVxuICAgICAgICBwcmVsb2FkOiBbJ1ByZWxvYWRlcicsIChQcmVsb2FkZXIpIC0+IFByZWxvYWRlci5sb2FkTWFpbigpXVxuICAgICAgZGF0YTpcbiAgICAgICAgc2hvd0luTWVudTogZmFsc2VcblxuICAgIC5zdGF0ZSAnY29udGVzdC5qdXJ5JyxcbiAgICAgIHVybDogJy9qdXJ5J1xuICAgICAgdmlld3M6XG4gICAgICAgIGNvbnRlc3Q6XG4gICAgICAgICAgdGVtcGxhdGVVcmw6ICd2aWV3cy9jb250ZXN0LWp1cnkuaHRtbCdcbiAgICAgICAgICBjb250cm9sbGVyOiAnQ29udGVzdEp1cnlDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdjb250ZXN0Lmp1cnknXVxuICAgICAgICBwcmVsb2FkOiBbJ1ByZWxvYWRlcicsIChQcmVsb2FkZXIpIC0+IFByZWxvYWRlci5sb2FkTWFpbigpXVxuICAgICAgZGF0YTpcbiAgICAgICAgc2hvd0luTWVudTogZmFsc2VcblxuICAgIC5zdGF0ZSAncHJvZ3JhbW1lJyxcbiAgICAgIHVybDogJy9wcm9ncmFtbWUnXG4gICAgICB2aWV3czpcbiAgICAgICAgbWFpbjpcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3ZpZXdzL3Byb2dyYW1tZS5odG1sJ1xuICAgICAgICAgIGNvbnRyb2xsZXI6ICdQcm9ncmFtbWVDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdwcm9ncmFtbWUnXVxuICAgICAgICBwcmVsb2FkOiBbJ1ByZWxvYWRlcicsIChQcmVsb2FkZXIpIC0+IFByZWxvYWRlci5sb2FkTWFpbigpXVxuICAgICAgZGF0YTpcbiAgICAgICAgaWQ6IDNcbiAgICAgICAgc2hvd0luTWVudTogdHJ1ZVxuICAgICAgICBiYWNrZ3JvdW5kRWZmZWN0czpcbiAgICAgICAgICBwb3NpdGlvbjpcbiAgICAgICAgICAgIHg6IDBcbiAgICAgICAgICAgIHk6IDBcbiAgICAgICAgICAgIHo6IDBcbiAgICAgICAgICByb3RhdGlvbjpcbiAgICAgICAgICAgIHg6IDEwXG4gICAgICAgICAgICB5OiAwXG4gICAgICAgICAgICB6OiAwXG5cbiAgICAuc3RhdGUgJ3BhcnRuZXJzJyxcbiAgICAgIHVybDogJy9wYXJ0bmVycydcbiAgICAgIHZpZXdzOlxuICAgICAgICBtYWluOlxuICAgICAgICAgIHRlbXBsYXRlVXJsOiAndmlld3MvcGFydG5lcnMuaHRtbCdcbiAgICAgICAgICBjb250cm9sbGVyOiAnUGFydG5lcnNDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdwYXJ0bmVycyddXG4gICAgICAgIHByZWxvYWQ6IFsnUHJlbG9hZGVyJywgKFByZWxvYWRlcikgLT4gUHJlbG9hZGVyLmxvYWRNYWluKCldXG4gICAgICBkYXRhOlxuICAgICAgICBpZDogNFxuICAgICAgICBzaG93SW5NZW51OiB0cnVlXG4gICAgICAgIGJhY2tncm91bmRFZmZlY3RzOlxuICAgICAgICAgIHBvc2l0aW9uOlxuICAgICAgICAgICAgeDogLTUwXG4gICAgICAgICAgICB5OiA1MFxuICAgICAgICAgICAgejogLTUwXG4gICAgICAgICAgcm90YXRpb246XG4gICAgICAgICAgICB4OiAtMzBcbiAgICAgICAgICAgIHk6IDMwXG4gICAgICAgICAgICB6OiA5MFxuXG4gICAgLnN0YXRlICdjb250YWN0JyxcbiAgICAgIHVybDogJy9jb250YWN0J1xuICAgICAgdmlld3M6XG4gICAgICAgIG1haW46XG4gICAgICAgICAgdGVtcGxhdGVVcmw6ICd2aWV3cy9jb250YWN0Lmh0bWwnXG4gICAgICAgICAgY29udHJvbGxlcjogJ0NvbnRhY3RDdHJsJ1xuICAgICAgcmVzb2x2ZTpcbiAgICAgICAgYWNjZXNzOiBbJ0FjY2VzcycsIChBY2Nlc3MpIC0+IEFjY2Vzcy5yZXF1aXJlICdjb250YWN0J11cbiAgICAgICAgcHJlbG9hZDogWydQcmVsb2FkZXInLCAoUHJlbG9hZGVyKSAtPiBQcmVsb2FkZXIubG9hZE1haW4oKV1cbiAgICAgIGRhdGE6XG4gICAgICAgIHNob3dJbk1lbnU6IHRydWVcbiAgICAgICAgaWQ6IDVcbiAgICAgICAgYmFja2dyb3VuZEVmZmVjdHM6XG4gICAgICAgICAgcG9zaXRpb246XG4gICAgICAgICAgICB4OiAtMTAwXG4gICAgICAgICAgICB5OiAtNzBcbiAgICAgICAgICAgIHo6IDBcbiAgICAgICAgICByb3RhdGlvbjpcbiAgICAgICAgICAgIHg6IDMwXG4gICAgICAgICAgICB5OiAtNDVcbiAgICAgICAgICAgIHo6IC01XG5cblxuIyBXaGVuIHRoZSBhcHAgcnVucy5cbmFwcC5ydW4gKCRyb290U2NvcGUsICR0aW1lb3V0LCAkbG9jYXRpb24sXG4gICAgICAgICAkd2luZG93LCAkc3RhdGUsIFZlcnNpb24sIFRvdWNoLCBBbmFseXRpY3MpIC0+XG4gIGNvbnNvbGUubG9nICdWZXJzaW9uOiAnLCBWZXJzaW9uLmluZm8sICdCdWlsZCBkYXRlOicsIFZlcnNpb24uZGF0ZVxuICBBbmFseXRpY3MuaW5pdCgpXG4gICMgVG91Y2gucHJldmVudE92ZXJzY3JvbGwoKVxuXG4gICRyb290U2NvcGUudHJhbnNpdGlvbiA9XG4gICAgaXNCYWNrd2FyZDogZmFsc2VcblxuICAkcm9vdFNjb3BlLiRvbiAnJHN0YXRlQ2hhbmdlU3VjY2VzcycsIC0+XG4gICAgcGF0aCA9ICRsb2NhdGlvbi5wYXRoKClcbiAgICBjb25zb2xlLmxvZyAnW0dBXSBbcGFnZXZpZXddJywgcGF0aFxuICAgIEFuYWx5dGljcy50cmFja1BhZ2V2aWV3IHBhdGhcbiAgICBwYWdlTmFtZSA9ICRsb2NhdGlvbi5wYXRoKClcbiAgICBjb25zb2xlLmxvZyAnW1RPRE9dIFtHQV0nLCBKU09OLnN0cmluZ2lmeSB7cGFnZU5hbWU6IHBhZ2VOYW1lLFxuICAgIGV2ZW50OidwYWdlVmlldyd9XG4gICAgIyBUT0RPOiBpbXBsZW1lbnQgdHJhY2tpbmcgYXMgeW91IHdpc2guXG4gICAgIyAkd2luZG93LmRhdGFMYXllci5wdXNoPyh7cGFnZU5hbWU6IHBhZ2VOYW1lLCBldmVudDoncGFnZVZpZXcnfSlcblxuICAkcm9vdFNjb3BlLiRvbiAnJHN0YXRlQ2hhbmdlU3RhcnQnLCAoZXZlbnQsIHRvLCB0b1BhcmFtcywgZnJvbSkgLT5cbiAgICBpZiAhJHJvb3RTY29wZS5zaG91bGRDaGFuZ2VTdGF0ZU5vd1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgJHJvb3RTY29wZS50cmFuc2l0aW9uLmlzQmFja3dhcmQgPVxuICAgICAgICBmcm9tLmRhdGE/IGFuZCB0by5kYXRhLmlkIDwgZnJvbS5kYXRhLmlkXG4gICAgICAkcm9vdFNjb3BlLnNob3VsZENoYW5nZVN0YXRlTm93ID0gdHJ1ZVxuICAgICAgJHRpbWVvdXQgKCkgLT5cbiAgICAgICAgJHN0YXRlLmdvIHRvLCB7fSwgdG9QYXJhbXNcbiAgICAgICwgMTAwXG4gICAgZWxzZVxuICAgICAgJHJvb3RTY29wZS5zaG91bGRDaGFuZ2VTdGF0ZU5vdyA9IGZhbHNlXG4gICAgdHJ1ZVxuXG4iXX0=

(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/background-effects.html', '');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/language-selector.html', '<nav class="language"><ul><li ng-repeat="language in languageSelector.languages"><a ng-click="selectLanguage(language.code)" ng-class="{active: language.code == languageSelector.current}">{{ language.name }}</a></li></ul></nav>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/logo.html', '<div class="asset _logo-unit9vr unit9vr"></div><div class="asset _logo-challenge challenge"></div><div class="asset _logo-2015 date"></div>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/menu.html', '<div class="menu" ng-class="{\'menu--open\': isOpen}"><div class="menu__toggle"><div class="menu__toggle__line"></div></div><div class="menu__toggle menu__toggle--active" ng-click="toggle()"><div class="menu__toggle__line"></div></div><nav class="menu__list"><a ng-repeat="state in menu.states" class="menu__list__link" ui-sref="{{ state.name }}" ng-class="{\'menu__list__link--active\': isActiveState(state.name)}" ng-click="close()">{{ \'menu.\' + state.name | translate }}</a></nav><div class="menu__background"></div></div>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/preloader.html', '');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/vrc-checkbox.html', '<section class="vrc-checkbox" ng-class="{\'checked\': checked}" ng-click="toggle()"><div class="state on"></div><div class="state off"></div></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('partials/vrc-message-box.html', '<div class="content" ng-class="{ \'open\': messageBox.isOpen }"><aside class="dimmer"></aside><section class="box"><p class="copy">{{ \'confirmation.contestant\' | translate }}</p><button class="close" ng-click="close()">{{ \'confirmation.closeCta\' | translate }}</button></section></div>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/about-festival.html', '<section class="subview about festival"><p class="p-heading heading">{{ \'aboutFestival.heading\' | translate }}</p><p class="p-body body" ng-bind-html="\'aboutFestival.body\' | translate"></p><p class="p-heading location">{{ \'aboutFestival.location\' | translate }}</p><p class="p-heading patronage">{{ \'aboutFestival.patronage\' | translate }}</p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/about-location.html', '<section class="subview about location"><p class="p-heading body">{{ \'aboutLocation.body\' | translate }}</p><p class="p-body info" ng-bind-html="\'aboutLocation.info\' | translate | trust"></p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/about-unit9.html', '<section class="subview about unit9"><p class="p-heading heading" ng-bind-html="\'aboutUnit9.heading\' | translate"></p><p class="p-body body" ng-bind-html="\'aboutUnit9.body\' | translate"></p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/about.html', '<section class="view about"><section class="content"><p class="p-title">{{ \'about.title\' | translate }}</p><nav class="about-menu"><ul><li><a ui-sref="about.festival" ui-sref-active="active">{{ \'about.menuFestival\' | translate }}</a></li><li><a ui-sref="about.unit9" ui-sref-active="active">{{ \'about.menuUnit9\' | translate }}</a></li><li><a ui-sref="about.location" ui-sref-active="active">{{ \'about.menuLocation\' | translate }}</a></li></ul></nav><div ui-view="about" class="uiview"></div></section></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contact.html', '<section class="view contact"><section class="content"><p class="p-title desktop">{{ \'contact.title\' | translate }}</p><article class="copy"><p class="p-title tablet mobile">{{ \'contact.title\' | translate }}</p><section class="social"><a class="asset _icon-facebook" href="http://fb.com/vrchallenge" target="_blank"></a> <a class="asset _icon-twitter" href="https://twitter.com/vrchallenge" target="_blank"></a></section><p class="p-body email"><a ng-href="mailto:{{contact.emailMain}}">{{ \'contact.emailMain\' | translate }}</a></p><p class="p-body email"><a ng-href="mailto:{{contact.emailPress}}">{{ \'contact.emailPress\' | translate }}</a></p><p class="p-body phone">{{ \'contact.phone\' | translate }}</p><p class="p-body address">{{ \'contact.address\' | translate }}</p><section class="downloads"><a class="p-link" href="/documents/presskit_UNIT9_VR_Challenge_2015_pl.pdf" target="_blank">{{ \'contact.downloadPl\' | translate }}</a> <a class="p-link" href="/documents/presskit_UNIT9_VR_Challenge_2015_en.pdf" target="_blank">{{ \'contact.downloadEn\' | translate }}</a></section></article></section></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contest-apply.html', '<section class="subview contest apply" ng-init="init()"><p class="p-heading heading" ng-bind-html="\'contestApply.heading\' | translate"></p><p class="p-body body" ng-bind-html="\'contestApply.body\' | translate"></p><section class="form" ng-class="{\'invalid-email\': user.invalidEmail, \'invalid-accept\': user.invalidAccept, \'enabled\': !user.formSending}"><input class="input-email" type="email" placeholder="{{ \'contestApply.emailInputPlaceholder\' | translate }}" ng-model="user.email"> <button class="button-submit" ng-click="submit($event)">{{ \'contestApply.submitCta\' | translate }}</button><section class="checkbox"><div class="checkbox-component" vrc-checkbox="" checked="user.regulationsAccepted"></div><p class="p-body copy" ng-bind-html="\'contestApply.regulationsAcknowledgement\' | translate"></p></section></section><p class="p-headline confirmation" ng-class="{ \'shown\': user.formSent }">{{ \'contestApply.confirmation\' | translate }}</p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contest-jury.html', '<section class="subview contest jury"><p class="p-body body" ng-bind-html="\'contestJury.body\' | translate"></p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contest-prizes.html', '<section class="subview contest prizes"><p class="p-heading heading" ng-bind-html="\'contestPrizes.heading\' | translate"></p><p class="p-body body" ng-bind-html="\'contestPrizes.body\' | translate"></p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contest-rules.html', '<section class="subview contest rules"><p class="p-heading heading" ng-bind-html="\'contestRules.heading\' | translate"></p><p class="p-body body" ng-bind-html="\'contestRules.body\' | translate"></p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contest-works.html', '<section class="subview contest works"><p class="p-heading heading" ng-bind-html="\'contestWorks.heading\' | translate"></p></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/contest.html', '<section class="view contest"><section class="content"><p class="p-title">{{ \'contest.title\' | translate }}</p><nav class="contest-menu"><ul><li><a ui-sref="contest.rules" ui-sref-active="active">{{ \'contest.menuRules\' | translate }}</a></li><li><a ui-sref="contest.prizes" ui-sref-active="active">{{ \'contest.menuPrizes\' | translate }}</a></li><li><a ui-sref="contest.apply" ui-sref-active="active">{{ \'contest.menuApply\' | translate }}</a></li><li><a ui-sref="contest.works" ui-sref-active="active">{{ \'contest.menuWorks\' | translate }}</a></li><li><a ui-sref="contest.jury" ui-sref-active="active">{{ \'contest.menuJury\' | translate }}</a></li></ul></nav><div ui-view="contest" class="uiview"></div></section></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/landing.html', '<section class="view landing"><section class="content"><div logo=""></div><p class="info"><span class="date">{{ \'landing.date\' | translate }}</span> <span class="location">{{ \'landing.location\' | translate }}</span></p></section><button class="enter p-heading" ng-click="highlightMenu()">{{ \'landing.enter\' | translate }}</button></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/partners.html', '<section class="view partners"><section class="content"><div class="partners-group partners-group--centered"><div class="partners-group__title">{{\'partners.honour\' | translate}}</div><div class="partners-group__grid"><div class="partners-group__grid__item" ng-repeat="p in partners.honour"><a ng-if="p != null" href="{{p.url}}" target="_blank" class="partners-group__grid__item__asset asset {{ p.name | partnerClass : \'h\' }}"></a><div ng-if="p.url == null" class="partners-group__grid__item__asset asset {{ p | partnerClass : \'h\' }}"></div></div></div></div><div class="partners-group"><div class="partners-group__title">{{\'partners.partners\' | translate}}</div><div class="partners-group__grid"><div class="partners-group__grid__item" ng-repeat="p in partners.partners"><a ng-if="p != null" href="{{p.url}}" target="_blank" class="partners-group__grid__item__asset asset {{ p.name | partnerClass : \'p\' }}"></a><div ng-if="p.url == null" class="partners-group__grid__item__asset asset {{ p | partnerClass : \'p\' }}"></div></div></div></div><div class="partners-group"><div class="partners-group__title">{{\'partners.media\' | translate}}</div><div class="partners-group__grid"><div class="partners-group__grid__item" ng-repeat="p in partners.media"><a ng-if="p != null" href="{{p.url}}" target="_blank" class="partners-group__grid__item__asset asset {{ p.name | partnerClass : \'m\' }}"></a><div ng-if="p.url == null" class="partners-group__grid__item__asset asset {{ p | partnerClass : \'m\' }}"></div></div></div></div><div class="partners-group partners-group--sponsors"><div class="partners-group__title">{{\'partners.sponsors\' | translate}}</div><div class="partners-group__grid"><div class="partners-group__grid__item" ng-repeat="p in partners.sponsors"><a ng-if="p != null" href="{{p.url}}" target="_blank" class="partners-group__grid__item__asset asset {{ p.name | partnerClass : \'s\' }}"></a><div ng-if="p.url == null" class="partners-group__grid__item__asset asset {{ p | partnerClass : \'s\' }}"></div></div></div></div></section></section>');
    }
  ]);
}());
(function (module) {
  try {
    module = angular.module('vrchallengeioApp');
  } catch (e) {
    module = angular.module('vrchallengeioApp', []);
  }
  module.run([
    '$templateCache',
    function ($templateCache) {
      $templateCache.put('views/programme.html', '<section class="view programme"><section class="content"><p class="p-title">{{ \'programme.title\' | translate }}</p><p class="p-heading placeholder" ng-bind-html="\'programme.placeholder\' | translate"></p></section></section>');
    }
  ]);
}());
(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name mobileApp.controller:CallCtrl
    * @description
    * # CallCtrl
    * Controller of the mobileApp
   */
  angular.module('vrchallengeioApp').controller('LandingCtrl', [
    '$scope',
    function ($scope) {
      return $scope.highlightMenu = function () {
        $scope.$emit('menu.highlight');
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2xhbmRpbmcuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsVUFESCxDQUNjLGFBRGQsRUFDNkIsU0FBQyxNQUFELEdBQUE7V0FFekIsTUFBTSxDQUFDLGFBQVAsR0FBdUIsU0FBQSxHQUFBO0FBQ3JCLE1BQUEsTUFBTSxDQUFDLEtBQVAsQ0FBYSxnQkFBYixDQUFBLENBRHFCO0lBQUEsRUFGRTtFQUFBLENBRDdCLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2xhbmRpbmcuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZ1bmN0aW9uXG4gIyBAbmFtZSBtb2JpbGVBcHAuY29udHJvbGxlcjpDYWxsQ3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIENhbGxDdHJsXG4gIyBDb250cm9sbGVyIG9mIHRoZSBtb2JpbGVBcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUoJ3ZyY2hhbGxlbmdlaW9BcHAnKVxuICAuY29udHJvbGxlciAnTGFuZGluZ0N0cmwnLCAoJHNjb3BlKSAtPlxuXG4gICAgJHNjb3BlLmhpZ2hsaWdodE1lbnUgPSAtPlxuICAgICAgJHNjb3BlLiRlbWl0ICdtZW51LmhpZ2hsaWdodCdcbiAgICAgIHJldHVyblxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name mobileApp.directive:preloader
    * @description
    * # preloader
   */
  angular.module('vrchallengeioApp').directive('preloader', [
    '$timeout',
    'Preloader',
    'Environment',
    function ($timeout, Preloader, Environment) {
      var HIDING_TIME, MIN_LOADING_TIME;
      MIN_LOADING_TIME = Environment.name === 'local' ? 0 : 3000;
      HIDING_TIME = 1000;
      return {
        templateUrl: 'partials/preloader.html',
        restrict: 'A',
        link: function (scope, element, attrs) {
          var loadingStartTime;
          scope.preloaded = false;
          scope.loaded = false;
          loadingStartTime = new Date().getTime();
          Preloader.preloadInitial().then(function () {
            scope.preloaded = true;
            Preloader.loadMain().then(function () {
              var loadingTime;
              loadingTime = new Date().getTime() - loadingStartTime;
              $timeout(function () {
                scope.loaded = true;
                return $timeout(function () {
                  scope.hidden = true;
                  return element.remove();
                }, HIDING_TIME);
              }, Math.max(MIN_LOADING_TIME - loadingTime, 0));
            });
          });
          return element;
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvcHJlbG9hZGVyLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7S0FGQTtBQUFBLEVBUUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsU0FESCxDQUNhLFdBRGIsRUFDMEIsU0FBQyxRQUFELEVBQVcsU0FBWCxFQUFzQixXQUF0QixHQUFBO0FBRXRCLFFBQUEsNkJBQUE7QUFBQSxJQUFBLGdCQUFBLEdBQXNCLFdBQVcsQ0FBQyxJQUFaLEtBQW9CLE9BQXZCLEdBQW9DLENBQXBDLEdBQTJDLElBQTlELENBQUE7QUFBQSxJQUNBLFdBQUEsR0FBYyxJQURkLENBQUE7V0FHQTtBQUFBLE1BQUEsV0FBQSxFQUFhLHlCQUFiO0FBQUEsTUFDQSxRQUFBLEVBQVUsR0FEVjtBQUFBLE1BR0EsSUFBQSxFQUFNLFNBQUMsS0FBRCxFQUFRLE9BQVIsRUFBaUIsS0FBakIsR0FBQTtBQUNKLFlBQUEsZ0JBQUE7QUFBQSxRQUFBLEtBQUssQ0FBQyxTQUFOLEdBQWtCLEtBQWxCLENBQUE7QUFBQSxRQUNBLEtBQUssQ0FBQyxNQUFOLEdBQWUsS0FEZixDQUFBO0FBQUEsUUFFQSxnQkFBQSxHQUF1QixJQUFBLElBQUEsQ0FBQSxDQUFNLENBQUMsT0FBUCxDQUFBLENBRnZCLENBQUE7QUFBQSxRQUlBLFNBQVMsQ0FBQyxjQUFWLENBQUEsQ0FBMEIsQ0FBQyxJQUEzQixDQUFnQyxTQUFBLEdBQUE7QUFDOUIsVUFBQSxLQUFLLENBQUMsU0FBTixHQUFrQixJQUFsQixDQUFBO0FBQUEsVUFDQSxTQUFTLENBQUMsUUFBVixDQUFBLENBQW9CLENBQUMsSUFBckIsQ0FBMEIsU0FBQSxHQUFBO0FBQ3hCLGdCQUFBLFdBQUE7QUFBQSxZQUFBLFdBQUEsR0FBa0IsSUFBQSxJQUFBLENBQUEsQ0FBTSxDQUFDLE9BQVAsQ0FBQSxDQUFKLEdBQXVCLGdCQUFyQyxDQUFBO0FBQUEsWUFDQSxRQUFBLENBQVMsU0FBQSxHQUFBO0FBQ1AsY0FBQSxLQUFLLENBQUMsTUFBTixHQUFlLElBQWYsQ0FBQTtxQkFDQSxRQUFBLENBQVMsU0FBQSxHQUFBO0FBQ1AsZ0JBQUEsS0FBSyxDQUFDLE1BQU4sR0FBZSxJQUFmLENBQUE7dUJBQ0EsT0FBTyxDQUFDLE1BQVIsQ0FBQSxFQUZPO2NBQUEsQ0FBVCxFQUdFLFdBSEYsRUFGTztZQUFBLENBQVQsRUFNRSxJQUFJLENBQUMsR0FBTCxDQUFTLGdCQUFBLEdBQW1CLFdBQTVCLEVBQXlDLENBQXpDLENBTkYsQ0FEQSxDQUR3QjtVQUFBLENBQTFCLENBREEsQ0FEOEI7UUFBQSxDQUFoQyxDQUpBLENBQUE7ZUFrQkEsUUFuQkk7TUFBQSxDQUhOO01BTHNCO0VBQUEsQ0FEMUIsQ0FSQSxDQUFBO0FBQUEiLCJmaWxlIjoiZGlyZWN0aXZlcy9wcmVsb2FkZXIuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGRpcmVjdGl2ZVxuICMgQG5hbWUgbW9iaWxlQXBwLmRpcmVjdGl2ZTpwcmVsb2FkZXJcbiAjIEBkZXNjcmlwdGlvblxuICMgIyBwcmVsb2FkZXJcbiMjI1xuYW5ndWxhci5tb2R1bGUoJ3ZyY2hhbGxlbmdlaW9BcHAnKVxuICAuZGlyZWN0aXZlKCdwcmVsb2FkZXInLCAoJHRpbWVvdXQsIFByZWxvYWRlciwgRW52aXJvbm1lbnQpIC0+XG5cbiAgICBNSU5fTE9BRElOR19USU1FID0gaWYgRW52aXJvbm1lbnQubmFtZSA9PSAnbG9jYWwnIHRoZW4gMCBlbHNlIDMwMDBcbiAgICBISURJTkdfVElNRSA9IDEwMDBcblxuICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvcHJlbG9hZGVyLmh0bWwnXG4gICAgcmVzdHJpY3Q6ICdBJ1xuXG4gICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgLT5cbiAgICAgIHNjb3BlLnByZWxvYWRlZCA9IGZhbHNlXG4gICAgICBzY29wZS5sb2FkZWQgPSBmYWxzZVxuICAgICAgbG9hZGluZ1N0YXJ0VGltZSA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpXG5cbiAgICAgIFByZWxvYWRlci5wcmVsb2FkSW5pdGlhbCgpLnRoZW4gKCkgLT5cbiAgICAgICAgc2NvcGUucHJlbG9hZGVkID0gdHJ1ZVxuICAgICAgICBQcmVsb2FkZXIubG9hZE1haW4oKS50aGVuICgpIC0+XG4gICAgICAgICAgbG9hZGluZ1RpbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIGxvYWRpbmdTdGFydFRpbWVcbiAgICAgICAgICAkdGltZW91dCAoKSAtPlxuICAgICAgICAgICAgc2NvcGUubG9hZGVkID0gdHJ1ZVxuICAgICAgICAgICAgJHRpbWVvdXQgKCkgLT5cbiAgICAgICAgICAgICAgc2NvcGUuaGlkZGVuID0gdHJ1ZVxuICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZSgpXG4gICAgICAgICAgICAsIEhJRElOR19USU1FXG4gICAgICAgICAgLCBNYXRoLm1heChNSU5fTE9BRElOR19USU1FIC0gbG9hZGluZ1RpbWUsIDApXG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgIHJldHVyblxuXG4gICAgICBlbGVtZW50XG4gIClcbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:languageSelector
    * @description
    * # languageSelector
   */
  angular.module('vrchallengeioApp').directive('languageSelector', [
    '$translate',
    '$interval',
    function ($translate, $interval) {
      return {
        restrict: 'A',
        templateUrl: 'partials/language-selector.html',
        controller: [
          '$scope',
          '$element',
          '$attrs',
          '$transclude',
          function ($scope, $element, $attrs, $transclude) {
            var changePromise;
            changePromise = null;
            $translate.use('pl');
            $scope.languageSelector = {
              current: $translate.use(),
              languages: [
                {
                  name: 'POL',
                  code: 'pl'
                },
                {
                  name: 'ENG',
                  code: 'en'
                }
              ]
            };
            $scope.selectLanguage = function (code) {
              $translate.use(code);
              $translate.use(code);
              $scope.languageSelector.current = $translate.use();
            };
            if ($scope.languageSelector.current === void 0) {
              changePromise = $interval(function () {
                if ($translate.use() !== void 0) {
                  $scope.languageSelector.current = $translate.use();
                  $interval.cancel(changePromise);
                }
              }, 200);
            }
          }
        ],
        link: function (scope, element, attrs) {
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbGFuZ3VhZ2Utc2VsZWN0b3IuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7OztLQUZBO0FBQUEsRUFRQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxTQURILENBQ2Esa0JBRGIsRUFDaUMsU0FBQyxVQUFELEVBQWEsU0FBYixHQUFBO1dBQzdCO0FBQUEsTUFBQSxRQUFBLEVBQVUsR0FBVjtBQUFBLE1BQ0EsV0FBQSxFQUFhLGlDQURiO0FBQUEsTUFHQSxVQUFBLEVBQVksU0FBQyxNQUFELEVBQVMsUUFBVCxFQUFtQixNQUFuQixFQUEyQixXQUEzQixHQUFBO0FBQ1YsWUFBQSxhQUFBO0FBQUEsUUFBQSxhQUFBLEdBQWdCLElBQWhCLENBQUE7QUFBQSxRQUVBLFVBQVUsQ0FBQyxHQUFYLENBQWUsSUFBZixDQUZBLENBQUE7QUFBQSxRQUlBLE1BQU0sQ0FBQyxnQkFBUCxHQUNFO0FBQUEsVUFBQSxPQUFBLEVBQVMsVUFBVSxDQUFDLEdBQVgsQ0FBQSxDQUFUO0FBQUEsVUFDQSxTQUFBLEVBQVc7WUFDVDtBQUFBLGNBQUMsSUFBQSxFQUFNLEtBQVA7QUFBQSxjQUFjLElBQUEsRUFBTSxJQUFwQjthQURTLEVBRVQ7QUFBQSxjQUFDLElBQUEsRUFBTSxLQUFQO0FBQUEsY0FBYyxJQUFBLEVBQU0sSUFBcEI7YUFGUztXQURYO1NBTEYsQ0FBQTtBQUFBLFFBV0EsTUFBTSxDQUFDLGNBQVAsR0FBd0IsU0FBQyxJQUFELEdBQUE7QUFDdEIsVUFBQSxVQUFVLENBQUMsR0FBWCxDQUFlLElBQWYsQ0FBQSxDQUFBO0FBQUEsVUFDQSxVQUFVLENBQUMsR0FBWCxDQUFlLElBQWYsQ0FEQSxDQUFBO0FBQUEsVUFFQSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBeEIsR0FBa0MsVUFBVSxDQUFDLEdBQVgsQ0FBQSxDQUZsQyxDQURzQjtRQUFBLENBWHhCLENBQUE7QUFrQkEsUUFBQSxJQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUF4QixLQUFtQyxNQUF0QztBQUNFLFVBQUEsYUFBQSxHQUFnQixTQUFBLENBQVUsU0FBQSxHQUFBO0FBQ3hCLFlBQUEsSUFBRyxVQUFVLENBQUMsR0FBWCxDQUFBLENBQUEsS0FBb0IsTUFBdkI7QUFDRSxjQUFBLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUF4QixHQUFrQyxVQUFVLENBQUMsR0FBWCxDQUFBLENBQWxDLENBQUE7QUFBQSxjQUNBLFNBQVMsQ0FBQyxNQUFWLENBQWlCLGFBQWpCLENBREEsQ0FERjthQUR3QjtVQUFBLENBQVYsRUFLZCxHQUxjLENBQWhCLENBREY7U0FuQlU7TUFBQSxDQUhaO0FBQUEsTUFnQ0EsSUFBQSxFQUFNLFNBQUMsS0FBRCxFQUFRLE9BQVIsRUFBaUIsS0FBakIsR0FBQSxDQWhDTjtNQUQ2QjtFQUFBLENBRGpDLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvbGFuZ3VhZ2Utc2VsZWN0b3IuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGRpcmVjdGl2ZVxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5kaXJlY3RpdmU6bGFuZ3VhZ2VTZWxlY3RvclxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIGxhbmd1YWdlU2VsZWN0b3JcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5kaXJlY3RpdmUgJ2xhbmd1YWdlU2VsZWN0b3InLCAoJHRyYW5zbGF0ZSwgJGludGVydmFsKSAtPlxuICAgIHJlc3RyaWN0OiAnQSdcbiAgICB0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2xhbmd1YWdlLXNlbGVjdG9yLmh0bWwnXG5cbiAgICBjb250cm9sbGVyOiAoJHNjb3BlLCAkZWxlbWVudCwgJGF0dHJzLCAkdHJhbnNjbHVkZSkgLT5cbiAgICAgIGNoYW5nZVByb21pc2UgPSBudWxsXG5cbiAgICAgICR0cmFuc2xhdGUudXNlICdwbCdcblxuICAgICAgJHNjb3BlLmxhbmd1YWdlU2VsZWN0b3IgPVxuICAgICAgICBjdXJyZW50OiAkdHJhbnNsYXRlLnVzZSgpXG4gICAgICAgIGxhbmd1YWdlczogW1xuICAgICAgICAgIHtuYW1lOiAnUE9MJywgY29kZTogJ3BsJ31cbiAgICAgICAgICB7bmFtZTogJ0VORycsIGNvZGU6ICdlbid9XG4gICAgICAgIF1cblxuICAgICAgJHNjb3BlLnNlbGVjdExhbmd1YWdlID0gKGNvZGUpIC0+XG4gICAgICAgICR0cmFuc2xhdGUudXNlIGNvZGVcbiAgICAgICAgJHRyYW5zbGF0ZS51c2UgY29kZVxuICAgICAgICAkc2NvcGUubGFuZ3VhZ2VTZWxlY3Rvci5jdXJyZW50ID0gJHRyYW5zbGF0ZS51c2UoKVxuICAgICAgICByZXR1cm5cblxuICAgICAgIyBXYWl0IGZvciB0aGUgbGFuZ3VhZ2UgdG8gYmUgbG9hZGVkLlxuICAgICAgaWYgJHNjb3BlLmxhbmd1YWdlU2VsZWN0b3IuY3VycmVudCA9PSB1bmRlZmluZWRcbiAgICAgICAgY2hhbmdlUHJvbWlzZSA9ICRpbnRlcnZhbCAtPlxuICAgICAgICAgIGlmICR0cmFuc2xhdGUudXNlKCkgIT0gdW5kZWZpbmVkXG4gICAgICAgICAgICAkc2NvcGUubGFuZ3VhZ2VTZWxlY3Rvci5jdXJyZW50ID0gJHRyYW5zbGF0ZS51c2UoKVxuICAgICAgICAgICAgJGludGVydmFsLmNhbmNlbCBjaGFuZ2VQcm9taXNlXG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgICwgMjAwXG5cbiAgICAgIHJldHVyblxuXG4gICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgLT5cbiAgICAgIHJldHVyblxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:ngDynamicController
    * @description
    * # ngDynamicController
   */
  angular.module('vrchallengeioApp').directive('ngDynamicController', [
    '$compile',
    function ($compile) {
      return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
          var controllerName;
          controllerName = scope.$eval(attrs.ngDynamicController);
          element.removeAttr('ng-dynamic-controller');
          element.attr('ng-controller', controllerName);
          return $compile(element)(scope);
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbmctZHluYW1pYy1jb250cm9sbGVyLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7S0FGQTtBQUFBLEVBUUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsU0FESCxDQUNhLHFCQURiLEVBQ29DLFNBQUMsUUFBRCxHQUFBO1dBQ2hDO0FBQUEsTUFBQSxRQUFBLEVBQVUsR0FBVjtBQUFBLE1BQ0EsS0FBQSxFQUFPLElBRFA7QUFBQSxNQUVBLElBQUEsRUFBTSxTQUFDLEtBQUQsRUFBUSxPQUFSLEVBQWlCLEtBQWpCLEdBQUE7QUFDSixZQUFBLGNBQUE7QUFBQSxRQUFBLGNBQUEsR0FBaUIsS0FBSyxDQUFDLEtBQU4sQ0FBWSxLQUFLLENBQUMsbUJBQWxCLENBQWpCLENBQUE7QUFBQSxRQUNBLE9BQU8sQ0FBQyxVQUFSLENBQW1CLHVCQUFuQixDQURBLENBQUE7QUFBQSxRQUVBLE9BQU8sQ0FBQyxJQUFSLENBQWEsZUFBYixFQUE4QixjQUE5QixDQUZBLENBQUE7ZUFHQSxRQUFBLENBQVMsT0FBVCxDQUFBLENBQWtCLEtBQWxCLEVBSkk7TUFBQSxDQUZOO01BRGdDO0VBQUEsQ0FEcEMsQ0FSQSxDQUFBO0FBQUEiLCJmaWxlIjoiZGlyZWN0aXZlcy9uZy1keW5hbWljLWNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGRpcmVjdGl2ZVxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5kaXJlY3RpdmU6bmdEeW5hbWljQ29udHJvbGxlclxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIG5nRHluYW1pY0NvbnRyb2xsZXJcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5kaXJlY3RpdmUgJ25nRHluYW1pY0NvbnRyb2xsZXInLCAoJGNvbXBpbGUpIC0+XG4gICAgcmVzdHJpY3Q6ICdBJ1xuICAgIHNjb3BlOiB0cnVlXG4gICAgbGluazogKHNjb3BlLCBlbGVtZW50LCBhdHRycykgLT5cbiAgICAgIGNvbnRyb2xsZXJOYW1lID0gc2NvcGUuJGV2YWwgYXR0cnMubmdEeW5hbWljQ29udHJvbGxlclxuICAgICAgZWxlbWVudC5yZW1vdmVBdHRyICduZy1keW5hbWljLWNvbnRyb2xsZXInXG4gICAgICBlbGVtZW50LmF0dHIgJ25nLWNvbnRyb2xsZXInLCBjb250cm9sbGVyTmFtZVxuICAgICAgJGNvbXBpbGUoZWxlbWVudCkoc2NvcGUpXG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name mobileApp.preloader
    * @description
    * # preloader
    * Service in the mobileApp.
   */
  angular.module('vrchallengeioApp').service('Preloader', [
    '$q',
    function ($q) {
      var generateInitialManifest, generateLoadQueue, generateMainManifest, getAssetUrl, getAssetUrls, getAssetsPackageUrl, getPlatform, getResolution, hasStartedLoading, isLoaded, loader, loadingPromise;
      loader = null;
      isLoaded = false;
      hasStartedLoading = false;
      loadingPromise = null;
      this.hasStartedLoading = function () {
        return hasStartedLoading;
      };
      this.isLoaded = function () {
        return isLoaded;
      };
      this.preloadInitial = function () {
        var deferred, queue;
        deferred = $q.defer();
        console.log('preloading initial');
        queue = generateLoadQueue();
        queue.on('complete', function () {
          console.log('initial loaded');
          deferred.resolve();
        });
        queue.loadManifest(generateInitialManifest());
        return deferred.promise;
      };
      this.loadMain = function () {
        var deferred, e, queue;
        if (hasStartedLoading) {
          return loadingPromise;
        }
        console.log('loading main');
        hasStartedLoading = true;
        deferred = $q.defer();
        queue = generateLoadQueue();
        queue.on('complete', function () {
          console.log('main loaded');
          isLoaded = true;
          deferred.resolve();
        });
        try {
          queue.loadManifest(generateMainManifest());
        } catch (_error) {
          e = _error;
          console.log('error', e);
        }
        loadingPromise = deferred.promise;
        return loadingPromise;
      };
      generateLoadQueue = function (manifest) {
        var queue;
        queue = new createjs.LoadQueue(true, '', true);
        return queue;
      };
      generateInitialManifest = function () {
        return getAssetUrls('essential', []).concat([]);
      };
      generateMainManifest = function () {
        return getAssetUrls('main', []).concat([getAssetsPackageUrl('main')]);
      };
      getResolution = function () {
        if ($('.detection .display-retina').is(':visible')) {
          return '2x';
        } else {
          return '1x';
        }
      };
      getPlatform = function () {
        if ($('.detection .desktop').is(':visible')) {
          return 'desktop';
        } else if ($('.detection .tablet').is(':visible')) {
          return 'tablet';
        }
        return 'mobile';
      };
      getAssetUrls = function (packageName, assetNames) {
        return assetNames.map(function (assetName) {
          return getAssetUrl(packageName, assetName);
        });
      };
      getAssetUrl = function (packageName, assetName) {
        return 'images/' + packageName + '/' + getPlatform() + '/' + packageName + '-' + getPlatform() + '-' + getResolution() + '/' + assetName;
      };
      getAssetsPackageUrl = function (packageName) {
        var i, j, mapUrl, match, maxj, rule, self, _i, _j, _ref;
        self = this;
        rule = null;
        mapUrl = null;
        maxj = null;
        for (i = _i = 0, _ref = document.styleSheets.length; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          if (mapUrl) {
            break;
          }
          if (document.styleSheets[i]) {
            if (document.styleSheets[i].cssRules) {
              maxj = document.styleSheets[i].cssRules.length;
            } else {
              maxj = document.styleSheets[i].rules.length;
            }
            for (j = _j = 0; 0 <= maxj ? _j <= maxj : _j >= maxj; j = 0 <= maxj ? ++_j : --_j) {
              if (document.styleSheets[i].cssRules) {
                rule = document.styleSheets[i].cssRules[j];
              } else {
                rule = document.styleSheets[i].rules[j];
              }
              if (rule && rule.selectorText === '.detection .url-' + packageName) {
                if (rule.cssText) {
                  mapUrl = rule.cssText.match('url(.*)')[0];
                } else {
                  mapUrl = rule.style.content;
                }
                mapUrl = mapUrl.substring(mapUrl.indexOf('img/'), mapUrl.indexOf(')')).replace('\'', '').replace('"', '');
                break;
              }
            }
          }
        }
        match = mapUrl ? mapUrl.match(/url\((.+)/) : null;
        if (match) {
          mapUrl = match[1];
          if (mapUrl.indexOf('..') === 0) {
            mapUrl = mapUrl.substring(2);
          }
          return mapUrl;
        }
        return null;
      };
      return this;
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL3ByZWxvYWRlci5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsRUFTQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxPQURILENBQ1csV0FEWCxFQUN3QixTQUFDLEVBQUQsR0FBQTtBQUlwQixRQUFBLGlNQUFBO0FBQUEsSUFBQSxNQUFBLEdBQVMsSUFBVCxDQUFBO0FBQUEsSUFDQSxRQUFBLEdBQVcsS0FEWCxDQUFBO0FBQUEsSUFFQSxpQkFBQSxHQUFvQixLQUZwQixDQUFBO0FBQUEsSUFHQSxjQUFBLEdBQWlCLElBSGpCLENBQUE7QUFBQSxJQU1BLElBQUMsQ0FBQSxpQkFBRCxHQUFxQixTQUFBLEdBQUE7YUFBTSxrQkFBTjtJQUFBLENBTnJCLENBQUE7QUFBQSxJQVNBLElBQUMsQ0FBQSxRQUFELEdBQVksU0FBQSxHQUFBO2FBQU0sU0FBTjtJQUFBLENBVFosQ0FBQTtBQUFBLElBWUEsSUFBQyxDQUFBLGNBQUQsR0FBa0IsU0FBQSxHQUFBO0FBQ2hCLFVBQUEsZUFBQTtBQUFBLE1BQUEsUUFBQSxHQUFXLEVBQUUsQ0FBQyxLQUFILENBQUEsQ0FBWCxDQUFBO0FBQUEsTUFDQSxPQUFPLENBQUMsR0FBUixDQUFZLG9CQUFaLENBREEsQ0FBQTtBQUFBLE1BRUEsS0FBQSxHQUFRLGlCQUFBLENBQUEsQ0FGUixDQUFBO0FBQUEsTUFHQSxLQUFLLENBQUMsRUFBTixDQUFTLFVBQVQsRUFBcUIsU0FBQSxHQUFBO0FBQ25CLFFBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxnQkFBWixDQUFBLENBQUE7QUFBQSxRQUNBLFFBQVEsQ0FBQyxPQUFULENBQUEsQ0FEQSxDQURtQjtNQUFBLENBQXJCLENBSEEsQ0FBQTtBQUFBLE1BT0EsS0FBSyxDQUFDLFlBQU4sQ0FBbUIsdUJBQUEsQ0FBQSxDQUFuQixDQVBBLENBQUE7QUFRQSxhQUFPLFFBQVEsQ0FBQyxPQUFoQixDQVRnQjtJQUFBLENBWmxCLENBQUE7QUFBQSxJQXdCQSxJQUFDLENBQUEsUUFBRCxHQUFZLFNBQUEsR0FBQTtBQUNWLFVBQUEsa0JBQUE7QUFBQSxNQUFBLElBQUcsaUJBQUg7QUFDRSxlQUFPLGNBQVAsQ0FERjtPQUFBO0FBQUEsTUFFQSxPQUFPLENBQUMsR0FBUixDQUFZLGNBQVosQ0FGQSxDQUFBO0FBQUEsTUFHQSxpQkFBQSxHQUFvQixJQUhwQixDQUFBO0FBQUEsTUFJQSxRQUFBLEdBQVcsRUFBRSxDQUFDLEtBQUgsQ0FBQSxDQUpYLENBQUE7QUFBQSxNQUtBLEtBQUEsR0FBUSxpQkFBQSxDQUFBLENBTFIsQ0FBQTtBQUFBLE1BTUEsS0FBSyxDQUFDLEVBQU4sQ0FBUyxVQUFULEVBQXFCLFNBQUEsR0FBQTtBQUNuQixRQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksYUFBWixDQUFBLENBQUE7QUFBQSxRQUNBLFFBQUEsR0FBVyxJQURYLENBQUE7QUFBQSxRQUVBLFFBQVEsQ0FBQyxPQUFULENBQUEsQ0FGQSxDQURtQjtNQUFBLENBQXJCLENBTkEsQ0FBQTtBQVdBO0FBQ0UsUUFBQSxLQUFLLENBQUMsWUFBTixDQUFtQixvQkFBQSxDQUFBLENBQW5CLENBQUEsQ0FERjtPQUFBLGNBQUE7QUFHRSxRQURJLFVBQ0osQ0FBQTtBQUFBLFFBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxPQUFaLEVBQXFCLENBQXJCLENBQUEsQ0FIRjtPQVhBO0FBQUEsTUFlQSxjQUFBLEdBQWlCLFFBQVEsQ0FBQyxPQWYxQixDQUFBO0FBZ0JBLGFBQU8sY0FBUCxDQWpCVTtJQUFBLENBeEJaLENBQUE7QUFBQSxJQTRDQSxpQkFBQSxHQUFvQixTQUFDLFFBQUQsR0FBQTtBQUNsQixVQUFBLEtBQUE7QUFBQSxNQUFBLEtBQUEsR0FBWSxJQUFBLFFBQVEsQ0FBQyxTQUFULENBQW1CLElBQW5CLEVBQXlCLEVBQXpCLEVBQTZCLElBQTdCLENBQVosQ0FBQTtBQUNBLGFBQU8sS0FBUCxDQUZrQjtJQUFBLENBNUNwQixDQUFBO0FBQUEsSUFpREEsdUJBQUEsR0FBMEIsU0FBQSxHQUFBO2FBQ3hCLFlBQUEsQ0FBYSxXQUFiLEVBQTBCLEVBQTFCLENBR0EsQ0FBQyxNQUhELENBR1EsRUFIUixFQUR3QjtJQUFBLENBakQxQixDQUFBO0FBQUEsSUEwREEsb0JBQUEsR0FBdUIsU0FBQSxHQUFBO2FBQ3JCLFlBQUEsQ0FBYSxNQUFiLEVBQXFCLEVBQXJCLENBR0EsQ0FBQyxNQUhELENBR1EsQ0FDTixtQkFBQSxDQUFvQixNQUFwQixDQURNLENBSFIsRUFEcUI7SUFBQSxDQTFEdkIsQ0FBQTtBQUFBLElBbUVBLGFBQUEsR0FBZ0IsU0FBQSxHQUFBO0FBQ1AsTUFBQSxJQUFHLENBQUEsQ0FBRSw0QkFBRixDQUErQixDQUFDLEVBQWhDLENBQW1DLFVBQW5DLENBQUg7ZUFBdUQsS0FBdkQ7T0FBQSxNQUFBO2VBQ0YsS0FERTtPQURPO0lBQUEsQ0FuRWhCLENBQUE7QUFBQSxJQXdFQSxXQUFBLEdBQWMsU0FBQSxHQUFBO0FBQ1osTUFBQSxJQUFHLENBQUEsQ0FBRSxxQkFBRixDQUF3QixDQUFDLEVBQXpCLENBQTRCLFVBQTVCLENBQUg7QUFDRSxlQUFPLFNBQVAsQ0FERjtPQUFBLE1BRUssSUFBRyxDQUFBLENBQUUsb0JBQUYsQ0FBdUIsQ0FBQyxFQUF4QixDQUEyQixVQUEzQixDQUFIO0FBQ0gsZUFBTyxRQUFQLENBREc7T0FGTDtBQUlBLGFBQU8sUUFBUCxDQUxZO0lBQUEsQ0F4RWQsQ0FBQTtBQUFBLElBZ0ZBLFlBQUEsR0FBZSxTQUFDLFdBQUQsRUFBYyxVQUFkLEdBQUE7QUFDYixhQUFPLFVBQVUsQ0FBQyxHQUFYLENBQWUsU0FBQyxTQUFELEdBQUE7QUFDcEIsZUFBTyxXQUFBLENBQVksV0FBWixFQUF5QixTQUF6QixDQUFQLENBRG9CO01BQUEsQ0FBZixDQUFQLENBRGE7SUFBQSxDQWhGZixDQUFBO0FBQUEsSUFxRkEsV0FBQSxHQUFjLFNBQUMsV0FBRCxFQUFjLFNBQWQsR0FBQTtBQUNaLGFBQU8sU0FBQSxHQUFZLFdBQVosR0FBMEIsR0FBMUIsR0FBZ0MsV0FBQSxDQUFBLENBQWhDLEdBQWdELEdBQWhELEdBQXNELFdBQXRELEdBQ1AsR0FETyxHQUNELFdBQUEsQ0FBQSxDQURDLEdBQ2UsR0FEZixHQUNxQixhQUFBLENBQUEsQ0FEckIsR0FDdUMsR0FEdkMsR0FDNkMsU0FEcEQsQ0FEWTtJQUFBLENBckZkLENBQUE7QUFBQSxJQTBGQSxtQkFBQSxHQUFzQixTQUFDLFdBQUQsR0FBQTtBQUNwQixVQUFBLG1EQUFBO0FBQUEsTUFBQSxJQUFBLEdBQU8sSUFBUCxDQUFBO0FBQUEsTUFDQSxJQUFBLEdBQU8sSUFEUCxDQUFBO0FBQUEsTUFFQSxNQUFBLEdBQVMsSUFGVCxDQUFBO0FBQUEsTUFHQSxJQUFBLEdBQU8sSUFIUCxDQUFBO0FBSUEsV0FBUyxnSEFBVCxHQUFBO0FBQ0UsUUFBQSxJQUFHLE1BQUg7QUFDRSxnQkFERjtTQUFBO0FBRUEsUUFBQSxJQUFHLFFBQVEsQ0FBQyxXQUFZLENBQUEsQ0FBQSxDQUF4QjtBQUNFLFVBQUEsSUFBRyxRQUFRLENBQUMsV0FBWSxDQUFBLENBQUEsQ0FBRSxDQUFDLFFBQTNCO0FBQ0UsWUFBQSxJQUFBLEdBQU8sUUFBUSxDQUFDLFdBQVksQ0FBQSxDQUFBLENBQUUsQ0FBQyxRQUFRLENBQUMsTUFBeEMsQ0FERjtXQUFBLE1BQUE7QUFHRSxZQUFBLElBQUEsR0FBTyxRQUFRLENBQUMsV0FBWSxDQUFBLENBQUEsQ0FBRSxDQUFDLEtBQUssQ0FBQyxNQUFyQyxDQUhGO1dBQUE7QUFJQSxlQUFTLDRFQUFULEdBQUE7QUFDRSxZQUFBLElBQUcsUUFBUSxDQUFDLFdBQVksQ0FBQSxDQUFBLENBQUUsQ0FBQyxRQUEzQjtBQUNFLGNBQUEsSUFBQSxHQUFPLFFBQVEsQ0FBQyxXQUFZLENBQUEsQ0FBQSxDQUFFLENBQUMsUUFBUyxDQUFBLENBQUEsQ0FBeEMsQ0FERjthQUFBLE1BQUE7QUFHRSxjQUFBLElBQUEsR0FBTyxRQUFRLENBQUMsV0FBWSxDQUFBLENBQUEsQ0FBRSxDQUFDLEtBQU0sQ0FBQSxDQUFBLENBQXJDLENBSEY7YUFBQTtBQUlBLFlBQUEsSUFBRyxJQUFBLElBQVEsSUFBSSxDQUFDLFlBQUwsS0FBcUIsa0JBQUEsR0FBcUIsV0FBckQ7QUFDRSxjQUFBLElBQUcsSUFBSSxDQUFDLE9BQVI7QUFDRSxnQkFBQSxNQUFBLEdBQVMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFiLENBQW1CLFNBQW5CLENBQThCLENBQUEsQ0FBQSxDQUF2QyxDQURGO2VBQUEsTUFBQTtBQUdFLGdCQUFBLE1BQUEsR0FBUyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQXBCLENBSEY7ZUFBQTtBQUFBLGNBSUEsTUFBQSxHQUFTLE1BQU0sQ0FBQyxTQUFQLENBQWlCLE1BQU0sQ0FBQyxPQUFQLENBQWUsTUFBZixDQUFqQixFQUNULE1BQU0sQ0FBQyxPQUFQLENBQWUsR0FBZixDQURTLENBQ1csQ0FBQyxPQURaLENBQ29CLElBRHBCLEVBQzBCLEVBRDFCLENBQzZCLENBQUMsT0FEOUIsQ0FDc0MsR0FEdEMsRUFDMkMsRUFEM0MsQ0FKVCxDQUFBO0FBTUEsb0JBUEY7YUFMRjtBQUFBLFdBTEY7U0FIRjtBQUFBLE9BSkE7QUFBQSxNQXlCQSxLQUFBLEdBQVcsTUFBSCxHQUFlLE1BQU0sQ0FBQyxLQUFQLENBQWEsV0FBYixDQUFmLEdBQThDLElBekJ0RCxDQUFBO0FBMEJBLE1BQUEsSUFBRyxLQUFIO0FBQ0UsUUFBQSxNQUFBLEdBQVMsS0FBTSxDQUFBLENBQUEsQ0FBZixDQUFBO0FBQ0EsUUFBQSxJQUFHLE1BQU0sQ0FBQyxPQUFQLENBQWUsSUFBZixDQUFBLEtBQXdCLENBQTNCO0FBQ0UsVUFBQSxNQUFBLEdBQVMsTUFBTSxDQUFDLFNBQVAsQ0FBaUIsQ0FBakIsQ0FBVCxDQURGO1NBREE7QUFHQSxlQUFPLE1BQVAsQ0FKRjtPQTFCQTtBQStCQSxhQUFPLElBQVAsQ0FoQ29CO0lBQUEsQ0ExRnRCLENBQUE7V0E2SEEsS0FqSW9CO0VBQUEsQ0FEeEIsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoic2VydmljZXMvcHJlbG9hZGVyLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBzZXJ2aWNlXG4gIyBAbmFtZSBtb2JpbGVBcHAucHJlbG9hZGVyXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgcHJlbG9hZGVyXG4gIyBTZXJ2aWNlIGluIHRoZSBtb2JpbGVBcHAuXG4jIyNcbmFuZ3VsYXIubW9kdWxlKCd2cmNoYWxsZW5nZWlvQXBwJylcbiAgLnNlcnZpY2UgJ1ByZWxvYWRlcicsICgkcSkgLT5cbiAgICAjIENvbnN0YW50c1xuXG4gICAgIyBQcml2YXRlXG4gICAgbG9hZGVyID0gbnVsbFxuICAgIGlzTG9hZGVkID0gZmFsc2VcbiAgICBoYXNTdGFydGVkTG9hZGluZyA9IGZhbHNlXG4gICAgbG9hZGluZ1Byb21pc2UgPSBudWxsXG5cbiAgICAjIFB1YmxpY1xuICAgIEBoYXNTdGFydGVkTG9hZGluZyA9ICgpIC0+IGhhc1N0YXJ0ZWRMb2FkaW5nXG5cblxuICAgIEBpc0xvYWRlZCA9ICgpIC0+IGlzTG9hZGVkXG5cblxuICAgIEBwcmVsb2FkSW5pdGlhbCA9ICgpIC0+XG4gICAgICBkZWZlcnJlZCA9ICRxLmRlZmVyKClcbiAgICAgIGNvbnNvbGUubG9nICdwcmVsb2FkaW5nIGluaXRpYWwnXG4gICAgICBxdWV1ZSA9IGdlbmVyYXRlTG9hZFF1ZXVlKClcbiAgICAgIHF1ZXVlLm9uICdjb21wbGV0ZScsICgpIC0+XG4gICAgICAgIGNvbnNvbGUubG9nICdpbml0aWFsIGxvYWRlZCdcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSgpXG4gICAgICAgIHJldHVyblxuICAgICAgcXVldWUubG9hZE1hbmlmZXN0IGdlbmVyYXRlSW5pdGlhbE1hbmlmZXN0KClcbiAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlXG5cblxuICAgIEBsb2FkTWFpbiA9ICgpIC0+XG4gICAgICBpZiBoYXNTdGFydGVkTG9hZGluZ1xuICAgICAgICByZXR1cm4gbG9hZGluZ1Byb21pc2VcbiAgICAgIGNvbnNvbGUubG9nICdsb2FkaW5nIG1haW4nXG4gICAgICBoYXNTdGFydGVkTG9hZGluZyA9IHRydWVcbiAgICAgIGRlZmVycmVkID0gJHEuZGVmZXIoKVxuICAgICAgcXVldWUgPSBnZW5lcmF0ZUxvYWRRdWV1ZSgpXG4gICAgICBxdWV1ZS5vbiAnY29tcGxldGUnLCAoKSAtPlxuICAgICAgICBjb25zb2xlLmxvZyAnbWFpbiBsb2FkZWQnXG4gICAgICAgIGlzTG9hZGVkID0gdHJ1ZVxuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKClcbiAgICAgICAgcmV0dXJuXG4gICAgICB0cnlcbiAgICAgICAgcXVldWUubG9hZE1hbmlmZXN0IGdlbmVyYXRlTWFpbk1hbmlmZXN0KClcbiAgICAgIGNhdGNoIGVcbiAgICAgICAgY29uc29sZS5sb2cgJ2Vycm9yJywgZVxuICAgICAgbG9hZGluZ1Byb21pc2UgPSBkZWZlcnJlZC5wcm9taXNlXG4gICAgICByZXR1cm4gbG9hZGluZ1Byb21pc2VcblxuXG4gICAgZ2VuZXJhdGVMb2FkUXVldWUgPSAobWFuaWZlc3QpIC0+XG4gICAgICBxdWV1ZSA9IG5ldyBjcmVhdGVqcy5Mb2FkUXVldWUgdHJ1ZSwgJycsIHRydWVcbiAgICAgIHJldHVybiBxdWV1ZVxuXG5cbiAgICBnZW5lcmF0ZUluaXRpYWxNYW5pZmVzdCA9ICgpIC0+XG4gICAgICBnZXRBc3NldFVybHMgJ2Vzc2VudGlhbCcsIFtcbiAgICAgICAgIyAnc29tZS1qcGVnLWJlY2F1c2Utbm90LWluLXNwcml0ZXNoZWV0LmpwZydcbiAgICAgIF1cbiAgICAgIC5jb25jYXQgW1xuICAgICAgICAjIGdldEFzc2V0c1BhY2thZ2VVcmwgJ2Vzc2VudGlhbCdcbiAgICAgIF1cblxuXG4gICAgZ2VuZXJhdGVNYWluTWFuaWZlc3QgPSAoKSAtPlxuICAgICAgZ2V0QXNzZXRVcmxzICdtYWluJywgW1xuICAgICAgICAjICdzb21lLWpwZWctYmVjYXVzZS1ub3QtaW4tc3ByaXRlc2hlZXQuanBnJ1xuICAgICAgXVxuICAgICAgLmNvbmNhdCBbXG4gICAgICAgIGdldEFzc2V0c1BhY2thZ2VVcmwgJ21haW4nXG4gICAgICBdXG5cblxuICAgIGdldFJlc29sdXRpb24gPSAoKSAtPlxuICAgICAgcmV0dXJuIGlmICQoJy5kZXRlY3Rpb24gLmRpc3BsYXktcmV0aW5hJykuaXMoJzp2aXNpYmxlJykgdGhlbiAnMngnXG4gICAgICBlbHNlICcxeCdcblxuXG4gICAgZ2V0UGxhdGZvcm0gPSAoKSAtPlxuICAgICAgaWYgJCgnLmRldGVjdGlvbiAuZGVza3RvcCcpLmlzKCc6dmlzaWJsZScpXG4gICAgICAgIHJldHVybiAnZGVza3RvcCdcbiAgICAgIGVsc2UgaWYgJCgnLmRldGVjdGlvbiAudGFibGV0JykuaXMoJzp2aXNpYmxlJylcbiAgICAgICAgcmV0dXJuICd0YWJsZXQnXG4gICAgICByZXR1cm4gJ21vYmlsZSdcblxuXG4gICAgZ2V0QXNzZXRVcmxzID0gKHBhY2thZ2VOYW1lLCBhc3NldE5hbWVzKSAtPlxuICAgICAgcmV0dXJuIGFzc2V0TmFtZXMubWFwIChhc3NldE5hbWUpIC0+XG4gICAgICAgIHJldHVybiBnZXRBc3NldFVybCBwYWNrYWdlTmFtZSwgYXNzZXROYW1lXG5cblxuICAgIGdldEFzc2V0VXJsID0gKHBhY2thZ2VOYW1lLCBhc3NldE5hbWUpIC0+XG4gICAgICByZXR1cm4gJ2ltYWdlcy8nICsgcGFja2FnZU5hbWUgKyAnLycgKyBnZXRQbGF0Zm9ybSgpICsgJy8nICsgcGFja2FnZU5hbWUgK1xuICAgICAgJy0nICsgZ2V0UGxhdGZvcm0oKSArICctJyArIGdldFJlc29sdXRpb24oKSArICcvJyArIGFzc2V0TmFtZVxuXG5cbiAgICBnZXRBc3NldHNQYWNrYWdlVXJsID0gKHBhY2thZ2VOYW1lKSAtPlxuICAgICAgc2VsZiA9IEBcbiAgICAgIHJ1bGUgPSBudWxsXG4gICAgICBtYXBVcmwgPSBudWxsXG4gICAgICBtYXhqID0gbnVsbFxuICAgICAgZm9yIGkgaW4gWzAgLi4gZG9jdW1lbnQuc3R5bGVTaGVldHMubGVuZ3RoXVxuICAgICAgICBpZiBtYXBVcmxcbiAgICAgICAgICBicmVha1xuICAgICAgICBpZiBkb2N1bWVudC5zdHlsZVNoZWV0c1tpXVxuICAgICAgICAgIGlmIGRvY3VtZW50LnN0eWxlU2hlZXRzW2ldLmNzc1J1bGVzXG4gICAgICAgICAgICBtYXhqID0gZG9jdW1lbnQuc3R5bGVTaGVldHNbaV0uY3NzUnVsZXMubGVuZ3RoXG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgbWF4aiA9IGRvY3VtZW50LnN0eWxlU2hlZXRzW2ldLnJ1bGVzLmxlbmd0aFxuICAgICAgICAgIGZvciBqIGluIFswIC4uIG1heGpdXG4gICAgICAgICAgICBpZiBkb2N1bWVudC5zdHlsZVNoZWV0c1tpXS5jc3NSdWxlc1xuICAgICAgICAgICAgICBydWxlID0gZG9jdW1lbnQuc3R5bGVTaGVldHNbaV0uY3NzUnVsZXNbal1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgcnVsZSA9IGRvY3VtZW50LnN0eWxlU2hlZXRzW2ldLnJ1bGVzW2pdXG4gICAgICAgICAgICBpZiBydWxlICYmIHJ1bGUuc2VsZWN0b3JUZXh0ID09ICcuZGV0ZWN0aW9uIC51cmwtJyArIHBhY2thZ2VOYW1lXG4gICAgICAgICAgICAgIGlmIHJ1bGUuY3NzVGV4dFxuICAgICAgICAgICAgICAgIG1hcFVybCA9IHJ1bGUuY3NzVGV4dC5tYXRjaCgndXJsKC4qKScpWzBdXG4gICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBtYXBVcmwgPSBydWxlLnN0eWxlLmNvbnRlbnRcbiAgICAgICAgICAgICAgbWFwVXJsID0gbWFwVXJsLnN1YnN0cmluZyhtYXBVcmwuaW5kZXhPZignaW1nLycpLFxuICAgICAgICAgICAgICBtYXBVcmwuaW5kZXhPZignKScpKS5yZXBsYWNlKCdcXCcnLCAnJykucmVwbGFjZSgnXCInLCAnJylcbiAgICAgICAgICAgICAgYnJlYWtcbiAgICAgIG1hdGNoID0gaWYgbWFwVXJsIHRoZW4gbWFwVXJsLm1hdGNoKC91cmxcXCgoLispLykgZWxzZSBudWxsXG4gICAgICBpZiBtYXRjaFxuICAgICAgICBtYXBVcmwgPSBtYXRjaFsxXVxuICAgICAgICBpZiBtYXBVcmwuaW5kZXhPZignLi4nKSA9PSAwXG4gICAgICAgICAgbWFwVXJsID0gbWFwVXJsLnN1YnN0cmluZygyKVxuICAgICAgICByZXR1cm4gbWFwVXJsXG4gICAgICByZXR1cm4gbnVsbFxuXG5cbiAgICBAICMgUmV0dXJuIHNlcnZpY2UgaW5zdGFuY2VcbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name mobileApp.access
    * @description
    * # access
    * Service in the mobileApp.
   */
  var __indexOf = [].indexOf || function (item) {
      for (var i = 0, l = this.length; i < l; i++) {
        if (i in this && this[i] === item)
          return i;
      }
      return -1;
    };
  angular.module('vrchallengeioApp').service('Access', [
    '$q',
    '$state',
    'Environment',
    function ($q, $state, Environment) {
      /**
     * The state to go to if a requested state is not allowed.
     * @type {String}
     */
      var AUTO_GRANT_WHEN_STATE_GO, DEFAULT_ALLOWED_STATES, FALLBACK_STATE, FALLBACK_URL, granted, originalStateGo, self;
      FALLBACK_STATE = 'landing';
      /**
     * For some reason redirecting to a fallback state sometimes doesn't work.
     * We use a native browser redirect in such case.
     * @type {String}
     */
      FALLBACK_URL = '/';
      /**
     * Default allowed states.
     * @type {Array}
     */
      DEFAULT_ALLOWED_STATES = ['*'];
      /**
     * If true, new states will be automatically granted when using $state.go.
     * @type {Boolean}
     */
      AUTO_GRANT_WHEN_STATE_GO = true;
      /**
     * List of granted states.
     * @type {Array}
     */
      granted = DEFAULT_ALLOWED_STATES.concat([]);
      /**
     * $state.go override.
     */
      if (AUTO_GRANT_WHEN_STATE_GO) {
        originalStateGo = $state.go;
        self = this;
        $state.go = function (state) {
          self.grant(state);
          return originalStateGo.apply(this, arguments);
        };
      }
      /**
     * Requires access to a specific state and returns a promise that will be
     * resolved based on whether the state is accessible or not.
     * @param  {String} state The state being required.
     * @return {Promise}      The promise to be resolved based on whether the
     * state is accessible or not.
     */
      this.require = function (state) {
        var deferred;
        deferred = $q.defer();
        if (this.has(state || __indexOf.call(DEFAULT_ALLOWED_STATES, state) >= 0)) {
          deferred.resolve();
        } else {
          deferred.reject();
          window.location.href = FALLBACK_URL;
        }
        return deferred.promise;
      };
      /**
     * Checks whether access to a specific step is allowed.
     * @param  {String}  state State to which access check is requested.
     * @return {Boolean}       [description]
     */
      this.has = function (state) {
        return granted.indexOf(state) !== -1 || granted.indexOf('*') !== -1;
      };
      /**
     * Grants access to a specific state.
     * @param  {String} state State to which the access is being granted.
     */
      this.grant = function (state) {
        if (!this.has(state)) {
          granted.push(state);
        }
      };
      /**
     * Restricts access to a specific state. If undefined, restricts access to
     * all states.
     * @param  {String} state State to which access is being restricted.
     */
      this.restrict = function (state) {
        var index;
        if (state) {
          index = granted.indexOf(state);
          if (index !== -1) {
            granted.splice(index, 1);
          }
        } else {
          granted = [];
        }
      };
      /**
     * Restricts allowed states to the default ones.
     */
      this.reset = function () {
        granted = DEFAULT_ALLOWED_STATES.concat([]);
      };
      return this;
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FjY2Vzcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsTUFBQSxxSkFBQTs7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLE9BREgsQ0FDVyxRQURYLEVBQ3FCLFNBQUMsRUFBRCxFQUFLLE1BQUwsRUFBYSxXQUFiLEdBQUE7QUFFakI7QUFBQTs7O09BQUE7QUFBQSxRQUFBLDhHQUFBO0FBQUEsSUFJQSxjQUFBLEdBQWlCLFNBSmpCLENBQUE7QUFNQTtBQUFBOzs7O09BTkE7QUFBQSxJQVdBLFlBQUEsR0FBZSxHQVhmLENBQUE7QUFhQTtBQUFBOzs7T0FiQTtBQUFBLElBaUJBLHNCQUFBLEdBQXlCLENBQ3ZCLEdBRHVCLENBakJ6QixDQUFBO0FBcUJBO0FBQUE7OztPQXJCQTtBQUFBLElBeUJBLHdCQUFBLEdBQTJCLElBekIzQixDQUFBO0FBMkJBO0FBQUE7OztPQTNCQTtBQUFBLElBK0JBLE9BQUEsR0FBVSxzQkFBc0IsQ0FBQyxNQUF2QixDQUE4QixFQUE5QixDQS9CVixDQUFBO0FBa0NBO0FBQUE7O09BbENBO0FBcUNBLElBQUEsSUFBRyx3QkFBSDtBQUNFLE1BQUEsZUFBQSxHQUFrQixNQUFNLENBQUMsRUFBekIsQ0FBQTtBQUFBLE1BQ0EsSUFBQSxHQUFPLElBRFAsQ0FBQTtBQUFBLE1BRUEsTUFBTSxDQUFDLEVBQVAsR0FBWSxTQUFDLEtBQUQsR0FBQTtBQUNWLFFBQUEsSUFBSSxDQUFDLEtBQUwsQ0FBVyxLQUFYLENBQUEsQ0FBQTtlQUNBLGVBQWUsQ0FBQyxLQUFoQixDQUFzQixJQUF0QixFQUE0QixTQUE1QixFQUZVO01BQUEsQ0FGWixDQURGO0tBckNBO0FBNkNBO0FBQUE7Ozs7OztPQTdDQTtBQUFBLElBb0RBLElBQUMsQ0FBQSxPQUFELEdBQVcsU0FBQyxLQUFELEdBQUE7QUFDVCxVQUFBLFFBQUE7QUFBQSxNQUFBLFFBQUEsR0FBVyxFQUFFLENBQUMsS0FBSCxDQUFBLENBQVgsQ0FBQTtBQUNBLE1BQUEsSUFBRyxJQUFDLENBQUEsR0FBRCxDQUFLLEtBQUEsSUFBUyxlQUFTLHNCQUFULEVBQUEsS0FBQSxNQUFkLENBQUg7QUFDRSxRQUFBLFFBQVEsQ0FBQyxPQUFULENBQUEsQ0FBQSxDQURGO09BQUEsTUFBQTtBQUdFLFFBQUEsUUFBUSxDQUFDLE1BQVQsQ0FBQSxDQUFBLENBQUE7QUFBQSxRQUNBLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBaEIsR0FBdUIsWUFEdkIsQ0FIRjtPQURBO2FBTUEsUUFBUSxDQUFDLFFBUEE7SUFBQSxDQXBEWCxDQUFBO0FBOERBO0FBQUE7Ozs7T0E5REE7QUFBQSxJQW1FQSxJQUFDLENBQUEsR0FBRCxHQUFPLFNBQUMsS0FBRCxHQUFBO2FBQVcsT0FBTyxDQUFDLE9BQVIsQ0FBZ0IsS0FBaEIsQ0FBQSxLQUEwQixDQUFBLENBQTFCLElBQWdDLE9BQU8sQ0FBQyxPQUFSLENBQWdCLEdBQWhCLENBQUEsS0FBd0IsQ0FBQSxFQUFuRTtJQUFBLENBbkVQLENBQUE7QUFzRUE7QUFBQTs7O09BdEVBO0FBQUEsSUEwRUEsSUFBQyxDQUFBLEtBQUQsR0FBUyxTQUFDLEtBQUQsR0FBQTtBQUNQLE1BQUEsSUFBRyxDQUFBLElBQUUsQ0FBQSxHQUFELENBQUssS0FBTCxDQUFKO0FBQ0UsUUFBQSxPQUFPLENBQUMsSUFBUixDQUFhLEtBQWIsQ0FBQSxDQURGO09BRE87SUFBQSxDQTFFVCxDQUFBO0FBZ0ZBO0FBQUE7Ozs7T0FoRkE7QUFBQSxJQXFGQSxJQUFDLENBQUEsUUFBRCxHQUFZLFNBQUMsS0FBRCxHQUFBO0FBQ1YsVUFBQSxLQUFBO0FBQUEsTUFBQSxJQUFHLEtBQUg7QUFDRSxRQUFBLEtBQUEsR0FBUSxPQUFPLENBQUMsT0FBUixDQUFnQixLQUFoQixDQUFSLENBQUE7QUFDQSxRQUFBLElBQUcsS0FBQSxLQUFTLENBQUEsQ0FBWjtBQUNFLFVBQUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxLQUFmLEVBQXNCLENBQXRCLENBQUEsQ0FERjtTQUZGO09BQUEsTUFBQTtBQUtFLFFBQUEsT0FBQSxHQUFVLEVBQVYsQ0FMRjtPQURVO0lBQUEsQ0FyRlosQ0FBQTtBQStGQTtBQUFBOztPQS9GQTtBQUFBLElBa0dBLElBQUMsQ0FBQSxLQUFELEdBQVMsU0FBQSxHQUFBO0FBQ1AsTUFBQSxPQUFBLEdBQVUsc0JBQXNCLENBQUMsTUFBdkIsQ0FBOEIsRUFBOUIsQ0FBVixDQURPO0lBQUEsQ0FsR1QsQ0FBQTtXQXNHQSxLQXhHaUI7RUFBQSxDQURyQixDQVRBLENBQUE7QUFBQSIsImZpbGUiOiJzZXJ2aWNlcy9hY2Nlc3MuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIHNlcnZpY2VcbiAjIEBuYW1lIG1vYmlsZUFwcC5hY2Nlc3NcbiAjIEBkZXNjcmlwdGlvblxuICMgIyBhY2Nlc3NcbiAjIFNlcnZpY2UgaW4gdGhlIG1vYmlsZUFwcC5cbiMjI1xuYW5ndWxhci5tb2R1bGUoJ3ZyY2hhbGxlbmdlaW9BcHAnKVxuICAuc2VydmljZSAnQWNjZXNzJywgKCRxLCAkc3RhdGUsIEVudmlyb25tZW50KSAtPlxuXG4gICAgIyMjKlxuICAgICAqIFRoZSBzdGF0ZSB0byBnbyB0byBpZiBhIHJlcXVlc3RlZCBzdGF0ZSBpcyBub3QgYWxsb3dlZC5cbiAgICAgKiBAdHlwZSB7U3RyaW5nfVxuICAgICMjI1xuICAgIEZBTExCQUNLX1NUQVRFID0gJ2xhbmRpbmcnXG5cbiAgICAjIyMqXG4gICAgICogRm9yIHNvbWUgcmVhc29uIHJlZGlyZWN0aW5nIHRvIGEgZmFsbGJhY2sgc3RhdGUgc29tZXRpbWVzIGRvZXNuJ3Qgd29yay5cbiAgICAgKiBXZSB1c2UgYSBuYXRpdmUgYnJvd3NlciByZWRpcmVjdCBpbiBzdWNoIGNhc2UuXG4gICAgICogQHR5cGUge1N0cmluZ31cbiAgICAjIyNcbiAgICBGQUxMQkFDS19VUkwgPSAnLydcblxuICAgICMjIypcbiAgICAgKiBEZWZhdWx0IGFsbG93ZWQgc3RhdGVzLlxuICAgICAqIEB0eXBlIHtBcnJheX1cbiAgICAjIyNcbiAgICBERUZBVUxUX0FMTE9XRURfU1RBVEVTID0gW1xuICAgICAgJyonICMgVE9ETzogbWFrZSB0aGUgc2VydmljZSBtb3JlIHJlbGlhYmxlLiBGb3Igbm93IGFsbG93IGFsbCByb3V0ZXMuXG4gICAgXVxuXG4gICAgIyMjKlxuICAgICAqIElmIHRydWUsIG5ldyBzdGF0ZXMgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IGdyYW50ZWQgd2hlbiB1c2luZyAkc3RhdGUuZ28uXG4gICAgICogQHR5cGUge0Jvb2xlYW59XG4gICAgIyMjXG4gICAgQVVUT19HUkFOVF9XSEVOX1NUQVRFX0dPID0gdHJ1ZVxuXG4gICAgIyMjKlxuICAgICAqIExpc3Qgb2YgZ3JhbnRlZCBzdGF0ZXMuXG4gICAgICogQHR5cGUge0FycmF5fVxuICAgICMjI1xuICAgIGdyYW50ZWQgPSBERUZBVUxUX0FMTE9XRURfU1RBVEVTLmNvbmNhdCBbXVxuXG5cbiAgICAjIyMqXG4gICAgICogJHN0YXRlLmdvIG92ZXJyaWRlLlxuICAgICMjI1xuICAgIGlmIEFVVE9fR1JBTlRfV0hFTl9TVEFURV9HT1xuICAgICAgb3JpZ2luYWxTdGF0ZUdvID0gJHN0YXRlLmdvXG4gICAgICBzZWxmID0gQFxuICAgICAgJHN0YXRlLmdvID0gKHN0YXRlKSAtPlxuICAgICAgICBzZWxmLmdyYW50IHN0YXRlXG4gICAgICAgIG9yaWdpbmFsU3RhdGVHby5hcHBseSB0aGlzLCBhcmd1bWVudHNcblxuXG4gICAgIyMjKlxuICAgICAqIFJlcXVpcmVzIGFjY2VzcyB0byBhIHNwZWNpZmljIHN0YXRlIGFuZCByZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHdpbGwgYmVcbiAgICAgKiByZXNvbHZlZCBiYXNlZCBvbiB3aGV0aGVyIHRoZSBzdGF0ZSBpcyBhY2Nlc3NpYmxlIG9yIG5vdC5cbiAgICAgKiBAcGFyYW0gIHtTdHJpbmd9IHN0YXRlIFRoZSBzdGF0ZSBiZWluZyByZXF1aXJlZC5cbiAgICAgKiBAcmV0dXJuIHtQcm9taXNlfSAgICAgIFRoZSBwcm9taXNlIHRvIGJlIHJlc29sdmVkIGJhc2VkIG9uIHdoZXRoZXIgdGhlXG4gICAgICogc3RhdGUgaXMgYWNjZXNzaWJsZSBvciBub3QuXG4gICAgIyMjXG4gICAgQHJlcXVpcmUgPSAoc3RhdGUpIC0+XG4gICAgICBkZWZlcnJlZCA9ICRxLmRlZmVyKClcbiAgICAgIGlmIEBoYXMgc3RhdGUgfHwgc3RhdGUgaW4gREVGQVVMVF9BTExPV0VEX1NUQVRFU1xuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKClcbiAgICAgIGVsc2VcbiAgICAgICAgZGVmZXJyZWQucmVqZWN0KClcbiAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBGQUxMQkFDS19VUkxcbiAgICAgIGRlZmVycmVkLnByb21pc2VcblxuXG4gICAgIyMjKlxuICAgICAqIENoZWNrcyB3aGV0aGVyIGFjY2VzcyB0byBhIHNwZWNpZmljIHN0ZXAgaXMgYWxsb3dlZC5cbiAgICAgKiBAcGFyYW0gIHtTdHJpbmd9ICBzdGF0ZSBTdGF0ZSB0byB3aGljaCBhY2Nlc3MgY2hlY2sgaXMgcmVxdWVzdGVkLlxuICAgICAqIEByZXR1cm4ge0Jvb2xlYW59ICAgICAgIFtkZXNjcmlwdGlvbl1cbiAgICAjIyNcbiAgICBAaGFzID0gKHN0YXRlKSAtPiBncmFudGVkLmluZGV4T2Yoc3RhdGUpICE9IC0xIHx8IGdyYW50ZWQuaW5kZXhPZignKicpICE9IC0xXG5cblxuICAgICMjIypcbiAgICAgKiBHcmFudHMgYWNjZXNzIHRvIGEgc3BlY2lmaWMgc3RhdGUuXG4gICAgICogQHBhcmFtICB7U3RyaW5nfSBzdGF0ZSBTdGF0ZSB0byB3aGljaCB0aGUgYWNjZXNzIGlzIGJlaW5nIGdyYW50ZWQuXG4gICAgIyMjXG4gICAgQGdyYW50ID0gKHN0YXRlKSAtPlxuICAgICAgaWYgIUBoYXMgc3RhdGVcbiAgICAgICAgZ3JhbnRlZC5wdXNoIHN0YXRlXG4gICAgICByZXR1cm5cblxuXG4gICAgIyMjKlxuICAgICAqIFJlc3RyaWN0cyBhY2Nlc3MgdG8gYSBzcGVjaWZpYyBzdGF0ZS4gSWYgdW5kZWZpbmVkLCByZXN0cmljdHMgYWNjZXNzIHRvXG4gICAgICogYWxsIHN0YXRlcy5cbiAgICAgKiBAcGFyYW0gIHtTdHJpbmd9IHN0YXRlIFN0YXRlIHRvIHdoaWNoIGFjY2VzcyBpcyBiZWluZyByZXN0cmljdGVkLlxuICAgICMjI1xuICAgIEByZXN0cmljdCA9IChzdGF0ZSkgLT5cbiAgICAgIGlmIHN0YXRlXG4gICAgICAgIGluZGV4ID0gZ3JhbnRlZC5pbmRleE9mIHN0YXRlXG4gICAgICAgIGlmIGluZGV4ICE9IC0xXG4gICAgICAgICAgZ3JhbnRlZC5zcGxpY2UgaW5kZXgsIDFcbiAgICAgIGVsc2VcbiAgICAgICAgZ3JhbnRlZCA9IFtdXG4gICAgICByZXR1cm5cblxuXG4gICAgIyMjKlxuICAgICAqIFJlc3RyaWN0cyBhbGxvd2VkIHN0YXRlcyB0byB0aGUgZGVmYXVsdCBvbmVzLlxuICAgICMjI1xuICAgIEByZXNldCA9ICgpIC0+XG4gICAgICBncmFudGVkID0gREVGQVVMVF9BTExPV0VEX1NUQVRFUy5jb25jYXQgW11cbiAgICAgIHJldHVyblxuXG4gICAgQFxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name mobileApp.environment
    * @description
    * # environment
    * Service in the mobileApp.
   */
  angular.module('vrchallengeioApp').service('Environment', function () {
    var name;
    name = 'prod';
    switch (window.location.hostname) {
    case 'localhost':
      name = 'local';
      break;
    case '0.0.0.0':
      name = 'local';
      break;
    case '127.0.0.1':
      name = 'local';
    }
    this.name = name;
    this.isLocal = function () {
      return this.name === 'local';
    };
    return this;
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL2Vudmlyb25tZW50LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLE9BREgsQ0FDVyxhQURYLEVBQzBCLFNBQUEsR0FBQTtBQUV0QixRQUFBLElBQUE7QUFBQSxJQUFBLElBQUEsR0FBTyxNQUFQLENBQUE7QUFDQSxZQUFPLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBdkI7QUFBQSxXQUNPLFdBRFA7QUFDd0IsUUFBQSxJQUFBLEdBQU8sT0FBUCxDQUR4QjtBQUNPO0FBRFAsV0FFTyxTQUZQO0FBRXNCLFFBQUEsSUFBQSxHQUFPLE9BQVAsQ0FGdEI7QUFFTztBQUZQLFdBR08sV0FIUDtBQUd3QixRQUFBLElBQUEsR0FBTyxPQUFQLENBSHhCO0FBQUEsS0FEQTtBQUFBLElBTUEsSUFBQyxDQUFBLElBQUQsR0FBUSxJQU5SLENBQUE7QUFBQSxJQVFBLElBQUMsQ0FBQSxPQUFELEdBQVcsU0FBQSxHQUFBO2FBQU0sSUFBQyxDQUFBLElBQUQsS0FBUyxRQUFmO0lBQUEsQ0FSWCxDQUFBO1dBVUEsS0Fac0I7RUFBQSxDQUQxQixDQVRBLENBQUE7QUFBQSIsImZpbGUiOiJzZXJ2aWNlcy9lbnZpcm9ubWVudC5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2Mgc2VydmljZVxuICMgQG5hbWUgbW9iaWxlQXBwLmVudmlyb25tZW50XG4gIyBAZGVzY3JpcHRpb25cbiAjICMgZW52aXJvbm1lbnRcbiAjIFNlcnZpY2UgaW4gdGhlIG1vYmlsZUFwcC5cbiMjI1xuYW5ndWxhci5tb2R1bGUoJ3ZyY2hhbGxlbmdlaW9BcHAnKVxuICAuc2VydmljZSAnRW52aXJvbm1lbnQnLCAtPlxuXG4gICAgbmFtZSA9ICdwcm9kJ1xuICAgIHN3aXRjaCB3aW5kb3cubG9jYXRpb24uaG9zdG5hbWVcbiAgICAgIHdoZW4gJ2xvY2FsaG9zdCcgdGhlbiBuYW1lID0gJ2xvY2FsJ1xuICAgICAgd2hlbiAnMC4wLjAuMCcgdGhlbiBuYW1lID0gJ2xvY2FsJ1xuICAgICAgd2hlbiAnMTI3LjAuMC4xJyB0aGVuIG5hbWUgPSAnbG9jYWwnXG5cbiAgICBAbmFtZSA9IG5hbWVcblxuICAgIEBpc0xvY2FsID0gKCkgLT4gQG5hbWUgPT0gJ2xvY2FsJ1xuXG4gICAgQFxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name mobileApp.touch
    * @description
    * # touch
    * Service in the mobileApp.
   */
  angular.module('vrchallengeioApp').service('Touch', function () {
    this.isTouch = function () {
      return !!('ontouchstart' in window);
    };
    this.preventOverscroll = function () {
      $(window).on('touchmove', function (event) {
        event.preventDefault();
      });
    };
    return this;
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL3RvdWNoLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLE9BREgsQ0FDVyxPQURYLEVBQ29CLFNBQUEsR0FBQTtBQUVoQixJQUFBLElBQUMsQ0FBQSxPQUFELEdBQVcsU0FBQSxHQUFBO2FBQU0sQ0FBQSxDQUFDLENBQUUsY0FBQSxJQUFrQixNQUFuQixFQUFSO0lBQUEsQ0FBWCxDQUFBO0FBQUEsSUFFQSxJQUFDLENBQUEsaUJBQUQsR0FBcUIsU0FBQSxHQUFBO0FBQ25CLE1BQUEsQ0FBQSxDQUFFLE1BQUYsQ0FBUyxDQUFDLEVBQVYsQ0FBYSxXQUFiLEVBQTBCLFNBQUMsS0FBRCxHQUFBO0FBQ3hCLFFBQUEsS0FBSyxDQUFDLGNBQU4sQ0FBQSxDQUFBLENBRHdCO01BQUEsQ0FBMUIsQ0FBQSxDQURtQjtJQUFBLENBRnJCLENBQUE7V0FRQSxLQVZnQjtFQUFBLENBRHBCLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6InNlcnZpY2VzL3RvdWNoLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBzZXJ2aWNlXG4gIyBAbmFtZSBtb2JpbGVBcHAudG91Y2hcbiAjIEBkZXNjcmlwdGlvblxuICMgIyB0b3VjaFxuICMgU2VydmljZSBpbiB0aGUgbW9iaWxlQXBwLlxuIyMjXG5hbmd1bGFyLm1vZHVsZSgndnJjaGFsbGVuZ2Vpb0FwcCcpXG4gIC5zZXJ2aWNlICdUb3VjaCcsIC0+XG5cbiAgICBAaXNUb3VjaCA9ICgpIC0+ICEhKCdvbnRvdWNoc3RhcnQnIG9mIHdpbmRvdylcblxuICAgIEBwcmV2ZW50T3ZlcnNjcm9sbCA9ICgpIC0+XG4gICAgICAkKHdpbmRvdykub24gJ3RvdWNobW92ZScsIChldmVudCkgLT5cbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICByZXR1cm5cbiAgICAgIHJldHVyblxuXG4gICAgQFxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name mobileApp.version
    * @description
    * # version
    * Service in the mobileApp.
   */
  angular.module('vrchallengeioApp').service('Version', function () {
    this.info = {
      major: 0,
      minor: 55,
      patch: 0
    };
    this.date = 'Mon Jun 01 2015 20:00:42 GMT+0000 (UTC)';
    return this;
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL3ZlcnNpb24uY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsT0FESCxDQUNXLFNBRFgsRUFDc0IsU0FBQSxHQUFBO0FBR2xCLElBQUEsSUFBQyxDQUFBLElBQUQsR0FDRTtBQUFBLE1BQUEsS0FBQSxFQUFPLENBQVA7QUFBQSxNQUNBLEtBQUEsRUFBTyxFQURQO0FBQUEsTUFFQSxLQUFBLEVBQU8sQ0FGUDtLQURGLENBQUE7QUFBQSxJQUlBLElBQUMsQ0FBQSxJQUFELEdBQVEseUNBSlIsQ0FBQTtXQU9BLEtBVmtCO0VBQUEsQ0FEdEIsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoic2VydmljZXMvdmVyc2lvbi5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2Mgc2VydmljZVxuICMgQG5hbWUgbW9iaWxlQXBwLnZlcnNpb25cbiAjIEBkZXNjcmlwdGlvblxuICMgIyB2ZXJzaW9uXG4gIyBTZXJ2aWNlIGluIHRoZSBtb2JpbGVBcHAuXG4jIyNcbmFuZ3VsYXIubW9kdWxlKCd2cmNoYWxsZW5nZWlvQXBwJylcbiAgLnNlcnZpY2UgJ1ZlcnNpb24nLCAtPlxuXG4gICAgIyBAQHZlcnNpb24tYmVnaW5cbiAgICBAaW5mbyA9XG4gICAgICBtYWpvcjogMFxuICAgICAgbWlub3I6IDU1XG4gICAgICBwYXRjaDogMFxuICAgIEBkYXRlID0gJ01vbiBKdW4gMDEgMjAxNSAyMDowMDo0MiBHTVQrMDAwMCAoVVRDKSdcbiAgICAjIEBAdmVyc2lvbi1lbmRcblxuICAgIEBcbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name vrchallengeioApp.analytics
    * @description
    * # analytics
    * Service in the vrchallengeioApp.
   */
  angular.module('vrchallengeioApp').service('Analytics', function () {
    var GOOGLE_ACCOUNT_ID;
    GOOGLE_ACCOUNT_ID = 'UA-60369781-1';
    this.init = function () {
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments);
        };
        i[r].l = 1 * new Date();
        a = s.createElement(o);
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m);
        console.log('Google Analytics initialised', GOOGLE_ACCOUNT_ID);
      }(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga'));
      ga('create', GOOGLE_ACCOUNT_ID, 'auto');
      ga('send', 'pageview');
    };
    this.trackPageview = function (page) {
      ga('send', 'pageview', page);
    };
    this.trackEvent = function (category, action, label, value) {
      if (value == null) {
        value = 1;
      }
      ga('send', 'event', category, action, label, value);
    };
    return this;
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FuYWx5dGljcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsRUFTQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxPQURILENBQ1csV0FEWCxFQUN3QixTQUFBLEdBQUE7QUFFcEIsUUFBQSxpQkFBQTtBQUFBLElBQUEsaUJBQUEsR0FBb0IsZUFBcEIsQ0FBQTtBQUFBLElBR0EsSUFBQyxDQUFBLElBQUQsR0FBUSxTQUFBLEdBQUE7QUFDTixNQUFBLENBQUMsU0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLENBQVAsRUFBVSxDQUFWLEVBQWEsQ0FBYixFQUFnQixDQUFoQixFQUFtQixDQUFuQixHQUFBO0FBQ0MsUUFBQSxDQUFFLENBQUEsdUJBQUEsQ0FBRixHQUE2QixDQUE3QixDQUFBO0FBQUEsUUFDQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sQ0FBRSxDQUFBLENBQUEsQ0FBRixJQUFRLFNBQUEsR0FBQTtBQUNiLFVBQUEsQ0FBQyxDQUFFLENBQUEsQ0FBQSxDQUFFLENBQUMsQ0FBTCxHQUFTLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxDQUFMLElBQVUsRUFBcEIsQ0FBdUIsQ0FBQyxJQUF4QixDQUE2QixTQUE3QixDQUFBLENBRGE7UUFBQSxDQURmLENBQUE7QUFBQSxRQUtBLENBQUUsQ0FBQSxDQUFBLENBQUUsQ0FBQyxDQUFMLEdBQVMsQ0FBQSxHQUFJLEdBQUEsQ0FBQSxJQUxiLENBQUE7QUFBQSxRQU1BLENBQUEsR0FBSSxDQUFDLENBQUMsYUFBRixDQUFnQixDQUFoQixDQU5KLENBQUE7QUFBQSxRQU9BLENBQUEsR0FBSSxDQUFDLENBQUMsb0JBQUYsQ0FBdUIsQ0FBdkIsQ0FBMEIsQ0FBQSxDQUFBLENBUDlCLENBQUE7QUFBQSxRQVFBLENBQUMsQ0FBQyxLQUFGLEdBQVUsQ0FSVixDQUFBO0FBQUEsUUFTQSxDQUFDLENBQUMsR0FBRixHQUFRLENBVFIsQ0FBQTtBQUFBLFFBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxZQUFiLENBQTBCLENBQTFCLEVBQTZCLENBQTdCLENBVkEsQ0FBQTtBQUFBLFFBV0EsT0FBTyxDQUFDLEdBQVIsQ0FBWSw4QkFBWixFQUE0QyxpQkFBNUMsQ0FYQSxDQUREO01BQUEsQ0FBRCxDQUFBLENBY0UsTUFkRixFQWNVLFFBZFYsRUFjb0IsUUFkcEIsRUFjOEIseUNBZDlCLEVBZUEsSUFmQSxDQUFBLENBQUE7QUFBQSxNQWdCQSxFQUFBLENBQUcsUUFBSCxFQUFhLGlCQUFiLEVBQWdDLE1BQWhDLENBaEJBLENBQUE7QUFBQSxNQWlCQSxFQUFBLENBQUcsTUFBSCxFQUFXLFVBQVgsQ0FqQkEsQ0FETTtJQUFBLENBSFIsQ0FBQTtBQUFBLElBeUJBLElBQUMsQ0FBQSxhQUFELEdBQWlCLFNBQUMsSUFBRCxHQUFBO0FBQ2YsTUFBQSxFQUFBLENBQUcsTUFBSCxFQUFXLFVBQVgsRUFBdUIsSUFBdkIsQ0FBQSxDQURlO0lBQUEsQ0F6QmpCLENBQUE7QUFBQSxJQThCQSxJQUFDLENBQUEsVUFBRCxHQUFjLFNBQUMsUUFBRCxFQUFXLE1BQVgsRUFBbUIsS0FBbkIsRUFBMEIsS0FBMUIsR0FBQTs7UUFBMEIsUUFBTTtPQUM1QztBQUFBLE1BQUEsRUFBQSxDQUFHLE1BQUgsRUFBVyxPQUFYLEVBQW9CLFFBQXBCLEVBQThCLE1BQTlCLEVBQXNDLEtBQXRDLEVBQTZDLEtBQTdDLENBQUEsQ0FEWTtJQUFBLENBOUJkLENBQUE7V0FtQ0EsS0FyQ29CO0VBQUEsQ0FEeEIsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoic2VydmljZXMvYW5hbHl0aWNzLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBzZXJ2aWNlXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmFuYWx5dGljc1xuICMgQGRlc2NyaXB0aW9uXG4gIyAjIGFuYWx5dGljc1xuICMgU2VydmljZSBpbiB0aGUgdnJjaGFsbGVuZ2Vpb0FwcC5cbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5zZXJ2aWNlICdBbmFseXRpY3MnLCAtPlxuXG4gICAgR09PR0xFX0FDQ09VTlRfSUQgPSAnVUEtNjAzNjk3ODEtMSdcblxuXG4gICAgQGluaXQgPSAtPlxuICAgICAgKChpLCBzLCBvLCBnLCByLCBhLCBtKSAtPlxuICAgICAgICBpWydHb29nbGVBbmFseXRpY3NPYmplY3QnXSA9IHJcbiAgICAgICAgaVtyXSA9IGlbcl0gb3IgLT5cbiAgICAgICAgICAoaVtyXS5xID0gaVtyXS5xIG9yIFtdKS5wdXNoIGFyZ3VtZW50c1xuICAgICAgICAgIHJldHVyblxuXG4gICAgICAgIGlbcl0ubCA9IDEgKiBuZXcgRGF0ZVxuICAgICAgICBhID0gcy5jcmVhdGVFbGVtZW50KG8pXG4gICAgICAgIG0gPSBzLmdldEVsZW1lbnRzQnlUYWdOYW1lKG8pWzBdXG4gICAgICAgIGEuYXN5bmMgPSAxXG4gICAgICAgIGEuc3JjID0gZ1xuICAgICAgICBtLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlIGEsIG1cbiAgICAgICAgY29uc29sZS5sb2cgJ0dvb2dsZSBBbmFseXRpY3MgaW5pdGlhbGlzZWQnLCBHT09HTEVfQUNDT1VOVF9JRFxuICAgICAgICByZXR1cm5cbiAgICAgICkgd2luZG93LCBkb2N1bWVudCwgJ3NjcmlwdCcsICcvL3d3dy5nb29nbGUtYW5hbHl0aWNzLmNvbS9hbmFseXRpY3MuanMnLFxuICAgICAgJ2dhJ1xuICAgICAgZ2EgJ2NyZWF0ZScsIEdPT0dMRV9BQ0NPVU5UX0lELCAnYXV0bydcbiAgICAgIGdhICdzZW5kJywgJ3BhZ2V2aWV3J1xuICAgICAgcmV0dXJuXG5cblxuICAgIEB0cmFja1BhZ2V2aWV3ID0gKHBhZ2UpIC0+XG4gICAgICBnYSAnc2VuZCcsICdwYWdldmlldycsIHBhZ2VcbiAgICAgIHJldHVyblxuXG5cbiAgICBAdHJhY2tFdmVudCA9IChjYXRlZ29yeSwgYWN0aW9uLCBsYWJlbCwgdmFsdWU9MSkgLT5cbiAgICAgIGdhICdzZW5kJywgJ2V2ZW50JywgY2F0ZWdvcnksIGFjdGlvbiwgbGFiZWwsIHZhbHVlXG4gICAgICByZXR1cm5cblxuXG4gICAgQFxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc service
    * @name vrchallengeioApp.subscription
    * @description
    * # subscription
    * Service in the vrchallengeioApp.
   */
  angular.module('vrchallengeioApp').service('Subscription', [
    '$http',
    function ($http) {
      var API_URL, TYPE_DATA;
      API_URL = 'http://vrchallenge.io/lists/?p=subscribe&id={ID}';
      TYPE_DATA = {
        newsletter: {
          formId: 1,
          lists: [2]
        },
        contestant: {
          formId: 2,
          lists: [
            2,
            3
          ]
        }
      };
      this.TYPE_NEWSLETTER = 'newsletter';
      this.TYPE_CONTESTANT = 'contestant';
      this.make = function (type, email) {
        var data, listId, typeData, _i, _len, _ref;
        typeData = TYPE_DATA[type];
        if (!typeData) {
          throw new Error('invalid subscription type');
        }
        email = email.toLowerCase();
        data = {
          email: email,
          htmlemail: 1,
          VerificationCodeX: '',
          subscribe: 'Subscribe'
        };
        _ref = typeData.lists;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          listId = _ref[_i];
          console.log('subscribing to list', listId);
          data['list[' + listId + ']'] = 'signup';
        }
        console.log('subscription data', data);
        return $http({
          method: 'POST',
          url: API_URL.replace('{ID}', typeData.formId),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
          transformRequest: function (obj) {
            var p, str;
            str = [];
            for (p in obj) {
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          },
          data: data
        });
      };
      return this;
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2VzL3N1YnNjcmlwdGlvbi5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsRUFTQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxPQURILENBQ1csY0FEWCxFQUMyQixTQUFDLEtBQUQsR0FBQTtBQUV2QixRQUFBLGtCQUFBO0FBQUEsSUFBQSxPQUFBLEdBQVUsa0RBQVYsQ0FBQTtBQUFBLElBRUEsU0FBQSxHQUNFO0FBQUEsTUFBQSxVQUFBLEVBQ0U7QUFBQSxRQUFBLE1BQUEsRUFBUSxDQUFSO0FBQUEsUUFDQSxLQUFBLEVBQU8sQ0FBQyxDQUFELENBRFA7T0FERjtBQUFBLE1BR0EsVUFBQSxFQUNFO0FBQUEsUUFBQSxNQUFBLEVBQVEsQ0FBUjtBQUFBLFFBQ0EsS0FBQSxFQUFPLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FEUDtPQUpGO0tBSEYsQ0FBQTtBQUFBLElBVUEsSUFBQyxDQUFBLGVBQUQsR0FBbUIsWUFWbkIsQ0FBQTtBQUFBLElBV0EsSUFBQyxDQUFBLGVBQUQsR0FBbUIsWUFYbkIsQ0FBQTtBQUFBLElBY0EsSUFBQyxDQUFBLElBQUQsR0FBUSxTQUFDLElBQUQsRUFBTyxLQUFQLEdBQUE7QUFDTixVQUFBLHNDQUFBO0FBQUEsTUFBQSxRQUFBLEdBQVcsU0FBVSxDQUFBLElBQUEsQ0FBckIsQ0FBQTtBQUNBLE1BQUEsSUFBRyxDQUFBLFFBQUg7QUFDRSxjQUFVLElBQUEsS0FBQSxDQUFNLDJCQUFOLENBQVYsQ0FERjtPQURBO0FBQUEsTUFJQSxLQUFBLEdBQVEsS0FBSyxDQUFDLFdBQU4sQ0FBQSxDQUpSLENBQUE7QUFBQSxNQU1BLElBQUEsR0FDRTtBQUFBLFFBQUEsS0FBQSxFQUFPLEtBQVA7QUFBQSxRQUNBLFNBQUEsRUFBVyxDQURYO0FBQUEsUUFFQSxpQkFBQSxFQUFtQixFQUZuQjtBQUFBLFFBR0EsU0FBQSxFQUFXLFdBSFg7T0FQRixDQUFBO0FBWUE7QUFBQSxXQUFBLDJDQUFBOzBCQUFBO0FBQ0UsUUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHFCQUFaLEVBQW1DLE1BQW5DLENBQUEsQ0FBQTtBQUFBLFFBQ0EsSUFBSyxDQUFDLE9BQUEsR0FBTyxNQUFQLEdBQWMsR0FBZixDQUFMLEdBQTBCLFFBRDFCLENBREY7QUFBQSxPQVpBO0FBQUEsTUFnQkEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxtQkFBWixFQUFpQyxJQUFqQyxDQWhCQSxDQUFBO2FBa0JBLEtBQUEsQ0FBTTtBQUFBLFFBQ0osTUFBQSxFQUFRLE1BREo7QUFBQSxRQUVKLEdBQUEsRUFBSyxPQUFPLENBQUMsT0FBUixDQUFnQixNQUFoQixFQUF3QixRQUFRLENBQUMsTUFBakMsQ0FGRDtBQUFBLFFBR0osT0FBQSxFQUFTO0FBQUEsVUFBQyxjQUFBLEVBQWdCLG1DQUFqQjtTQUhMO0FBQUEsUUFJSixnQkFBQSxFQUFrQixTQUFDLEdBQUQsR0FBQTtBQUNoQixjQUFBLE1BQUE7QUFBQSxVQUFBLEdBQUEsR0FBTSxFQUFOLENBQUE7QUFDQSxlQUFBLFFBQUEsR0FBQTtBQUNFLFlBQUEsR0FBRyxDQUFDLElBQUosQ0FBUyxrQkFBQSxDQUFtQixDQUFuQixDQUFBLEdBQXdCLEdBQXhCLEdBQThCLGtCQUFBLENBQW1CLEdBQUksQ0FBQSxDQUFBLENBQXZCLENBQXZDLENBQUEsQ0FERjtBQUFBLFdBREE7QUFHQSxpQkFBTyxHQUFHLENBQUMsSUFBSixDQUFTLEdBQVQsQ0FBUCxDQUpnQjtRQUFBLENBSmQ7QUFBQSxRQVNKLElBQUEsRUFBTSxJQVRGO09BQU4sRUFuQk07SUFBQSxDQWRSLENBQUE7V0E4Q0EsS0FoRHVCO0VBQUEsQ0FEM0IsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoic2VydmljZXMvc3Vic2NyaXB0aW9uLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBzZXJ2aWNlXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLnN1YnNjcmlwdGlvblxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIHN1YnNjcmlwdGlvblxuICMgU2VydmljZSBpbiB0aGUgdnJjaGFsbGVuZ2Vpb0FwcC5cbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5zZXJ2aWNlICdTdWJzY3JpcHRpb24nLCAoJGh0dHApIC0+XG5cbiAgICBBUElfVVJMID0gJ2h0dHA6Ly92cmNoYWxsZW5nZS5pby9saXN0cy8/cD1zdWJzY3JpYmUmaWQ9e0lEfSdcblxuICAgIFRZUEVfREFUQSA9XG4gICAgICBuZXdzbGV0dGVyOlxuICAgICAgICBmb3JtSWQ6IDFcbiAgICAgICAgbGlzdHM6IFsyXVxuICAgICAgY29udGVzdGFudDpcbiAgICAgICAgZm9ybUlkOiAyXG4gICAgICAgIGxpc3RzOiBbMiwgM11cblxuICAgIEBUWVBFX05FV1NMRVRURVIgPSAnbmV3c2xldHRlcidcbiAgICBAVFlQRV9DT05URVNUQU5UID0gJ2NvbnRlc3RhbnQnXG5cblxuICAgIEBtYWtlID0gKHR5cGUsIGVtYWlsKSAtPlxuICAgICAgdHlwZURhdGEgPSBUWVBFX0RBVEFbdHlwZV1cbiAgICAgIGlmICF0eXBlRGF0YVxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IgJ2ludmFsaWQgc3Vic2NyaXB0aW9uIHR5cGUnXG5cbiAgICAgIGVtYWlsID0gZW1haWwudG9Mb3dlckNhc2UoKVxuXG4gICAgICBkYXRhID1cbiAgICAgICAgZW1haWw6IGVtYWlsXG4gICAgICAgIGh0bWxlbWFpbDogMVxuICAgICAgICBWZXJpZmljYXRpb25Db2RlWDogJydcbiAgICAgICAgc3Vic2NyaWJlOiAnU3Vic2NyaWJlJ1xuXG4gICAgICBmb3IgbGlzdElkIGluIHR5cGVEYXRhLmxpc3RzXG4gICAgICAgIGNvbnNvbGUubG9nICdzdWJzY3JpYmluZyB0byBsaXN0JywgbGlzdElkXG4gICAgICAgIGRhdGFbXCJsaXN0WyN7bGlzdElkfV1cIl0gPSAnc2lnbnVwJ1xuXG4gICAgICBjb25zb2xlLmxvZyAnc3Vic2NyaXB0aW9uIGRhdGEnLCBkYXRhXG5cbiAgICAgICRodHRwIHtcbiAgICAgICAgbWV0aG9kOiAnUE9TVCdcbiAgICAgICAgdXJsOiBBUElfVVJMLnJlcGxhY2UgJ3tJRH0nLCB0eXBlRGF0YS5mb3JtSWRcbiAgICAgICAgaGVhZGVyczogeydDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJ31cbiAgICAgICAgdHJhbnNmb3JtUmVxdWVzdDogKG9iaikgLT5cbiAgICAgICAgICBzdHIgPSBbXVxuICAgICAgICAgIGZvciBwIG9mIG9ialxuICAgICAgICAgICAgc3RyLnB1c2goZW5jb2RlVVJJQ29tcG9uZW50KHApICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9ialtwXSkpXG4gICAgICAgICAgcmV0dXJuIHN0ci5qb2luKCcmJylcbiAgICAgICAgZGF0YTogZGF0YVxuICAgICAgfVxuXG5cbiAgICBAXG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:logo
    * @description
    * # logo
   */
  angular.module('vrchallengeioApp').directive('logo', [
    '$rootScope',
    '$state',
    function ($rootScope, $state) {
      return {
        restrict: 'A',
        templateUrl: 'partials/logo.html',
        link: function (scope, element, attrs) {
          scope.logo = { shown: false };
          $rootScope.$on('$stateChangeSuccess', function () {
            return scope.logo.shown = $state.current.name !== 'landing';
          });
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbG9nby5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7O0tBRkE7QUFBQSxFQVFBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLFNBREgsQ0FDYSxNQURiLEVBQ3FCLFNBQUMsVUFBRCxFQUFhLE1BQWIsR0FBQTtXQUNqQjtBQUFBLE1BQUEsUUFBQSxFQUFVLEdBQVY7QUFBQSxNQUNBLFdBQUEsRUFBYSxvQkFEYjtBQUFBLE1BRUEsSUFBQSxFQUFNLFNBQUMsS0FBRCxFQUFRLE9BQVIsRUFBaUIsS0FBakIsR0FBQTtBQUVKLFFBQUEsS0FBSyxDQUFDLElBQU4sR0FDRTtBQUFBLFVBQUEsS0FBQSxFQUFPLEtBQVA7U0FERixDQUFBO0FBQUEsUUFJQSxVQUFVLENBQUMsR0FBWCxDQUFlLHFCQUFmLEVBQXNDLFNBQUEsR0FBQTtpQkFDcEMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFYLEdBQW1CLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBZixLQUF1QixVQUROO1FBQUEsQ0FBdEMsQ0FKQSxDQUZJO01BQUEsQ0FGTjtNQURpQjtFQUFBLENBRHJCLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvbG9nby5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2MgZGlyZWN0aXZlXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmRpcmVjdGl2ZTpsb2dvXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgbG9nb1xuIyMjXG5hbmd1bGFyLm1vZHVsZSAndnJjaGFsbGVuZ2Vpb0FwcCdcbiAgLmRpcmVjdGl2ZSAnbG9nbycsICgkcm9vdFNjb3BlLCAkc3RhdGUpIC0+XG4gICAgcmVzdHJpY3Q6ICdBJ1xuICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvbG9nby5odG1sJ1xuICAgIGxpbms6IChzY29wZSwgZWxlbWVudCwgYXR0cnMpIC0+XG5cbiAgICAgIHNjb3BlLmxvZ28gPVxuICAgICAgICBzaG93bjogZmFsc2VcblxuXG4gICAgICAkcm9vdFNjb3BlLiRvbiAnJHN0YXRlQ2hhbmdlU3VjY2VzcycsIC0+XG4gICAgICAgIHNjb3BlLmxvZ28uc2hvd24gPSAkc3RhdGUuY3VycmVudC5uYW1lICE9ICdsYW5kaW5nJ1xuXG5cbiAgICAgIHJldHVyblxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:backgroundEffects
    * @description
    * # backgroundEffects
   */
  angular.module('vrchallengeioApp').directive('backgroundEffects', [
    '$rootScope',
    '$state',
    function ($rootScope, $state) {
      return {
        restrict: 'A',
        templateUrl: 'partials/background-effects.html',
        link: function (scope, element, attrs) {
          var COLOR, DUST_AMOUNT, DUST_OFFSET, DUST_PIXEL_SIZE, DUST_SIZE, DUST_SPREAD, EASING, MAX_PARALLAX_ROTATION, TO_DEGREES, animate, bindEvents, cameraDust, cameraMesh, destParallaxRotation, destPosition, destRotation, dustContainer, init, initCamera, initLights, initModels, initRenderer, initScene, isWebGLSupported, lastFrameTime, lightDust, lightMesh, onDeviceOrientation, onMouseMove, onResize, onStateChange, render, rendererDust, rendererMesh, sceneDust, sceneMesh, sphere, update, updateDust, updateMesh;
          COLOR = 65469;
          DUST_PIXEL_SIZE = 3;
          DUST_SIZE = {
            x: 1,
            y: 1,
            z: 1
          };
          DUST_AMOUNT = 1000;
          DUST_OFFSET = {
            x: 0,
            y: 0,
            z: -200
          };
          DUST_SPREAD = {
            x: 1000,
            y: 300,
            z: 600
          };
          EASING = {
            position: 0.0015,
            rotation: 0.0015
          };
          TO_DEGREES = Math.PI / 180;
          MAX_PARALLAX_ROTATION = {
            x: 5,
            y: 5
          };
          rendererMesh = null;
          rendererDust = null;
          cameraMesh = null;
          cameraDust = null;
          sceneMesh = null;
          sceneDust = null;
          sphere = null;
          dustContainer = null;
          lightMesh = null;
          lightDust = null;
          lastFrameTime = -1;
          destPosition = {
            x: 0,
            y: 0,
            z: 0
          };
          destRotation = {
            x: 0,
            y: 0,
            z: 0
          };
          destParallaxRotation = {
            x: 0,
            y: 0,
            z: 0
          };
          init = function () {
            initScene();
            initCamera();
            initRenderer();
            initLights();
            initModels();
            bindEvents();
            onResize();
            animate();
          };
          isWebGLSupported = function () {
            var canvas, e, test1, test2, test3;
            try {
              canvas = document.createElement('canvas');
              test1 = !!window.WebGLRenderingContext;
              test2 = !!canvas.getContext('webgl');
              test3 = !!canvas.getContext('experimental-webgl');
              return test1 && (test2 || test3);
            } catch (_error) {
              e = _error;
              return false;
            }
            return false;
          };
          initScene = function () {
            sceneMesh = new THREE.Scene();
            sceneDust = new THREE.Scene();
          };
          initCamera = function () {
            cameraMesh = new THREE.PerspectiveCamera();
            cameraDust = new THREE.PerspectiveCamera();
            sceneMesh.add(cameraMesh);
            sceneDust.add(cameraDust);
            cameraMesh.position.z = 300;
            cameraDust.position.z = 300;
          };
          initRenderer = function () {
            if (isWebGLSupported()) {
              rendererMesh = new THREE.WebGLRenderer({
                alpha: true,
                antialiasing: true
              });
              rendererDust = new THREE.WebGLRenderer({
                alpha: true,
                antialiasing: true
              });
            } else {
              rendererMesh = new THREE.CanvasRenderer({
                alpha: true,
                antialiasing: true
              });
              rendererDust = new THREE.CanvasRenderer({
                alpha: true,
                antialiasing: true
              });
            }
            rendererMesh.setClearColor(0, 0);
            rendererDust.setClearColor(0, 0);
            element.append(rendererDust.domElement);
            element.append(rendererMesh.domElement);
            rendererMesh.domElement.style.position = 'absolute';
            rendererDust.domElement.style.position = 'absolute';
          };
          initLights = function () {
            lightMesh = new THREE.PointLight(16777215, 1, 5000);
            lightMesh.position.x = 0;
            lightMesh.position.y = 0;
            lightMesh.position.z = 500;
            sceneMesh.add(lightMesh);
            lightDust = new THREE.PointLight(16777215, 1, 5000);
            lightDust.position.x = 0;
            lightDust.position.y = 0;
            lightDust.position.z = 500;
            sceneDust.add(lightDust);
          };
          initModels = function () {
            var dust, i, _i;
            sphere = new THREE.Mesh(new THREE.SphereGeometry(200, 4, 4), new THREE.MeshLambertMaterial({
              wireframe: true,
              wireframeLinewidth: 5,
              color: COLOR,
              emissive: 2046779
            }));
            sphere.position.x = 0;
            sphere.position.y = 0;
            sceneMesh.add(sphere);
            dustContainer = new THREE.Object3D();
            dustContainer.rotation.x = 45;
            for (i = _i = 0; 0 <= DUST_AMOUNT ? _i <= DUST_AMOUNT : _i >= DUST_AMOUNT; i = 0 <= DUST_AMOUNT ? ++_i : --_i) {
              dust = new THREE.Mesh(new THREE.BoxGeometry(DUST_SIZE.x, DUST_SIZE.y, DUST_SIZE.z), new THREE.MeshLambertMaterial({
                color: COLOR,
                emissive: 2046779
              }));
              dust.position.x = (Math.pow(Math.random(), 2) - 0.5) * DUST_SPREAD.x + DUST_OFFSET.x;
              dust.position.y = (Math.pow(Math.random(), 2) - 0.5) * DUST_SPREAD.y + DUST_OFFSET.y;
              dust.position.z = (Math.pow(Math.random(), 2) - 0.5) * DUST_SPREAD.z + DUST_OFFSET.z;
              dustContainer.add(dust);
            }
            sceneDust.add(dustContainer);
          };
          bindEvents = function () {
            $rootScope.$on('$stateChangeSuccess', onStateChange);
            window.addEventListener('resize', onResize);
            if ('ondeviceorientation' in window && 'ontouchstart' in window) {
              window.addEventListener('deviceorientation', onDeviceOrientation);
            } else {
              window.addEventListener('mousemove', onMouseMove);
            }
          };
          updateMesh = function (dt) {
            var destRotationComplete;
            sphere.position.x += (destPosition.x - sphere.position.x) * EASING.position * dt;
            sphere.position.y += (destPosition.y - sphere.position.y) * EASING.position * dt;
            sphere.position.z += (destPosition.z - sphere.position.z) * EASING.position * dt;
            destRotationComplete = {
              x: destRotation.x + destParallaxRotation.x,
              y: destRotation.y + destParallaxRotation.y,
              z: destRotation.z + destParallaxRotation.z
            };
            sphere.rotation.x += (destRotationComplete.x * TO_DEGREES - sphere.rotation.x) * EASING.rotation * dt;
            sphere.rotation.y += (destRotationComplete.y * TO_DEGREES - sphere.rotation.y) * EASING.rotation * dt;
            sphere.rotation.z += (destRotationComplete.z * TO_DEGREES - sphere.rotation.z) * EASING.rotation * dt;
          };
          updateDust = function (dt) {
            cameraDust.position.x = cameraMesh.position.x;
            cameraDust.position.y = cameraMesh.position.y;
            cameraDust.position.z = cameraMesh.position.z;
            dustContainer.position.x = sphere.position.x;
            dustContainer.position.y = sphere.position.y;
            dustContainer.position.z = sphere.position.z;
            dustContainer.rotation.x = sphere.rotation.x;
            dustContainer.rotation.y = sphere.rotation.y;
            dustContainer.rotation.z = sphere.rotation.z;
          };
          update = function (dt) {
            updateMesh(dt);
            updateDust(dt);
          };
          render = function () {
            rendererDust.render(sceneDust, cameraDust);
            rendererMesh.render(sceneMesh, cameraMesh);
          };
          animate = function () {
            var now;
            requestAnimationFrame(animate);
            now = window.performance ? window.performance.now() : new Date().getTime();
            update(now - lastFrameTime);
            render();
            lastFrameTime = now;
          };
          onStateChange = function () {
            destPosition.x = $state.current.data.backgroundEffects.position.x;
            destPosition.y = $state.current.data.backgroundEffects.position.y;
            destPosition.z = $state.current.data.backgroundEffects.position.z;
            destRotation.x = $state.current.data.backgroundEffects.rotation.x;
            destRotation.y = $state.current.data.backgroundEffects.rotation.y;
            destRotation.z = $state.current.data.backgroundEffects.rotation.z;
          };
          onMouseMove = function (e) {
            destParallaxRotation.x = MAX_PARALLAX_ROTATION.x * (e.pageY - window.innerHeight * 0.5) / (window.innerHeight * 0.5);
            destParallaxRotation.y = MAX_PARALLAX_ROTATION.y * (e.pageX - window.innerWidth * 0.5) / (window.innerWidth * 0.5);
          };
          onDeviceOrientation = function (e) {
            destParallaxRotation.x = MAX_PARALLAX_ROTATION.x * e.beta / 30;
            destParallaxRotation.y = MAX_PARALLAX_ROTATION.y * e.gamma / 30;
          };
          onResize = function () {
            var height, width;
            width = window.innerWidth;
            height = window.innerHeight;
            rendererMesh.setSize(width, height);
            rendererDust.setSize(width, height);
            cameraMesh.aspect = width / height;
            cameraMesh.updateProjectionMatrix();
            cameraDust.aspect = width / height;
            cameraDust.updateProjectionMatrix();
          };
          init();
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvYmFja2dyb3VuZC1lZmZlY3RzLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7S0FGQTtBQUFBLEVBUUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsU0FESCxDQUNhLG1CQURiLEVBQ2tDLFNBQUMsVUFBRCxFQUFhLE1BQWIsR0FBQTtXQUM5QjtBQUFBLE1BQUEsUUFBQSxFQUFVLEdBQVY7QUFBQSxNQUNBLFdBQUEsRUFBYSxrQ0FEYjtBQUFBLE1BRUEsSUFBQSxFQUFNLFNBQUMsS0FBRCxFQUFRLE9BQVIsRUFBaUIsS0FBakIsR0FBQTtBQUVKLFlBQUEsd2ZBQUE7QUFBQSxRQUFBLEtBQUEsR0FBUSxRQUFSLENBQUE7QUFBQSxRQUNBLGVBQUEsR0FBa0IsQ0FEbEIsQ0FBQTtBQUFBLFFBRUEsU0FBQSxHQUNFO0FBQUEsVUFBQSxDQUFBLEVBQUcsQ0FBSDtBQUFBLFVBQ0EsQ0FBQSxFQUFHLENBREg7QUFBQSxVQUVBLENBQUEsRUFBRyxDQUZIO1NBSEYsQ0FBQTtBQUFBLFFBTUEsV0FBQSxHQUFjLElBTmQsQ0FBQTtBQUFBLFFBT0EsV0FBQSxHQUNFO0FBQUEsVUFBQSxDQUFBLEVBQUcsQ0FBSDtBQUFBLFVBQ0EsQ0FBQSxFQUFHLENBREg7QUFBQSxVQUVBLENBQUEsRUFBRyxDQUFBLEdBRkg7U0FSRixDQUFBO0FBQUEsUUFXQSxXQUFBLEdBQ0U7QUFBQSxVQUFBLENBQUEsRUFBRyxJQUFIO0FBQUEsVUFDQSxDQUFBLEVBQUcsR0FESDtBQUFBLFVBRUEsQ0FBQSxFQUFHLEdBRkg7U0FaRixDQUFBO0FBQUEsUUFnQkEsTUFBQSxHQUNFO0FBQUEsVUFBQSxRQUFBLEVBQVUsTUFBVjtBQUFBLFVBQ0EsUUFBQSxFQUFVLE1BRFY7U0FqQkYsQ0FBQTtBQUFBLFFBb0JBLFVBQUEsR0FBYSxJQUFJLENBQUMsRUFBTCxHQUFVLEdBcEJ2QixDQUFBO0FBQUEsUUFzQkEscUJBQUEsR0FDRTtBQUFBLFVBQUEsQ0FBQSxFQUFHLENBQUg7QUFBQSxVQUNBLENBQUEsRUFBRyxDQURIO1NBdkJGLENBQUE7QUFBQSxRQTBCQSxZQUFBLEdBQWUsSUExQmYsQ0FBQTtBQUFBLFFBMkJBLFlBQUEsR0FBZSxJQTNCZixDQUFBO0FBQUEsUUE0QkEsVUFBQSxHQUFhLElBNUJiLENBQUE7QUFBQSxRQTZCQSxVQUFBLEdBQWEsSUE3QmIsQ0FBQTtBQUFBLFFBOEJBLFNBQUEsR0FBWSxJQTlCWixDQUFBO0FBQUEsUUErQkEsU0FBQSxHQUFZLElBL0JaLENBQUE7QUFBQSxRQWdDQSxNQUFBLEdBQVMsSUFoQ1QsQ0FBQTtBQUFBLFFBaUNBLGFBQUEsR0FBZ0IsSUFqQ2hCLENBQUE7QUFBQSxRQWtDQSxTQUFBLEdBQVksSUFsQ1osQ0FBQTtBQUFBLFFBbUNBLFNBQUEsR0FBWSxJQW5DWixDQUFBO0FBQUEsUUFvQ0EsYUFBQSxHQUFnQixDQUFBLENBcENoQixDQUFBO0FBQUEsUUFxQ0EsWUFBQSxHQUNFO0FBQUEsVUFBQSxDQUFBLEVBQUcsQ0FBSDtBQUFBLFVBQ0EsQ0FBQSxFQUFHLENBREg7QUFBQSxVQUVBLENBQUEsRUFBRyxDQUZIO1NBdENGLENBQUE7QUFBQSxRQXlDQSxZQUFBLEdBQ0U7QUFBQSxVQUFBLENBQUEsRUFBRyxDQUFIO0FBQUEsVUFDQSxDQUFBLEVBQUcsQ0FESDtBQUFBLFVBRUEsQ0FBQSxFQUFHLENBRkg7U0ExQ0YsQ0FBQTtBQUFBLFFBNkNBLG9CQUFBLEdBQ0U7QUFBQSxVQUFBLENBQUEsRUFBRyxDQUFIO0FBQUEsVUFDQSxDQUFBLEVBQUcsQ0FESDtBQUFBLFVBRUEsQ0FBQSxFQUFHLENBRkg7U0E5Q0YsQ0FBQTtBQUFBLFFBbURBLElBQUEsR0FBTyxTQUFBLEdBQUE7QUFDTCxVQUFBLFNBQUEsQ0FBQSxDQUFBLENBQUE7QUFBQSxVQUNBLFVBQUEsQ0FBQSxDQURBLENBQUE7QUFBQSxVQUVBLFlBQUEsQ0FBQSxDQUZBLENBQUE7QUFBQSxVQUdBLFVBQUEsQ0FBQSxDQUhBLENBQUE7QUFBQSxVQUlBLFVBQUEsQ0FBQSxDQUpBLENBQUE7QUFBQSxVQUtBLFVBQUEsQ0FBQSxDQUxBLENBQUE7QUFBQSxVQU1BLFFBQUEsQ0FBQSxDQU5BLENBQUE7QUFBQSxVQU9BLE9BQUEsQ0FBQSxDQVBBLENBREs7UUFBQSxDQW5EUCxDQUFBO0FBQUEsUUErREEsZ0JBQUEsR0FBbUIsU0FBQSxHQUFBO0FBQ2pCLGNBQUEsOEJBQUE7QUFBQTtBQUNFLFlBQUEsTUFBQSxHQUFTLFFBQVEsQ0FBQyxhQUFULENBQXVCLFFBQXZCLENBQVQsQ0FBQTtBQUFBLFlBQ0EsS0FBQSxHQUFRLENBQUEsQ0FBQyxNQUFPLENBQUMscUJBRGpCLENBQUE7QUFBQSxZQUVBLEtBQUEsR0FBUSxDQUFBLENBQUMsTUFBTyxDQUFDLFVBQVAsQ0FBa0IsT0FBbEIsQ0FGVixDQUFBO0FBQUEsWUFHQSxLQUFBLEdBQVEsQ0FBQSxDQUFDLE1BQU8sQ0FBQyxVQUFQLENBQWtCLG9CQUFsQixDQUhWLENBQUE7QUFJQSxtQkFBTyxLQUFBLElBQVMsQ0FBQyxLQUFBLElBQVMsS0FBVixDQUFoQixDQUxGO1dBQUEsY0FBQTtBQU9FLFlBREksVUFDSixDQUFBO0FBQUEsbUJBQU8sS0FBUCxDQVBGO1dBQUE7QUFRQSxpQkFBTyxLQUFQLENBVGlCO1FBQUEsQ0EvRG5CLENBQUE7QUFBQSxRQTJFQSxTQUFBLEdBQVksU0FBQSxHQUFBO0FBQ1YsVUFBQSxTQUFBLEdBQVksR0FBQSxDQUFBLEtBQVMsQ0FBQyxLQUF0QixDQUFBO0FBQUEsVUFDQSxTQUFBLEdBQVksR0FBQSxDQUFBLEtBQVMsQ0FBQyxLQUR0QixDQURVO1FBQUEsQ0EzRVosQ0FBQTtBQUFBLFFBaUZBLFVBQUEsR0FBYSxTQUFBLEdBQUE7QUFDWCxVQUFBLFVBQUEsR0FBYSxHQUFBLENBQUEsS0FBUyxDQUFDLGlCQUF2QixDQUFBO0FBQUEsVUFDQSxVQUFBLEdBQWEsR0FBQSxDQUFBLEtBQVMsQ0FBQyxpQkFEdkIsQ0FBQTtBQUFBLFVBRUEsU0FBUyxDQUFDLEdBQVYsQ0FBYyxVQUFkLENBRkEsQ0FBQTtBQUFBLFVBR0EsU0FBUyxDQUFDLEdBQVYsQ0FBYyxVQUFkLENBSEEsQ0FBQTtBQUFBLFVBSUEsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFwQixHQUF3QixHQUp4QixDQUFBO0FBQUEsVUFLQSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQXBCLEdBQXdCLEdBTHhCLENBRFc7UUFBQSxDQWpGYixDQUFBO0FBQUEsUUEyRkEsWUFBQSxHQUFlLFNBQUEsR0FBQTtBQUNiLFVBQUEsSUFBRyxnQkFBQSxDQUFBLENBQUg7QUFDRSxZQUFBLFlBQUEsR0FBbUIsSUFBQSxLQUFLLENBQUMsYUFBTixDQUFvQjtBQUFBLGNBQUMsS0FBQSxFQUFPLElBQVI7QUFBQSxjQUN2QyxZQUFBLEVBQWMsSUFEeUI7YUFBcEIsQ0FBbkIsQ0FBQTtBQUFBLFlBRUEsWUFBQSxHQUFtQixJQUFBLEtBQUssQ0FBQyxhQUFOLENBQW9CO0FBQUEsY0FBQyxLQUFBLEVBQU8sSUFBUjtBQUFBLGNBQ3ZDLFlBQUEsRUFBYyxJQUR5QjthQUFwQixDQUZuQixDQURGO1dBQUEsTUFBQTtBQU1FLFlBQUEsWUFBQSxHQUFtQixJQUFBLEtBQUssQ0FBQyxjQUFOLENBQXFCO0FBQUEsY0FBQyxLQUFBLEVBQU8sSUFBUjtBQUFBLGNBQ3hDLFlBQUEsRUFBYyxJQUQwQjthQUFyQixDQUFuQixDQUFBO0FBQUEsWUFFQSxZQUFBLEdBQW1CLElBQUEsS0FBSyxDQUFDLGNBQU4sQ0FBcUI7QUFBQSxjQUFDLEtBQUEsRUFBTyxJQUFSO0FBQUEsY0FDeEMsWUFBQSxFQUFjLElBRDBCO2FBQXJCLENBRm5CLENBTkY7V0FBQTtBQUFBLFVBVUEsWUFBWSxDQUFDLGFBQWIsQ0FBMkIsQ0FBM0IsRUFBOEIsQ0FBOUIsQ0FWQSxDQUFBO0FBQUEsVUFXQSxZQUFZLENBQUMsYUFBYixDQUEyQixDQUEzQixFQUE4QixDQUE5QixDQVhBLENBQUE7QUFBQSxVQVlBLE9BQU8sQ0FBQyxNQUFSLENBQWUsWUFBWSxDQUFDLFVBQTVCLENBWkEsQ0FBQTtBQUFBLFVBYUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxZQUFZLENBQUMsVUFBNUIsQ0FiQSxDQUFBO0FBQUEsVUFjQSxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxRQUE5QixHQUF5QyxVQWR6QyxDQUFBO0FBQUEsVUFlQSxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxRQUE5QixHQUF5QyxVQWZ6QyxDQURhO1FBQUEsQ0EzRmYsQ0FBQTtBQUFBLFFBK0dBLFVBQUEsR0FBYSxTQUFBLEdBQUE7QUFDWCxVQUFBLFNBQUEsR0FBZ0IsSUFBQSxLQUFLLENBQUMsVUFBTixDQUFpQixRQUFqQixFQUEyQixDQUEzQixFQUE4QixJQUE5QixDQUFoQixDQUFBO0FBQUEsVUFDQSxTQUFTLENBQUMsUUFBUSxDQUFDLENBQW5CLEdBQXVCLENBRHZCLENBQUE7QUFBQSxVQUVBLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBbkIsR0FBdUIsQ0FGdkIsQ0FBQTtBQUFBLFVBR0EsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFuQixHQUF1QixHQUh2QixDQUFBO0FBQUEsVUFJQSxTQUFTLENBQUMsR0FBVixDQUFjLFNBQWQsQ0FKQSxDQUFBO0FBQUEsVUFLQSxTQUFBLEdBQWdCLElBQUEsS0FBSyxDQUFDLFVBQU4sQ0FBaUIsUUFBakIsRUFBMkIsQ0FBM0IsRUFBOEIsSUFBOUIsQ0FMaEIsQ0FBQTtBQUFBLFVBTUEsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFuQixHQUF1QixDQU52QixDQUFBO0FBQUEsVUFPQSxTQUFTLENBQUMsUUFBUSxDQUFDLENBQW5CLEdBQXVCLENBUHZCLENBQUE7QUFBQSxVQVFBLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBbkIsR0FBdUIsR0FSdkIsQ0FBQTtBQUFBLFVBU0EsU0FBUyxDQUFDLEdBQVYsQ0FBYyxTQUFkLENBVEEsQ0FEVztRQUFBLENBL0diLENBQUE7QUFBQSxRQTZIQSxVQUFBLEdBQWEsU0FBQSxHQUFBO0FBRVgsY0FBQSxXQUFBO0FBQUEsVUFBQSxNQUFBLEdBQWEsSUFBQSxLQUFLLENBQUMsSUFBTixDQUNQLElBQUEsS0FBSyxDQUFDLGNBQU4sQ0FBcUIsR0FBckIsRUFBMEIsQ0FBMUIsRUFBNkIsQ0FBN0IsQ0FETyxFQUVQLElBQUEsS0FBSyxDQUFDLG1CQUFOLENBQTBCO0FBQUEsWUFDNUIsU0FBQSxFQUFXLElBRGlCO0FBQUEsWUFFNUIsa0JBQUEsRUFBb0IsQ0FGUTtBQUFBLFlBRzVCLEtBQUEsRUFBTyxLQUhxQjtBQUFBLFlBSTVCLFFBQUEsRUFBVSxRQUprQjtXQUExQixDQUZPLENBQWIsQ0FBQTtBQUFBLFVBU0EsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFoQixHQUFvQixDQVRwQixDQUFBO0FBQUEsVUFVQSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQWhCLEdBQW9CLENBVnBCLENBQUE7QUFBQSxVQVdBLFNBQVMsQ0FBQyxHQUFWLENBQWMsTUFBZCxDQVhBLENBQUE7QUFBQSxVQWNBLGFBQUEsR0FBZ0IsR0FBQSxDQUFBLEtBQVMsQ0FBQyxRQWQxQixDQUFBO0FBQUEsVUFlQSxhQUFhLENBQUMsUUFBUSxDQUFDLENBQXZCLEdBQTJCLEVBZjNCLENBQUE7QUFnQkEsZUFBUyx3R0FBVCxHQUFBO0FBQ0UsWUFBQSxJQUFBLEdBQVcsSUFBQSxLQUFLLENBQUMsSUFBTixDQUNMLElBQUEsS0FBSyxDQUFDLFdBQU4sQ0FBa0IsU0FBUyxDQUFDLENBQTVCLEVBQStCLFNBQVMsQ0FBQyxDQUF6QyxFQUE0QyxTQUFTLENBQUMsQ0FBdEQsQ0FESyxFQUVMLElBQUEsS0FBSyxDQUFDLG1CQUFOLENBQTBCO0FBQUEsY0FDNUIsS0FBQSxFQUFPLEtBRHFCO0FBQUEsY0FFNUIsUUFBQSxFQUFVLFFBRmtCO2FBQTFCLENBRkssQ0FBWCxDQUFBO0FBQUEsWUFPQSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQWQsR0FBa0IsQ0FBQyxJQUFJLENBQUMsR0FBTCxDQUFTLElBQUksQ0FBQyxNQUFMLENBQUEsQ0FBVCxFQUF3QixDQUF4QixDQUFBLEdBQTZCLEdBQTlCLENBQUEsR0FBcUMsV0FBVyxDQUFDLENBQWpELEdBQ2hCLFdBQVcsQ0FBQyxDQVJkLENBQUE7QUFBQSxZQVNBLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBZCxHQUFrQixDQUFDLElBQUksQ0FBQyxHQUFMLENBQVMsSUFBSSxDQUFDLE1BQUwsQ0FBQSxDQUFULEVBQXdCLENBQXhCLENBQUEsR0FBNkIsR0FBOUIsQ0FBQSxHQUFxQyxXQUFXLENBQUMsQ0FBakQsR0FDaEIsV0FBVyxDQUFDLENBVmQsQ0FBQTtBQUFBLFlBV0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFkLEdBQWtCLENBQUMsSUFBSSxDQUFDLEdBQUwsQ0FBUyxJQUFJLENBQUMsTUFBTCxDQUFBLENBQVQsRUFBd0IsQ0FBeEIsQ0FBQSxHQUE2QixHQUE5QixDQUFBLEdBQXFDLFdBQVcsQ0FBQyxDQUFqRCxHQUNoQixXQUFXLENBQUMsQ0FaZCxDQUFBO0FBQUEsWUFhQSxhQUFhLENBQUMsR0FBZCxDQUFrQixJQUFsQixDQWJBLENBREY7QUFBQSxXQWhCQTtBQUFBLFVBK0JBLFNBQVMsQ0FBQyxHQUFWLENBQWMsYUFBZCxDQS9CQSxDQUZXO1FBQUEsQ0E3SGIsQ0FBQTtBQUFBLFFBa0tBLFVBQUEsR0FBYSxTQUFBLEdBQUE7QUFDWCxVQUFBLFVBQVUsQ0FBQyxHQUFYLENBQWUscUJBQWYsRUFBc0MsYUFBdEMsQ0FBQSxDQUFBO0FBQUEsVUFDQSxNQUFNLENBQUMsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsUUFBbEMsQ0FEQSxDQUFBO0FBRUEsVUFBQSxJQUFHLHFCQUFBLElBQXlCLE1BQXpCLElBQW1DLGNBQUEsSUFBa0IsTUFBeEQ7QUFDRSxZQUFBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixtQkFBeEIsRUFBNkMsbUJBQTdDLENBQUEsQ0FERjtXQUFBLE1BQUE7QUFHRSxZQUFBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixXQUF4QixFQUFxQyxXQUFyQyxDQUFBLENBSEY7V0FIVztRQUFBLENBbEtiLENBQUE7QUFBQSxRQTRLQSxVQUFBLEdBQWEsU0FBQyxFQUFELEdBQUE7QUFDWCxjQUFBLG9CQUFBO0FBQUEsVUFBQSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQWhCLElBQXFCLENBQUMsWUFBWSxDQUFDLENBQWIsR0FBaUIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFsQyxDQUFBLEdBQ3JCLE1BQU0sQ0FBQyxRQURjLEdBQ0gsRUFEbEIsQ0FBQTtBQUFBLFVBRUEsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFoQixJQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFiLEdBQWlCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBbEMsQ0FBQSxHQUNyQixNQUFNLENBQUMsUUFEYyxHQUNILEVBSGxCLENBQUE7QUFBQSxVQUlBLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBaEIsSUFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBYixHQUFpQixNQUFNLENBQUMsUUFBUSxDQUFDLENBQWxDLENBQUEsR0FDckIsTUFBTSxDQUFDLFFBRGMsR0FDSCxFQUxsQixDQUFBO0FBQUEsVUFPQSxvQkFBQSxHQUNFO0FBQUEsWUFBQSxDQUFBLEVBQUcsWUFBWSxDQUFDLENBQWIsR0FBaUIsb0JBQW9CLENBQUMsQ0FBekM7QUFBQSxZQUNBLENBQUEsRUFBRyxZQUFZLENBQUMsQ0FBYixHQUFpQixvQkFBb0IsQ0FBQyxDQUR6QztBQUFBLFlBRUEsQ0FBQSxFQUFHLFlBQVksQ0FBQyxDQUFiLEdBQWlCLG9CQUFvQixDQUFDLENBRnpDO1dBUkYsQ0FBQTtBQUFBLFVBWUEsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFoQixJQUFxQixDQUFDLG9CQUFvQixDQUFDLENBQXJCLEdBQXlCLFVBQXpCLEdBQ3RCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FESyxDQUFBLEdBQ0EsTUFBTSxDQUFDLFFBRFAsR0FDa0IsRUFidkMsQ0FBQTtBQUFBLFVBY0EsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFoQixJQUFxQixDQUFDLG9CQUFvQixDQUFDLENBQXJCLEdBQXlCLFVBQXpCLEdBQ3RCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FESyxDQUFBLEdBQ0EsTUFBTSxDQUFDLFFBRFAsR0FDa0IsRUFmdkMsQ0FBQTtBQUFBLFVBZ0JBLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBaEIsSUFBcUIsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFyQixHQUF5QixVQUF6QixHQUN0QixNQUFNLENBQUMsUUFBUSxDQUFDLENBREssQ0FBQSxHQUNBLE1BQU0sQ0FBQyxRQURQLEdBQ2tCLEVBakJ2QyxDQURXO1FBQUEsQ0E1S2IsQ0FBQTtBQUFBLFFBa01BLFVBQUEsR0FBYSxTQUFDLEVBQUQsR0FBQTtBQUNYLFVBQUEsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFwQixHQUF3QixVQUFVLENBQUMsUUFBUSxDQUFDLENBQTVDLENBQUE7QUFBQSxVQUNBLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBcEIsR0FBd0IsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUQ1QyxDQUFBO0FBQUEsVUFFQSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQXBCLEdBQXdCLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FGNUMsQ0FBQTtBQUFBLFVBSUEsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUF2QixHQUEyQixNQUFNLENBQUMsUUFBUSxDQUFDLENBSjNDLENBQUE7QUFBQSxVQUtBLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBdkIsR0FBMkIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUwzQyxDQUFBO0FBQUEsVUFNQSxhQUFhLENBQUMsUUFBUSxDQUFDLENBQXZCLEdBQTJCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FOM0MsQ0FBQTtBQUFBLFVBUUEsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUF2QixHQUEyQixNQUFNLENBQUMsUUFBUSxDQUFDLENBUjNDLENBQUE7QUFBQSxVQVNBLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBdkIsR0FBMkIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQVQzQyxDQUFBO0FBQUEsVUFVQSxhQUFhLENBQUMsUUFBUSxDQUFDLENBQXZCLEdBQTJCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FWM0MsQ0FEVztRQUFBLENBbE1iLENBQUE7QUFBQSxRQWlOQSxNQUFBLEdBQVMsU0FBQyxFQUFELEdBQUE7QUFDUCxVQUFBLFVBQUEsQ0FBVyxFQUFYLENBQUEsQ0FBQTtBQUFBLFVBQ0EsVUFBQSxDQUFXLEVBQVgsQ0FEQSxDQURPO1FBQUEsQ0FqTlQsQ0FBQTtBQUFBLFFBdU5BLE1BQUEsR0FBUyxTQUFBLEdBQUE7QUFDUCxVQUFBLFlBQVksQ0FBQyxNQUFiLENBQW9CLFNBQXBCLEVBQStCLFVBQS9CLENBQUEsQ0FBQTtBQUFBLFVBQ0EsWUFBWSxDQUFDLE1BQWIsQ0FBb0IsU0FBcEIsRUFBK0IsVUFBL0IsQ0FEQSxDQURPO1FBQUEsQ0F2TlQsQ0FBQTtBQUFBLFFBNk5BLE9BQUEsR0FBVSxTQUFBLEdBQUE7QUFDUixjQUFBLEdBQUE7QUFBQSxVQUFBLHFCQUFBLENBQXNCLE9BQXRCLENBQUEsQ0FBQTtBQUFBLFVBQ0EsR0FBQSxHQUFTLE1BQU0sQ0FBQyxXQUFWLEdBQTJCLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBbkIsQ0FBQSxDQUEzQixHQUNHLElBQUEsSUFBQSxDQUFBLENBQU0sQ0FBQyxPQUFQLENBQUEsQ0FGVCxDQUFBO0FBQUEsVUFHQSxNQUFBLENBQU8sR0FBQSxHQUFNLGFBQWIsQ0FIQSxDQUFBO0FBQUEsVUFJQSxNQUFBLENBQUEsQ0FKQSxDQUFBO0FBQUEsVUFLQSxhQUFBLEdBQWdCLEdBTGhCLENBRFE7UUFBQSxDQTdOVixDQUFBO0FBQUEsUUF1T0EsYUFBQSxHQUFnQixTQUFBLEdBQUE7QUFDZCxVQUFBLFlBQVksQ0FBQyxDQUFiLEdBQWlCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFoRSxDQUFBO0FBQUEsVUFDQSxZQUFZLENBQUMsQ0FBYixHQUFpQixNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FEaEUsQ0FBQTtBQUFBLFVBRUEsWUFBWSxDQUFDLENBQWIsR0FBaUIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBRmhFLENBQUE7QUFBQSxVQUlBLFlBQVksQ0FBQyxDQUFiLEdBQWlCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUpoRSxDQUFBO0FBQUEsVUFLQSxZQUFZLENBQUMsQ0FBYixHQUFpQixNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FMaEUsQ0FBQTtBQUFBLFVBTUEsWUFBWSxDQUFDLENBQWIsR0FBaUIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBTmhFLENBRGM7UUFBQSxDQXZPaEIsQ0FBQTtBQUFBLFFBa1BBLFdBQUEsR0FBYyxTQUFDLENBQUQsR0FBQTtBQUNaLFVBQUEsb0JBQW9CLENBQUMsQ0FBckIsR0FBeUIscUJBQXFCLENBQUMsQ0FBdEIsR0FBMEIsQ0FBQyxDQUFDLENBQUMsS0FBRixHQUNwRCxNQUFNLENBQUMsV0FBUCxHQUFxQixHQUQ4QixDQUExQixHQUNHLENBQUMsTUFBTSxDQUFDLFdBQVAsR0FBcUIsR0FBdEIsQ0FENUIsQ0FBQTtBQUFBLFVBRUEsb0JBQW9CLENBQUMsQ0FBckIsR0FBeUIscUJBQXFCLENBQUMsQ0FBdEIsR0FBMEIsQ0FBQyxDQUFDLENBQUMsS0FBRixHQUNwRCxNQUFNLENBQUMsVUFBUCxHQUFvQixHQUQrQixDQUExQixHQUNFLENBQUMsTUFBTSxDQUFDLFVBQVAsR0FBb0IsR0FBckIsQ0FIM0IsQ0FEWTtRQUFBLENBbFBkLENBQUE7QUFBQSxRQTBQQSxtQkFBQSxHQUFzQixTQUFDLENBQUQsR0FBQTtBQUNwQixVQUFBLG9CQUFvQixDQUFDLENBQXJCLEdBQXlCLHFCQUFxQixDQUFDLENBQXRCLEdBQTBCLENBQUMsQ0FBQyxJQUE1QixHQUFtQyxFQUE1RCxDQUFBO0FBQUEsVUFDQSxvQkFBb0IsQ0FBQyxDQUFyQixHQUF5QixxQkFBcUIsQ0FBQyxDQUF0QixHQUEwQixDQUFDLENBQUMsS0FBNUIsR0FBb0MsRUFEN0QsQ0FEb0I7UUFBQSxDQTFQdEIsQ0FBQTtBQUFBLFFBZ1FBLFFBQUEsR0FBVyxTQUFBLEdBQUE7QUFDVCxjQUFBLGFBQUE7QUFBQSxVQUFBLEtBQUEsR0FBUSxNQUFNLENBQUMsVUFBZixDQUFBO0FBQUEsVUFDQSxNQUFBLEdBQVMsTUFBTSxDQUFDLFdBRGhCLENBQUE7QUFBQSxVQUVBLFlBQVksQ0FBQyxPQUFiLENBQXFCLEtBQXJCLEVBQTRCLE1BQTVCLENBRkEsQ0FBQTtBQUFBLFVBR0EsWUFBWSxDQUFDLE9BQWIsQ0FBcUIsS0FBckIsRUFBNEIsTUFBNUIsQ0FIQSxDQUFBO0FBQUEsVUFJQSxVQUFVLENBQUMsTUFBWCxHQUFvQixLQUFBLEdBQVEsTUFKNUIsQ0FBQTtBQUFBLFVBS0EsVUFBVSxDQUFDLHNCQUFYLENBQUEsQ0FMQSxDQUFBO0FBQUEsVUFNQSxVQUFVLENBQUMsTUFBWCxHQUFvQixLQUFBLEdBQVEsTUFONUIsQ0FBQTtBQUFBLFVBT0EsVUFBVSxDQUFDLHNCQUFYLENBQUEsQ0FQQSxDQURTO1FBQUEsQ0FoUVgsQ0FBQTtBQUFBLFFBNlFBLElBQUEsQ0FBQSxDQTdRQSxDQUZJO01BQUEsQ0FGTjtNQUQ4QjtFQUFBLENBRGxDLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvYmFja2dyb3VuZC1lZmZlY3RzLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBkaXJlY3RpdmVcbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuZGlyZWN0aXZlOmJhY2tncm91bmRFZmZlY3RzXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgYmFja2dyb3VuZEVmZmVjdHNcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5kaXJlY3RpdmUgJ2JhY2tncm91bmRFZmZlY3RzJywgKCRyb290U2NvcGUsICRzdGF0ZSkgLT5cbiAgICByZXN0cmljdDogJ0EnXG4gICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9iYWNrZ3JvdW5kLWVmZmVjdHMuaHRtbCdcbiAgICBsaW5rOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSAtPlxuXG4gICAgICBDT0xPUiA9IDB4MDBmZmJkXG4gICAgICBEVVNUX1BJWEVMX1NJWkUgPSAzXG4gICAgICBEVVNUX1NJWkUgPVxuICAgICAgICB4OiAxXG4gICAgICAgIHk6IDFcbiAgICAgICAgejogMVxuICAgICAgRFVTVF9BTU9VTlQgPSAxMDAwXG4gICAgICBEVVNUX09GRlNFVCA9XG4gICAgICAgIHg6IDBcbiAgICAgICAgeTogMFxuICAgICAgICB6OiAtMjAwXG4gICAgICBEVVNUX1NQUkVBRCA9XG4gICAgICAgIHg6IDEwMDBcbiAgICAgICAgeTogMzAwXG4gICAgICAgIHo6IDYwMFxuXG4gICAgICBFQVNJTkcgPVxuICAgICAgICBwb3NpdGlvbjogMC4wMDE1XG4gICAgICAgIHJvdGF0aW9uOiAwLjAwMTVcblxuICAgICAgVE9fREVHUkVFUyA9IE1hdGguUEkgLyAxODBcblxuICAgICAgTUFYX1BBUkFMTEFYX1JPVEFUSU9OID1cbiAgICAgICAgeDogNVxuICAgICAgICB5OiA1XG5cbiAgICAgIHJlbmRlcmVyTWVzaCA9IG51bGxcbiAgICAgIHJlbmRlcmVyRHVzdCA9IG51bGxcbiAgICAgIGNhbWVyYU1lc2ggPSBudWxsXG4gICAgICBjYW1lcmFEdXN0ID0gbnVsbFxuICAgICAgc2NlbmVNZXNoID0gbnVsbFxuICAgICAgc2NlbmVEdXN0ID0gbnVsbFxuICAgICAgc3BoZXJlID0gbnVsbFxuICAgICAgZHVzdENvbnRhaW5lciA9IG51bGxcbiAgICAgIGxpZ2h0TWVzaCA9IG51bGxcbiAgICAgIGxpZ2h0RHVzdCA9IG51bGxcbiAgICAgIGxhc3RGcmFtZVRpbWUgPSAtMVxuICAgICAgZGVzdFBvc2l0aW9uID1cbiAgICAgICAgeDogMFxuICAgICAgICB5OiAwXG4gICAgICAgIHo6IDBcbiAgICAgIGRlc3RSb3RhdGlvbiA9XG4gICAgICAgIHg6IDBcbiAgICAgICAgeTogMFxuICAgICAgICB6OiAwXG4gICAgICBkZXN0UGFyYWxsYXhSb3RhdGlvbiA9XG4gICAgICAgIHg6IDBcbiAgICAgICAgeTogMFxuICAgICAgICB6OiAwXG5cblxuICAgICAgaW5pdCA9IC0+XG4gICAgICAgIGluaXRTY2VuZSgpXG4gICAgICAgIGluaXRDYW1lcmEoKVxuICAgICAgICBpbml0UmVuZGVyZXIoKVxuICAgICAgICBpbml0TGlnaHRzKClcbiAgICAgICAgaW5pdE1vZGVscygpXG4gICAgICAgIGJpbmRFdmVudHMoKVxuICAgICAgICBvblJlc2l6ZSgpXG4gICAgICAgIGFuaW1hdGUoKVxuICAgICAgICByZXR1cm5cblxuXG4gICAgICBpc1dlYkdMU3VwcG9ydGVkID0gLT5cbiAgICAgICAgdHJ5XG4gICAgICAgICAgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCAnY2FudmFzJ1xuICAgICAgICAgIHRlc3QxID0gISF3aW5kb3cuV2ViR0xSZW5kZXJpbmdDb250ZXh0XG4gICAgICAgICAgdGVzdDIgPSAhIWNhbnZhcy5nZXRDb250ZXh0KCd3ZWJnbCcpXG4gICAgICAgICAgdGVzdDMgPSAhIWNhbnZhcy5nZXRDb250ZXh0KCdleHBlcmltZW50YWwtd2ViZ2wnKVxuICAgICAgICAgIHJldHVybiB0ZXN0MSAmJiAodGVzdDIgfHwgdGVzdDMpXG4gICAgICAgIGNhdGNoIGVcbiAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgcmV0dXJuIGZhbHNlXG5cblxuICAgICAgaW5pdFNjZW5lID0gLT5cbiAgICAgICAgc2NlbmVNZXNoID0gbmV3IFRIUkVFLlNjZW5lXG4gICAgICAgIHNjZW5lRHVzdCA9IG5ldyBUSFJFRS5TY2VuZVxuICAgICAgICByZXR1cm5cblxuXG4gICAgICBpbml0Q2FtZXJhID0gLT5cbiAgICAgICAgY2FtZXJhTWVzaCA9IG5ldyBUSFJFRS5QZXJzcGVjdGl2ZUNhbWVyYVxuICAgICAgICBjYW1lcmFEdXN0ID0gbmV3IFRIUkVFLlBlcnNwZWN0aXZlQ2FtZXJhXG4gICAgICAgIHNjZW5lTWVzaC5hZGQgY2FtZXJhTWVzaFxuICAgICAgICBzY2VuZUR1c3QuYWRkIGNhbWVyYUR1c3RcbiAgICAgICAgY2FtZXJhTWVzaC5wb3NpdGlvbi56ID0gMzAwXG4gICAgICAgIGNhbWVyYUR1c3QucG9zaXRpb24ueiA9IDMwMFxuICAgICAgICByZXR1cm5cblxuXG4gICAgICBpbml0UmVuZGVyZXIgPSAtPlxuICAgICAgICBpZiBpc1dlYkdMU3VwcG9ydGVkKClcbiAgICAgICAgICByZW5kZXJlck1lc2ggPSBuZXcgVEhSRUUuV2ViR0xSZW5kZXJlciB7YWxwaGE6IHRydWUsXG4gICAgICAgICAgYW50aWFsaWFzaW5nOiB0cnVlfVxuICAgICAgICAgIHJlbmRlcmVyRHVzdCA9IG5ldyBUSFJFRS5XZWJHTFJlbmRlcmVyIHthbHBoYTogdHJ1ZSxcbiAgICAgICAgICBhbnRpYWxpYXNpbmc6IHRydWV9XG4gICAgICAgIGVsc2VcbiAgICAgICAgICByZW5kZXJlck1lc2ggPSBuZXcgVEhSRUUuQ2FudmFzUmVuZGVyZXIge2FscGhhOiB0cnVlLFxuICAgICAgICAgIGFudGlhbGlhc2luZzogdHJ1ZX1cbiAgICAgICAgICByZW5kZXJlckR1c3QgPSBuZXcgVEhSRUUuQ2FudmFzUmVuZGVyZXIge2FscGhhOiB0cnVlLFxuICAgICAgICAgIGFudGlhbGlhc2luZzogdHJ1ZX1cbiAgICAgICAgcmVuZGVyZXJNZXNoLnNldENsZWFyQ29sb3IgMCwgMFxuICAgICAgICByZW5kZXJlckR1c3Quc2V0Q2xlYXJDb2xvciAwLCAwXG4gICAgICAgIGVsZW1lbnQuYXBwZW5kIHJlbmRlcmVyRHVzdC5kb21FbGVtZW50XG4gICAgICAgIGVsZW1lbnQuYXBwZW5kIHJlbmRlcmVyTWVzaC5kb21FbGVtZW50XG4gICAgICAgIHJlbmRlcmVyTWVzaC5kb21FbGVtZW50LnN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJ1xuICAgICAgICByZW5kZXJlckR1c3QuZG9tRWxlbWVudC5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSdcbiAgICAgICAgcmV0dXJuXG5cblxuICAgICAgaW5pdExpZ2h0cyA9IC0+XG4gICAgICAgIGxpZ2h0TWVzaCA9IG5ldyBUSFJFRS5Qb2ludExpZ2h0IDB4ZmZmZmZmLCAxLCA1MDAwXG4gICAgICAgIGxpZ2h0TWVzaC5wb3NpdGlvbi54ID0gMFxuICAgICAgICBsaWdodE1lc2gucG9zaXRpb24ueSA9IDBcbiAgICAgICAgbGlnaHRNZXNoLnBvc2l0aW9uLnogPSA1MDBcbiAgICAgICAgc2NlbmVNZXNoLmFkZCBsaWdodE1lc2hcbiAgICAgICAgbGlnaHREdXN0ID0gbmV3IFRIUkVFLlBvaW50TGlnaHQgMHhmZmZmZmYsIDEsIDUwMDBcbiAgICAgICAgbGlnaHREdXN0LnBvc2l0aW9uLnggPSAwXG4gICAgICAgIGxpZ2h0RHVzdC5wb3NpdGlvbi55ID0gMFxuICAgICAgICBsaWdodER1c3QucG9zaXRpb24ueiA9IDUwMFxuICAgICAgICBzY2VuZUR1c3QuYWRkIGxpZ2h0RHVzdFxuICAgICAgICByZXR1cm5cblxuXG4gICAgICBpbml0TW9kZWxzID0gLT5cbiAgICAgICAgIyBNZXNoXG4gICAgICAgIHNwaGVyZSA9IG5ldyBUSFJFRS5NZXNoKFxuICAgICAgICAgIG5ldyBUSFJFRS5TcGhlcmVHZW9tZXRyeSgyMDAsIDQsIDQpLFxuICAgICAgICAgIG5ldyBUSFJFRS5NZXNoTGFtYmVydE1hdGVyaWFsKHtcbiAgICAgICAgICAgIHdpcmVmcmFtZTogdHJ1ZVxuICAgICAgICAgICAgd2lyZWZyYW1lTGluZXdpZHRoOiA1XG4gICAgICAgICAgICBjb2xvcjogQ09MT1JcbiAgICAgICAgICAgIGVtaXNzaXZlOiAweDFmM2IzYlxuICAgICAgICAgIH0pXG4gICAgICAgIClcbiAgICAgICAgc3BoZXJlLnBvc2l0aW9uLnggPSAwXG4gICAgICAgIHNwaGVyZS5wb3NpdGlvbi55ID0gMFxuICAgICAgICBzY2VuZU1lc2guYWRkIHNwaGVyZVxuXG4gICAgICAgICMgRHVzdFxuICAgICAgICBkdXN0Q29udGFpbmVyID0gbmV3IFRIUkVFLk9iamVjdDNEXG4gICAgICAgIGR1c3RDb250YWluZXIucm90YXRpb24ueCA9IDQ1XG4gICAgICAgIGZvciBpIGluIFswLi5EVVNUX0FNT1VOVF1cbiAgICAgICAgICBkdXN0ID0gbmV3IFRIUkVFLk1lc2goXG4gICAgICAgICAgICBuZXcgVEhSRUUuQm94R2VvbWV0cnkoRFVTVF9TSVpFLngsIERVU1RfU0laRS55LCBEVVNUX1NJWkUueiksXG4gICAgICAgICAgICBuZXcgVEhSRUUuTWVzaExhbWJlcnRNYXRlcmlhbCh7XG4gICAgICAgICAgICAgIGNvbG9yOiBDT0xPUlxuICAgICAgICAgICAgICBlbWlzc2l2ZTogMHgxZjNiM2JcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgKVxuICAgICAgICAgIGR1c3QucG9zaXRpb24ueCA9IChNYXRoLnBvdyhNYXRoLnJhbmRvbSgpLCAyKSAtIDAuNSkgKiBEVVNUX1NQUkVBRC54ICtcbiAgICAgICAgICAgIERVU1RfT0ZGU0VULnhcbiAgICAgICAgICBkdXN0LnBvc2l0aW9uLnkgPSAoTWF0aC5wb3coTWF0aC5yYW5kb20oKSwgMikgLSAwLjUpICogRFVTVF9TUFJFQUQueSArXG4gICAgICAgICAgICBEVVNUX09GRlNFVC55XG4gICAgICAgICAgZHVzdC5wb3NpdGlvbi56ID0gKE1hdGgucG93KE1hdGgucmFuZG9tKCksIDIpIC0gMC41KSAqIERVU1RfU1BSRUFELnogK1xuICAgICAgICAgICAgRFVTVF9PRkZTRVQuelxuICAgICAgICAgIGR1c3RDb250YWluZXIuYWRkIGR1c3RcbiAgICAgICAgc2NlbmVEdXN0LmFkZCBkdXN0Q29udGFpbmVyXG4gICAgICAgIHJldHVyblxuXG5cbiAgICAgIGJpbmRFdmVudHMgPSAtPlxuICAgICAgICAkcm9vdFNjb3BlLiRvbiAnJHN0YXRlQ2hhbmdlU3VjY2VzcycsIG9uU3RhdGVDaGFuZ2VcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgJ3Jlc2l6ZScsIG9uUmVzaXplXG4gICAgICAgIGlmICdvbmRldmljZW9yaWVudGF0aW9uJyBvZiB3aW5kb3cgJiYgJ29udG91Y2hzdGFydCcgb2Ygd2luZG93XG4gICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgJ2RldmljZW9yaWVudGF0aW9uJywgb25EZXZpY2VPcmllbnRhdGlvblxuICAgICAgICBlbHNlXG4gICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgJ21vdXNlbW92ZScsIG9uTW91c2VNb3ZlXG4gICAgICAgIHJldHVyblxuXG5cbiAgICAgIHVwZGF0ZU1lc2ggPSAoZHQpIC0+XG4gICAgICAgIHNwaGVyZS5wb3NpdGlvbi54ICs9IChkZXN0UG9zaXRpb24ueCAtIHNwaGVyZS5wb3NpdGlvbi54KSAqXG4gICAgICAgIEVBU0lORy5wb3NpdGlvbiAqIGR0XG4gICAgICAgIHNwaGVyZS5wb3NpdGlvbi55ICs9IChkZXN0UG9zaXRpb24ueSAtIHNwaGVyZS5wb3NpdGlvbi55KSAqXG4gICAgICAgIEVBU0lORy5wb3NpdGlvbiAqIGR0XG4gICAgICAgIHNwaGVyZS5wb3NpdGlvbi56ICs9IChkZXN0UG9zaXRpb24ueiAtIHNwaGVyZS5wb3NpdGlvbi56KSAqXG4gICAgICAgIEVBU0lORy5wb3NpdGlvbiAqIGR0XG5cbiAgICAgICAgZGVzdFJvdGF0aW9uQ29tcGxldGUgPVxuICAgICAgICAgIHg6IGRlc3RSb3RhdGlvbi54ICsgZGVzdFBhcmFsbGF4Um90YXRpb24ueFxuICAgICAgICAgIHk6IGRlc3RSb3RhdGlvbi55ICsgZGVzdFBhcmFsbGF4Um90YXRpb24ueVxuICAgICAgICAgIHo6IGRlc3RSb3RhdGlvbi56ICsgZGVzdFBhcmFsbGF4Um90YXRpb24uelxuXG4gICAgICAgIHNwaGVyZS5yb3RhdGlvbi54ICs9IChkZXN0Um90YXRpb25Db21wbGV0ZS54ICogVE9fREVHUkVFUyAtXG4gICAgICAgIHNwaGVyZS5yb3RhdGlvbi54KSAqIEVBU0lORy5yb3RhdGlvbiAqIGR0XG4gICAgICAgIHNwaGVyZS5yb3RhdGlvbi55ICs9IChkZXN0Um90YXRpb25Db21wbGV0ZS55ICogVE9fREVHUkVFUyAtXG4gICAgICAgIHNwaGVyZS5yb3RhdGlvbi55KSAqIEVBU0lORy5yb3RhdGlvbiAqIGR0XG4gICAgICAgIHNwaGVyZS5yb3RhdGlvbi56ICs9IChkZXN0Um90YXRpb25Db21wbGV0ZS56ICogVE9fREVHUkVFUyAtXG4gICAgICAgIHNwaGVyZS5yb3RhdGlvbi56KSAqIEVBU0lORy5yb3RhdGlvbiAqIGR0XG4gICAgICAgIHJldHVyblxuXG5cbiAgICAgIHVwZGF0ZUR1c3QgPSAoZHQpIC0+XG4gICAgICAgIGNhbWVyYUR1c3QucG9zaXRpb24ueCA9IGNhbWVyYU1lc2gucG9zaXRpb24ueFxuICAgICAgICBjYW1lcmFEdXN0LnBvc2l0aW9uLnkgPSBjYW1lcmFNZXNoLnBvc2l0aW9uLnlcbiAgICAgICAgY2FtZXJhRHVzdC5wb3NpdGlvbi56ID0gY2FtZXJhTWVzaC5wb3NpdGlvbi56XG5cbiAgICAgICAgZHVzdENvbnRhaW5lci5wb3NpdGlvbi54ID0gc3BoZXJlLnBvc2l0aW9uLnhcbiAgICAgICAgZHVzdENvbnRhaW5lci5wb3NpdGlvbi55ID0gc3BoZXJlLnBvc2l0aW9uLnlcbiAgICAgICAgZHVzdENvbnRhaW5lci5wb3NpdGlvbi56ID0gc3BoZXJlLnBvc2l0aW9uLnpcblxuICAgICAgICBkdXN0Q29udGFpbmVyLnJvdGF0aW9uLnggPSBzcGhlcmUucm90YXRpb24ueFxuICAgICAgICBkdXN0Q29udGFpbmVyLnJvdGF0aW9uLnkgPSBzcGhlcmUucm90YXRpb24ueVxuICAgICAgICBkdXN0Q29udGFpbmVyLnJvdGF0aW9uLnogPSBzcGhlcmUucm90YXRpb24uelxuICAgICAgICByZXR1cm5cblxuXG4gICAgICB1cGRhdGUgPSAoZHQpIC0+XG4gICAgICAgIHVwZGF0ZU1lc2ggZHRcbiAgICAgICAgdXBkYXRlRHVzdCBkdFxuICAgICAgICByZXR1cm5cblxuXG4gICAgICByZW5kZXIgPSAtPlxuICAgICAgICByZW5kZXJlckR1c3QucmVuZGVyIHNjZW5lRHVzdCwgY2FtZXJhRHVzdFxuICAgICAgICByZW5kZXJlck1lc2gucmVuZGVyIHNjZW5lTWVzaCwgY2FtZXJhTWVzaFxuICAgICAgICByZXR1cm5cblxuXG4gICAgICBhbmltYXRlID0gLT5cbiAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lIGFuaW1hdGVcbiAgICAgICAgbm93ID0gaWYgd2luZG93LnBlcmZvcm1hbmNlIHRoZW4gd2luZG93LnBlcmZvcm1hbmNlLm5vdygpXG4gICAgICAgIGVsc2UgbmV3IERhdGUoKS5nZXRUaW1lKClcbiAgICAgICAgdXBkYXRlKG5vdyAtIGxhc3RGcmFtZVRpbWUpXG4gICAgICAgIHJlbmRlcigpXG4gICAgICAgIGxhc3RGcmFtZVRpbWUgPSBub3dcbiAgICAgICAgcmV0dXJuXG5cblxuICAgICAgb25TdGF0ZUNoYW5nZSA9IC0+XG4gICAgICAgIGRlc3RQb3NpdGlvbi54ID0gJHN0YXRlLmN1cnJlbnQuZGF0YS5iYWNrZ3JvdW5kRWZmZWN0cy5wb3NpdGlvbi54XG4gICAgICAgIGRlc3RQb3NpdGlvbi55ID0gJHN0YXRlLmN1cnJlbnQuZGF0YS5iYWNrZ3JvdW5kRWZmZWN0cy5wb3NpdGlvbi55XG4gICAgICAgIGRlc3RQb3NpdGlvbi56ID0gJHN0YXRlLmN1cnJlbnQuZGF0YS5iYWNrZ3JvdW5kRWZmZWN0cy5wb3NpdGlvbi56XG5cbiAgICAgICAgZGVzdFJvdGF0aW9uLnggPSAkc3RhdGUuY3VycmVudC5kYXRhLmJhY2tncm91bmRFZmZlY3RzLnJvdGF0aW9uLnhcbiAgICAgICAgZGVzdFJvdGF0aW9uLnkgPSAkc3RhdGUuY3VycmVudC5kYXRhLmJhY2tncm91bmRFZmZlY3RzLnJvdGF0aW9uLnlcbiAgICAgICAgZGVzdFJvdGF0aW9uLnogPSAkc3RhdGUuY3VycmVudC5kYXRhLmJhY2tncm91bmRFZmZlY3RzLnJvdGF0aW9uLnpcbiAgICAgICAgcmV0dXJuXG5cblxuICAgICAgb25Nb3VzZU1vdmUgPSAoZSkgLT5cbiAgICAgICAgZGVzdFBhcmFsbGF4Um90YXRpb24ueCA9IE1BWF9QQVJBTExBWF9ST1RBVElPTi54ICogKGUucGFnZVkgLVxuICAgICAgICB3aW5kb3cuaW5uZXJIZWlnaHQgKiAwLjUpIC8gKHdpbmRvdy5pbm5lckhlaWdodCAqIDAuNSlcbiAgICAgICAgZGVzdFBhcmFsbGF4Um90YXRpb24ueSA9IE1BWF9QQVJBTExBWF9ST1RBVElPTi55ICogKGUucGFnZVggLVxuICAgICAgICB3aW5kb3cuaW5uZXJXaWR0aCAqIDAuNSkgLyAod2luZG93LmlubmVyV2lkdGggKiAwLjUpXG4gICAgICAgIHJldHVyblxuXG5cbiAgICAgIG9uRGV2aWNlT3JpZW50YXRpb24gPSAoZSkgLT5cbiAgICAgICAgZGVzdFBhcmFsbGF4Um90YXRpb24ueCA9IE1BWF9QQVJBTExBWF9ST1RBVElPTi54ICogZS5iZXRhIC8gMzBcbiAgICAgICAgZGVzdFBhcmFsbGF4Um90YXRpb24ueSA9IE1BWF9QQVJBTExBWF9ST1RBVElPTi55ICogZS5nYW1tYSAvIDMwXG4gICAgICAgIHJldHVyblxuXG5cbiAgICAgIG9uUmVzaXplID0gKCkgLT5cbiAgICAgICAgd2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aFxuICAgICAgICBoZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHRcbiAgICAgICAgcmVuZGVyZXJNZXNoLnNldFNpemUgd2lkdGgsIGhlaWdodFxuICAgICAgICByZW5kZXJlckR1c3Quc2V0U2l6ZSB3aWR0aCwgaGVpZ2h0XG4gICAgICAgIGNhbWVyYU1lc2guYXNwZWN0ID0gd2lkdGggLyBoZWlnaHRcbiAgICAgICAgY2FtZXJhTWVzaC51cGRhdGVQcm9qZWN0aW9uTWF0cml4KClcbiAgICAgICAgY2FtZXJhRHVzdC5hc3BlY3QgPSB3aWR0aCAvIGhlaWdodFxuICAgICAgICBjYW1lcmFEdXN0LnVwZGF0ZVByb2plY3Rpb25NYXRyaXgoKVxuXG4gICAgICAgIHJldHVyblxuXG5cbiAgICAgIGluaXQoKVxuXG4gICAgICByZXR1cm5cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:menu
    * @description
    * # menu
   */
  angular.module('vrchallengeioApp').directive('menu', [
    '$rootScope',
    '$state',
    '$timeout',
    function ($rootScope, $state, $timeout) {
      return {
        restrict: 'A',
        replace: true,
        templateUrl: 'partials/menu.html',
        link: function (scope, element, attrs) {
          var HIGHLIGHT_TIME, bindEvents, close, getMenuStates, highlight, init, initScope, isActiveState, onStateChange, open, toggle;
          HIGHLIGHT_TIME = 1000;
          init = function () {
            initScope();
            bindEvents();
          };
          initScope = function () {
            scope.isOpen = false;
            scope.menu = { states: getMenuStates() };
            scope.close = close;
            scope.toggle = toggle;
            scope.isActiveState = isActiveState;
          };
          getMenuStates = function () {
            var allStates, menuStates, state, _i, _len;
            allStates = $state.get();
            menuStates = [];
            for (_i = 0, _len = allStates.length; _i < _len; _i++) {
              state = allStates[_i];
              if (state.name !== '' && state.data.showInMenu) {
                menuStates.push(state);
              }
            }
            return menuStates;
          };
          bindEvents = function () {
            $rootScope.$on('$stateChangeStart', onStateChange);
            scope.$on('menu.highlight', highlight);
          };
          isActiveState = function (stateName) {
            return $state.includes(stateName);
          };
          highlight = function () {
            open();
          };
          open = function () {
            return scope.isOpen = true;
          };
          close = function () {
            return scope.isOpen = false;
          };
          toggle = function () {
            scope.isOpen = !scope.isOpen;
            return false;
          };
          onStateChange = function () {
            close();
            return false;
          };
          init();
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbWVudS5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7O0tBRkE7QUFBQSxFQVFBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDQSxDQUFDLFNBREQsQ0FDVyxNQURYLEVBQ21CLFNBQUMsVUFBRCxFQUFhLE1BQWIsRUFBcUIsUUFBckIsR0FBQTtXQUNqQjtBQUFBLE1BQUEsUUFBQSxFQUFVLEdBQVY7QUFBQSxNQUNBLE9BQUEsRUFBUyxJQURUO0FBQUEsTUFFQSxXQUFBLEVBQWEsb0JBRmI7QUFBQSxNQUdBLElBQUEsRUFBTSxTQUFDLEtBQUQsRUFBUSxPQUFSLEVBQWlCLEtBQWpCLEdBQUE7QUFFSixZQUFBLHdIQUFBO0FBQUEsUUFBQSxjQUFBLEdBQWlCLElBQWpCLENBQUE7QUFBQSxRQUVBLElBQUEsR0FBTyxTQUFBLEdBQUE7QUFDTCxVQUFBLFNBQUEsQ0FBQSxDQUFBLENBQUE7QUFBQSxVQUNBLFVBQUEsQ0FBQSxDQURBLENBREs7UUFBQSxDQUZQLENBQUE7QUFBQSxRQVFBLFNBQUEsR0FBWSxTQUFBLEdBQUE7QUFDVixVQUFBLEtBQUssQ0FBQyxNQUFOLEdBQWUsS0FBZixDQUFBO0FBQUEsVUFDQSxLQUFLLENBQUMsSUFBTixHQUNFO0FBQUEsWUFBQSxNQUFBLEVBQVEsYUFBQSxDQUFBLENBQVI7V0FGRixDQUFBO0FBQUEsVUFHQSxLQUFLLENBQUMsS0FBTixHQUFjLEtBSGQsQ0FBQTtBQUFBLFVBSUEsS0FBSyxDQUFDLE1BQU4sR0FBZSxNQUpmLENBQUE7QUFBQSxVQUtBLEtBQUssQ0FBQyxhQUFOLEdBQXNCLGFBTHRCLENBRFU7UUFBQSxDQVJaLENBQUE7QUFBQSxRQWtCQSxhQUFBLEdBQWdCLFNBQUEsR0FBQTtBQUNkLGNBQUEsc0NBQUE7QUFBQSxVQUFBLFNBQUEsR0FBWSxNQUFNLENBQUMsR0FBUCxDQUFBLENBQVosQ0FBQTtBQUFBLFVBQ0EsVUFBQSxHQUFhLEVBRGIsQ0FBQTtBQUVBLGVBQUEsZ0RBQUE7a0NBQUE7QUFDRSxZQUFBLElBQUcsS0FBSyxDQUFDLElBQU4sS0FBYyxFQUFkLElBQW9CLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBbEM7QUFDRSxjQUFBLFVBQVUsQ0FBQyxJQUFYLENBQWdCLEtBQWhCLENBQUEsQ0FERjthQURGO0FBQUEsV0FGQTtpQkFLQSxXQU5jO1FBQUEsQ0FsQmhCLENBQUE7QUFBQSxRQTJCQSxVQUFBLEdBQWEsU0FBQSxHQUFBO0FBQ1gsVUFBQSxVQUFVLENBQUMsR0FBWCxDQUFlLG1CQUFmLEVBQW9DLGFBQXBDLENBQUEsQ0FBQTtBQUFBLFVBQ0EsS0FBSyxDQUFDLEdBQU4sQ0FBVSxnQkFBVixFQUE0QixTQUE1QixDQURBLENBRFc7UUFBQSxDQTNCYixDQUFBO0FBQUEsUUFnQ0EsYUFBQSxHQUFnQixTQUFDLFNBQUQsR0FBQTtpQkFDZCxNQUFNLENBQUMsUUFBUCxDQUFnQixTQUFoQixFQURjO1FBQUEsQ0FoQ2hCLENBQUE7QUFBQSxRQW9DQSxTQUFBLEdBQVksU0FBQSxHQUFBO0FBQ1YsVUFBQSxJQUFBLENBQUEsQ0FBQSxDQURVO1FBQUEsQ0FwQ1osQ0FBQTtBQUFBLFFBMENBLElBQUEsR0FBTyxTQUFBLEdBQUE7aUJBQ0wsS0FBSyxDQUFDLE1BQU4sR0FBZSxLQURWO1FBQUEsQ0ExQ1AsQ0FBQTtBQUFBLFFBOENBLEtBQUEsR0FBUSxTQUFBLEdBQUE7aUJBQ04sS0FBSyxDQUFDLE1BQU4sR0FBZSxNQURUO1FBQUEsQ0E5Q1IsQ0FBQTtBQUFBLFFBa0RBLE1BQUEsR0FBUyxTQUFBLEdBQUE7QUFDUCxVQUFBLEtBQUssQ0FBQyxNQUFOLEdBQWUsQ0FBQSxLQUFNLENBQUMsTUFBdEIsQ0FBQTtpQkFDQSxNQUZPO1FBQUEsQ0FsRFQsQ0FBQTtBQUFBLFFBdURBLGFBQUEsR0FBZ0IsU0FBQSxHQUFBO0FBQ2QsVUFBQSxLQUFBLENBQUEsQ0FBQSxDQUFBO2lCQUNBLE1BRmM7UUFBQSxDQXZEaEIsQ0FBQTtBQUFBLFFBMkRBLElBQUEsQ0FBQSxDQTNEQSxDQUZJO01BQUEsQ0FITjtNQURpQjtFQUFBLENBRG5CLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvbWVudS5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2MgZGlyZWN0aXZlXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmRpcmVjdGl2ZTptZW51XG4gIyBAZGVzY3JpcHRpb25cbiAjICMgbWVudVxuIyMjXG5hbmd1bGFyLm1vZHVsZSAndnJjaGFsbGVuZ2Vpb0FwcCdcbi5kaXJlY3RpdmUgJ21lbnUnLCAoJHJvb3RTY29wZSwgJHN0YXRlLCAkdGltZW91dCkgLT5cbiAgcmVzdHJpY3Q6ICdBJ1xuICByZXBsYWNlOiB0cnVlXG4gIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvbWVudS5odG1sJ1xuICBsaW5rOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSAtPlxuXG4gICAgSElHSExJR0hUX1RJTUUgPSAxMDAwXG5cbiAgICBpbml0ID0gLT5cbiAgICAgIGluaXRTY29wZSgpXG4gICAgICBiaW5kRXZlbnRzKClcbiAgICAgIHJldHVyblxuXG5cbiAgICBpbml0U2NvcGUgPSAtPlxuICAgICAgc2NvcGUuaXNPcGVuID0gZmFsc2VcbiAgICAgIHNjb3BlLm1lbnUgPVxuICAgICAgICBzdGF0ZXM6IGdldE1lbnVTdGF0ZXMoKVxuICAgICAgc2NvcGUuY2xvc2UgPSBjbG9zZVxuICAgICAgc2NvcGUudG9nZ2xlID0gdG9nZ2xlXG4gICAgICBzY29wZS5pc0FjdGl2ZVN0YXRlID0gaXNBY3RpdmVTdGF0ZVxuICAgICAgcmV0dXJuXG5cblxuICAgIGdldE1lbnVTdGF0ZXMgPSAtPlxuICAgICAgYWxsU3RhdGVzID0gJHN0YXRlLmdldCgpXG4gICAgICBtZW51U3RhdGVzID0gW11cbiAgICAgIGZvciBzdGF0ZSBpbiBhbGxTdGF0ZXNcbiAgICAgICAgaWYgc3RhdGUubmFtZSAhPSAnJyAmJiBzdGF0ZS5kYXRhLnNob3dJbk1lbnVcbiAgICAgICAgICBtZW51U3RhdGVzLnB1c2ggc3RhdGVcbiAgICAgIG1lbnVTdGF0ZXNcblxuXG4gICAgYmluZEV2ZW50cyA9IC0+XG4gICAgICAkcm9vdFNjb3BlLiRvbiAnJHN0YXRlQ2hhbmdlU3RhcnQnLCBvblN0YXRlQ2hhbmdlXG4gICAgICBzY29wZS4kb24gJ21lbnUuaGlnaGxpZ2h0JywgaGlnaGxpZ2h0XG4gICAgICByZXR1cm5cblxuICAgIGlzQWN0aXZlU3RhdGUgPSAoc3RhdGVOYW1lKSAtPlxuICAgICAgJHN0YXRlLmluY2x1ZGVzIHN0YXRlTmFtZVxuXG5cbiAgICBoaWdobGlnaHQgPSAtPlxuICAgICAgb3BlbigpXG4gICAgICAjICR0aW1lb3V0IGNsb3NlLCBISUdITElHSFRfVElNRVxuICAgICAgcmV0dXJuXG5cblxuICAgIG9wZW4gPSAtPlxuICAgICAgc2NvcGUuaXNPcGVuID0gdHJ1ZVxuXG5cbiAgICBjbG9zZSA9IC0+XG4gICAgICBzY29wZS5pc09wZW4gPSBmYWxzZVxuXG5cbiAgICB0b2dnbGUgPSAtPlxuICAgICAgc2NvcGUuaXNPcGVuID0gIXNjb3BlLmlzT3BlblxuICAgICAgZmFsc2VcblxuXG4gICAgb25TdGF0ZUNoYW5nZSA9ICgpIC0+XG4gICAgICBjbG9zZSgpXG4gICAgICBmYWxzZVxuXG4gICAgaW5pdCgpXG5cbiAgICByZXR1cm5cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:AboutCtrl
    * @description
    * # AboutCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('AboutCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2Fib3V0LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLFVBREgsQ0FDYyxXQURkLEVBQzJCLFNBQUMsTUFBRCxHQUFBLENBRDNCLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2Fib3V0LmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBmdW5jdGlvblxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5jb250cm9sbGVyOkFib3V0Q3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIEFib3V0Q3RybFxuICMgQ29udHJvbGxlciBvZiB0aGUgdnJjaGFsbGVuZ2Vpb0FwcFxuIyMjXG5hbmd1bGFyLm1vZHVsZSAndnJjaGFsbGVuZ2Vpb0FwcCdcbiAgLmNvbnRyb2xsZXIgJ0Fib3V0Q3RybCcsICgkc2NvcGUpIC0+XG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ProgrammeCtrl
    * @description
    * # ProgrammeCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ProgrammeCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL3Byb2dyYW1tZS5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsRUFTQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxVQURILENBQ2MsZUFEZCxFQUMrQixTQUFDLE1BQUQsR0FBQSxDQUQvQixDQVRBLENBQUE7QUFBQSIsImZpbGUiOiJjb250cm9sbGVycy9wcm9ncmFtbWUuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZ1bmN0aW9uXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmNvbnRyb2xsZXI6UHJvZ3JhbW1lQ3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIFByb2dyYW1tZUN0cmxcbiAjIENvbnRyb2xsZXIgb2YgdGhlIHZyY2hhbGxlbmdlaW9BcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5jb250cm9sbGVyICdQcm9ncmFtbWVDdHJsJywgKCRzY29wZSkgLT5cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:OffPiotrkowskaCtrl
    * @description
    * # OffPiotrkowskaCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('OffPiotrkowskaCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL29mZi1waW90cmtvd3NrYS5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsRUFTQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxVQURILENBQ2Msb0JBRGQsRUFDb0MsU0FBQyxNQUFELEdBQUEsQ0FEcEMsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoiY29udHJvbGxlcnMvb2ZmLXBpb3Rya293c2thLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBmdW5jdGlvblxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5jb250cm9sbGVyOk9mZlBpb3Rya293c2thQ3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIE9mZlBpb3Rya293c2thQ3RybFxuICMgQ29udHJvbGxlciBvZiB0aGUgdnJjaGFsbGVuZ2Vpb0FwcFxuIyMjXG5hbmd1bGFyLm1vZHVsZSAndnJjaGFsbGVuZ2Vpb0FwcCdcbiAgLmNvbnRyb2xsZXIgJ09mZlBpb3Rya293c2thQ3RybCcsICgkc2NvcGUpIC0+XG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContactCtrl
    * @description
    * # ContactCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContactCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRhY3QuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsVUFESCxDQUNjLGFBRGQsRUFDNkIsU0FBQyxNQUFELEdBQUEsQ0FEN0IsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoiY29udHJvbGxlcnMvY29udGFjdC5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2MgZnVuY3Rpb25cbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuY29udHJvbGxlcjpDb250YWN0Q3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIENvbnRhY3RDdHJsXG4gIyBDb250cm9sbGVyIG9mIHRoZSB2cmNoYWxsZW5nZWlvQXBwXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuY29udHJvbGxlciAnQ29udGFjdEN0cmwnLCAoJHNjb3BlKSAtPlxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:AboutFestivalCtrl
    * @description
    * # AboutFestivalCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('AboutFestivalCtrl', [
    '$scope',
    function ($scope) {
      return $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2Fib3V0LWZlc3RpdmFsLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLFVBREgsQ0FDYyxtQkFEZCxFQUNtQyxTQUFDLE1BQUQsR0FBQTtXQUMvQixNQUFNLENBQUMsYUFBUCxHQUF1QixDQUNyQixtQkFEcUIsRUFFckIsV0FGcUIsRUFHckIsT0FIcUIsRUFEUTtFQUFBLENBRG5DLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2Fib3V0LWZlc3RpdmFsLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBmdW5jdGlvblxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5jb250cm9sbGVyOkFib3V0RmVzdGl2YWxDdHJsXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgQWJvdXRGZXN0aXZhbEN0cmxcbiAjIENvbnRyb2xsZXIgb2YgdGhlIHZyY2hhbGxlbmdlaW9BcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5jb250cm9sbGVyICdBYm91dEZlc3RpdmFsQ3RybCcsICgkc2NvcGUpIC0+XG4gICAgJHNjb3BlLmF3ZXNvbWVUaGluZ3MgPSBbXG4gICAgICAnSFRNTDUgQm9pbGVycGxhdGUnXG4gICAgICAnQW5ndWxhckpTJ1xuICAgICAgJ0thcm1hJ1xuICAgIF1cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:AboutUnit9Ctrl
    * @description
    * # AboutUnit9Ctrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('AboutUnit9Ctrl', [
    '$scope',
    function ($scope) {
      return $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2Fib3V0LXVuaXQ5LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLFVBREgsQ0FDYyxnQkFEZCxFQUNnQyxTQUFDLE1BQUQsR0FBQTtXQUM1QixNQUFNLENBQUMsYUFBUCxHQUF1QixDQUNyQixtQkFEcUIsRUFFckIsV0FGcUIsRUFHckIsT0FIcUIsRUFESztFQUFBLENBRGhDLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2Fib3V0LXVuaXQ5LmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBmdW5jdGlvblxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5jb250cm9sbGVyOkFib3V0VW5pdDlDdHJsXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgQWJvdXRVbml0OUN0cmxcbiAjIENvbnRyb2xsZXIgb2YgdGhlIHZyY2hhbGxlbmdlaW9BcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5jb250cm9sbGVyICdBYm91dFVuaXQ5Q3RybCcsICgkc2NvcGUpIC0+XG4gICAgJHNjb3BlLmF3ZXNvbWVUaGluZ3MgPSBbXG4gICAgICAnSFRNTDUgQm9pbGVycGxhdGUnXG4gICAgICAnQW5ndWxhckpTJ1xuICAgICAgJ0thcm1hJ1xuICAgIF1cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:AboutLocationCtrl
    * @description
    * # AboutLocationCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('AboutLocationCtrl', [
    '$scope',
    function ($scope) {
      return $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2Fib3V0LWxvY2F0aW9uLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLFVBREgsQ0FDYyxtQkFEZCxFQUNtQyxTQUFDLE1BQUQsR0FBQTtXQUMvQixNQUFNLENBQUMsYUFBUCxHQUF1QixDQUNyQixtQkFEcUIsRUFFckIsV0FGcUIsRUFHckIsT0FIcUIsRUFEUTtFQUFBLENBRG5DLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2Fib3V0LWxvY2F0aW9uLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBmdW5jdGlvblxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5jb250cm9sbGVyOkFib3V0TG9jYXRpb25DdHJsXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgQWJvdXRMb2NhdGlvbkN0cmxcbiAjIENvbnRyb2xsZXIgb2YgdGhlIHZyY2hhbGxlbmdlaW9BcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5jb250cm9sbGVyICdBYm91dExvY2F0aW9uQ3RybCcsICgkc2NvcGUpIC0+XG4gICAgJHNjb3BlLmF3ZXNvbWVUaGluZ3MgPSBbXG4gICAgICAnSFRNTDUgQm9pbGVycGxhdGUnXG4gICAgICAnQW5ndWxhckpTJ1xuICAgICAgJ0thcm1hJ1xuICAgIF1cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContestCtrl
    * @description
    * # ContestCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContestCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRlc3QuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsVUFESCxDQUNjLGFBRGQsRUFDNkIsU0FBQyxNQUFELEdBQUEsQ0FEN0IsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoiY29udHJvbGxlcnMvY29udGVzdC5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2MgZnVuY3Rpb25cbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuY29udHJvbGxlcjpDb250ZXN0Q3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIENvbnRlc3RDdHJsXG4gIyBDb250cm9sbGVyIG9mIHRoZSB2cmNoYWxsZW5nZWlvQXBwXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuY29udHJvbGxlciAnQ29udGVzdEN0cmwnLCAoJHNjb3BlKSAtPlxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContestRulesCtrl
    * @description
    * # ContestRulesCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContestRulesCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRlc3QtcnVsZXMuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsVUFESCxDQUNjLGtCQURkLEVBQ2tDLFNBQUMsTUFBRCxHQUFBLENBRGxDLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2NvbnRlc3QtcnVsZXMuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZ1bmN0aW9uXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmNvbnRyb2xsZXI6Q29udGVzdFJ1bGVzQ3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIENvbnRlc3RSdWxlc0N0cmxcbiAjIENvbnRyb2xsZXIgb2YgdGhlIHZyY2hhbGxlbmdlaW9BcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5jb250cm9sbGVyICdDb250ZXN0UnVsZXNDdHJsJywgKCRzY29wZSkgLT5cbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContestPrizesCtrl
    * @description
    * # ContestPrizesCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContestPrizesCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRlc3QtcHJpemVzLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLFVBREgsQ0FDYyxtQkFEZCxFQUNtQyxTQUFDLE1BQUQsR0FBQSxDQURuQyxDQVRBLENBQUE7QUFBQSIsImZpbGUiOiJjb250cm9sbGVycy9jb250ZXN0LXByaXplcy5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2MgZnVuY3Rpb25cbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuY29udHJvbGxlcjpDb250ZXN0UHJpemVzQ3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIENvbnRlc3RQcml6ZXNDdHJsXG4gIyBDb250cm9sbGVyIG9mIHRoZSB2cmNoYWxsZW5nZWlvQXBwXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuY29udHJvbGxlciAnQ29udGVzdFByaXplc0N0cmwnLCAoJHNjb3BlKSAtPlxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContestApplyCtrl
    * @description
    * # ContestApplyCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContestApplyCtrl', [
    '$scope',
    '$timeout',
    'Subscription',
    function ($scope, $timeout, Subscription) {
      var BLINK_TIMES, BLINK_TIME_OFF, BLINK_TIME_ON, blink, validate;
      BLINK_TIMES = 3;
      BLINK_TIME_ON = 200;
      BLINK_TIME_OFF = 200;
      $scope.user = {
        email: void 0,
        regulationsAccepted: false,
        invalidEmail: false,
        invalidAccept: false,
        formSending: false,
        formSent: false
      };
      $scope.init = function () {
        $scope.user.formSending = false;
        return $scope.user.formSent = false;
      };
      $scope.submit = function (e) {
        var promise;
        if ($scope.user.formSending) {
          return;
        }
        if (validate()) {
          $scope.user.formSending = true;
          promise = Subscription.make(Subscription.TYPE_CONTESTANT, $scope.user.email);
          promise.then(function () {
            return $scope.user.formSent = true;
          });
        }
      };
      blink = function (field, index) {
        if (index == null) {
          index = 1;
        }
        if (index > BLINK_TIMES) {
          return;
        }
        $scope.user[field] = true;
        $timeout(function () {
          return $scope.user[field] = false;
        }, BLINK_TIME_ON);
        $timeout(function () {
          return blink(field, ++index);
        }, BLINK_TIME_ON + BLINK_TIME_OFF);
      };
      return validate = function () {
        if (!$scope.user.email) {
          blink('invalidEmail');
        }
        if (!$scope.user.regulationsAccepted) {
          blink('invalidAccept');
        }
        return !!$scope.user.email && $scope.user.regulationsAccepted;
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRlc3QtYXBwbHkuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsVUFESCxDQUNjLGtCQURkLEVBQ2tDLFNBQUMsTUFBRCxFQUFTLFFBQVQsRUFBbUIsWUFBbkIsR0FBQTtBQUU5QixRQUFBLDJEQUFBO0FBQUEsSUFBQSxXQUFBLEdBQWMsQ0FBZCxDQUFBO0FBQUEsSUFDQSxhQUFBLEdBQWdCLEdBRGhCLENBQUE7QUFBQSxJQUVBLGNBQUEsR0FBaUIsR0FGakIsQ0FBQTtBQUFBLElBSUEsTUFBTSxDQUFDLElBQVAsR0FDRTtBQUFBLE1BQUEsS0FBQSxFQUFPLE1BQVA7QUFBQSxNQUNBLG1CQUFBLEVBQXFCLEtBRHJCO0FBQUEsTUFFQSxZQUFBLEVBQWMsS0FGZDtBQUFBLE1BR0EsYUFBQSxFQUFlLEtBSGY7QUFBQSxNQUlBLFdBQUEsRUFBYSxLQUpiO0FBQUEsTUFLQSxRQUFBLEVBQVUsS0FMVjtLQUxGLENBQUE7QUFBQSxJQWFBLE1BQU0sQ0FBQyxJQUFQLEdBQWMsU0FBQSxHQUFBO0FBQ1osTUFBQSxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVosR0FBMEIsS0FBMUIsQ0FBQTthQUNBLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBWixHQUF1QixNQUZYO0lBQUEsQ0FiZCxDQUFBO0FBQUEsSUFrQkEsTUFBTSxDQUFDLE1BQVAsR0FBZ0IsU0FBQyxDQUFELEdBQUE7QUFDZCxVQUFBLE9BQUE7QUFBQSxNQUFBLElBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFmO0FBQ0UsY0FBQSxDQURGO09BQUE7QUFHQSxNQUFBLElBQUcsUUFBQSxDQUFBLENBQUg7QUFDRSxRQUFBLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBWixHQUEwQixJQUExQixDQUFBO0FBQUEsUUFDQSxPQUFBLEdBQVUsWUFBWSxDQUFDLElBQWIsQ0FBa0IsWUFBWSxDQUFDLGVBQS9CLEVBQ1IsTUFBTSxDQUFDLElBQUksQ0FBQyxLQURKLENBRFYsQ0FBQTtBQUFBLFFBR0EsT0FBTyxDQUFDLElBQVIsQ0FBYSxTQUFBLEdBQUE7aUJBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFaLEdBQXVCLEtBRFo7UUFBQSxDQUFiLENBSEEsQ0FERjtPQUpjO0lBQUEsQ0FsQmhCLENBQUE7QUFBQSxJQStCQSxLQUFBLEdBQVEsU0FBQyxLQUFELEVBQVEsS0FBUixHQUFBOztRQUFRLFFBQVE7T0FDdEI7QUFBQSxNQUFBLElBQUcsS0FBQSxHQUFRLFdBQVg7QUFDRSxjQUFBLENBREY7T0FBQTtBQUFBLE1BRUEsTUFBTSxDQUFDLElBQUssQ0FBQSxLQUFBLENBQVosR0FBcUIsSUFGckIsQ0FBQTtBQUFBLE1BR0EsUUFBQSxDQUFTLFNBQUEsR0FBQTtlQUNQLE1BQU0sQ0FBQyxJQUFLLENBQUEsS0FBQSxDQUFaLEdBQXFCLE1BRGQ7TUFBQSxDQUFULEVBRUUsYUFGRixDQUhBLENBQUE7QUFBQSxNQU1BLFFBQUEsQ0FBUyxTQUFBLEdBQUE7ZUFDUCxLQUFBLENBQU0sS0FBTixFQUFhLEVBQUEsS0FBYixFQURPO01BQUEsQ0FBVCxFQUVFLGFBQUEsR0FBZ0IsY0FGbEIsQ0FOQSxDQURNO0lBQUEsQ0EvQlIsQ0FBQTtXQTRDQSxRQUFBLEdBQVcsU0FBQSxHQUFBO0FBQ1QsTUFBQSxJQUFHLENBQUEsTUFBTyxDQUFDLElBQUksQ0FBQyxLQUFoQjtBQUNFLFFBQUEsS0FBQSxDQUFNLGNBQU4sQ0FBQSxDQURGO09BQUE7QUFFQSxNQUFBLElBQUcsQ0FBQSxNQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFoQjtBQUNFLFFBQUEsS0FBQSxDQUFNLGVBQU4sQ0FBQSxDQURGO09BRkE7YUFJQSxDQUFBLENBQUMsTUFBTyxDQUFDLElBQUksQ0FBQyxLQUFkLElBQXVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBTDFCO0lBQUEsRUE5Q21CO0VBQUEsQ0FEbEMsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoiY29udHJvbGxlcnMvY29udGVzdC1hcHBseS5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0J1xuXG4jIyMqXG4gIyBAbmdkb2MgZnVuY3Rpb25cbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuY29udHJvbGxlcjpDb250ZXN0QXBwbHlDdHJsXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgQ29udGVzdEFwcGx5Q3RybFxuICMgQ29udHJvbGxlciBvZiB0aGUgdnJjaGFsbGVuZ2Vpb0FwcFxuIyMjXG5hbmd1bGFyLm1vZHVsZSAndnJjaGFsbGVuZ2Vpb0FwcCdcbiAgLmNvbnRyb2xsZXIgJ0NvbnRlc3RBcHBseUN0cmwnLCAoJHNjb3BlLCAkdGltZW91dCwgU3Vic2NyaXB0aW9uKSAtPlxuXG4gICAgQkxJTktfVElNRVMgPSAzXG4gICAgQkxJTktfVElNRV9PTiA9IDIwMFxuICAgIEJMSU5LX1RJTUVfT0ZGID0gMjAwXG5cbiAgICAkc2NvcGUudXNlciA9XG4gICAgICBlbWFpbDogdW5kZWZpbmVkXG4gICAgICByZWd1bGF0aW9uc0FjY2VwdGVkOiBmYWxzZVxuICAgICAgaW52YWxpZEVtYWlsOiBmYWxzZVxuICAgICAgaW52YWxpZEFjY2VwdDogZmFsc2VcbiAgICAgIGZvcm1TZW5kaW5nOiBmYWxzZVxuICAgICAgZm9ybVNlbnQ6IGZhbHNlXG5cblxuICAgICRzY29wZS5pbml0ID0gLT5cbiAgICAgICRzY29wZS51c2VyLmZvcm1TZW5kaW5nID0gZmFsc2VcbiAgICAgICRzY29wZS51c2VyLmZvcm1TZW50ID0gZmFsc2VcblxuXG4gICAgJHNjb3BlLnN1Ym1pdCA9IChlKSAtPlxuICAgICAgaWYgJHNjb3BlLnVzZXIuZm9ybVNlbmRpbmdcbiAgICAgICAgcmV0dXJuXG5cbiAgICAgIGlmIHZhbGlkYXRlKClcbiAgICAgICAgJHNjb3BlLnVzZXIuZm9ybVNlbmRpbmcgPSB0cnVlXG4gICAgICAgIHByb21pc2UgPSBTdWJzY3JpcHRpb24ubWFrZSBTdWJzY3JpcHRpb24uVFlQRV9DT05URVNUQU5ULFxuICAgICAgICAgICRzY29wZS51c2VyLmVtYWlsXG4gICAgICAgIHByb21pc2UudGhlbiAtPlxuICAgICAgICAgICRzY29wZS51c2VyLmZvcm1TZW50ID0gdHJ1ZVxuICAgICAgcmV0dXJuXG5cblxuICAgIGJsaW5rID0gKGZpZWxkLCBpbmRleCA9IDEpIC0+XG4gICAgICBpZiBpbmRleCA+IEJMSU5LX1RJTUVTXG4gICAgICAgIHJldHVyblxuICAgICAgJHNjb3BlLnVzZXJbZmllbGRdID0gdHJ1ZVxuICAgICAgJHRpbWVvdXQgLT5cbiAgICAgICAgJHNjb3BlLnVzZXJbZmllbGRdID0gZmFsc2VcbiAgICAgICwgQkxJTktfVElNRV9PTlxuICAgICAgJHRpbWVvdXQgLT5cbiAgICAgICAgYmxpbmsgZmllbGQsICsraW5kZXhcbiAgICAgICwgQkxJTktfVElNRV9PTiArIEJMSU5LX1RJTUVfT0ZGXG4gICAgICByZXR1cm5cblxuXG4gICAgdmFsaWRhdGUgPSAtPlxuICAgICAgaWYgISRzY29wZS51c2VyLmVtYWlsXG4gICAgICAgIGJsaW5rICdpbnZhbGlkRW1haWwnXG4gICAgICBpZiAhJHNjb3BlLnVzZXIucmVndWxhdGlvbnNBY2NlcHRlZFxuICAgICAgICBibGluayAnaW52YWxpZEFjY2VwdCdcbiAgICAgICEhJHNjb3BlLnVzZXIuZW1haWwgJiYgJHNjb3BlLnVzZXIucmVndWxhdGlvbnNBY2NlcHRlZFxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContestWorksCtrl
    * @description
    * # ContestWorksCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContestWorksCtrl', [
    '$scope',
    function ($scope) {
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRlc3Qtd29ya3MuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7S0FGQTtBQUFBLEVBU0EsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsVUFESCxDQUNjLGtCQURkLEVBQ2tDLFNBQUMsTUFBRCxHQUFBLENBRGxDLENBVEEsQ0FBQTtBQUFBIiwiZmlsZSI6ImNvbnRyb2xsZXJzL2NvbnRlc3Qtd29ya3MuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZ1bmN0aW9uXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmNvbnRyb2xsZXI6Q29udGVzdFdvcmtzQ3RybFxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIENvbnRlc3RXb3Jrc0N0cmxcbiAjIENvbnRyb2xsZXIgb2YgdGhlIHZyY2hhbGxlbmdlaW9BcHBcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5jb250cm9sbGVyICdDb250ZXN0V29ya3NDdHJsJywgKCRzY29wZSkgLT5cblxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:ContestJuryCtrl
    * @description
    * # ContestJuryCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('ContestJuryCtrl', [
    '$scope',
    function ($scope) {
      return $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL2NvbnRlc3QtanVyeS5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxFQUFBLFlBQUEsQ0FBQTtBQUVBO0FBQUE7Ozs7OztLQUZBO0FBQUEsRUFTQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxVQURILENBQ2MsaUJBRGQsRUFDaUMsU0FBQyxNQUFELEdBQUE7V0FDN0IsTUFBTSxDQUFDLGFBQVAsR0FBdUIsQ0FDckIsbUJBRHFCLEVBRXJCLFdBRnFCLEVBR3JCLE9BSHFCLEVBRE07RUFBQSxDQURqQyxDQVRBLENBQUE7QUFBQSIsImZpbGUiOiJjb250cm9sbGVycy9jb250ZXN0LWp1cnkuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZ1bmN0aW9uXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmNvbnRyb2xsZXI6Q29udGVzdEp1cnlDdHJsXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgQ29udGVzdEp1cnlDdHJsXG4gIyBDb250cm9sbGVyIG9mIHRoZSB2cmNoYWxsZW5nZWlvQXBwXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuY29udHJvbGxlciAnQ29udGVzdEp1cnlDdHJsJywgKCRzY29wZSkgLT5cbiAgICAkc2NvcGUuYXdlc29tZVRoaW5ncyA9IFtcbiAgICAgICdIVE1MNSBCb2lsZXJwbGF0ZSdcbiAgICAgICdBbmd1bGFySlMnXG4gICAgICAnS2FybWEnXG4gICAgXVxuIl19

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:vrcCheckbox
    * @description
    * # vrcCheckbox
   */
  angular.module('vrchallengeioApp').directive('vrcCheckbox', function () {
    return {
      restrict: 'A',
      templateUrl: 'partials/vrc-checkbox.html',
      scope: { checked: '=' },
      link: function (scope, element, attrs) {
        scope.toggle = function () {
          scope.checked = !scope.checked;
        };
      }
    };
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvdnJjLWNoZWNrYm94LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7S0FGQTtBQUFBLEVBUUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsU0FESCxDQUNhLGFBRGIsRUFDNEIsU0FBQSxHQUFBO1dBQ3hCO0FBQUEsTUFBQSxRQUFBLEVBQVUsR0FBVjtBQUFBLE1BQ0EsV0FBQSxFQUFhLDRCQURiO0FBQUEsTUFFQSxLQUFBLEVBQ0U7QUFBQSxRQUFBLE9BQUEsRUFBUyxHQUFUO09BSEY7QUFBQSxNQUlBLElBQUEsRUFBTSxTQUFDLEtBQUQsRUFBUSxPQUFSLEVBQWlCLEtBQWpCLEdBQUE7QUFFSixRQUFBLEtBQUssQ0FBQyxNQUFOLEdBQWUsU0FBQSxHQUFBO0FBQ2IsVUFBQSxLQUFLLENBQUMsT0FBTixHQUFnQixDQUFBLEtBQU0sQ0FBQyxPQUF2QixDQURhO1FBQUEsQ0FBZixDQUZJO01BQUEsQ0FKTjtNQUR3QjtFQUFBLENBRDVCLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvdnJjLWNoZWNrYm94LmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBkaXJlY3RpdmVcbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuZGlyZWN0aXZlOnZyY0NoZWNrYm94XG4gIyBAZGVzY3JpcHRpb25cbiAjICMgdnJjQ2hlY2tib3hcbiMjI1xuYW5ndWxhci5tb2R1bGUgJ3ZyY2hhbGxlbmdlaW9BcHAnXG4gIC5kaXJlY3RpdmUgJ3ZyY0NoZWNrYm94JywgLT5cbiAgICByZXN0cmljdDogJ0EnXG4gICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy92cmMtY2hlY2tib3guaHRtbCdcbiAgICBzY29wZTpcbiAgICAgIGNoZWNrZWQ6ICc9J1xuICAgIGxpbms6IChzY29wZSwgZWxlbWVudCwgYXR0cnMpIC0+XG5cbiAgICAgIHNjb3BlLnRvZ2dsZSA9IC0+XG4gICAgICAgIHNjb3BlLmNoZWNrZWQgPSAhc2NvcGUuY2hlY2tlZFxuICAgICAgICByZXR1cm5cblxuICAgICAgcmV0dXJuXG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc function
    * @name vrchallengeioApp.controller:PartnersCtrl
    * @description
    * # PartnersCtrl
    * Controller of the vrchallengeioApp
   */
  angular.module('vrchallengeioApp').controller('PartnersCtrl', [
    '$scope',
    function ($scope) {
      return $scope.partners = {
        honour: [
          '01_prezydent-miasta',
          '02_marszalek'
        ],
        media: [
          '01_gazeta',
          '02_tvp3',
          '09_eska',
          '08_mediarun',
          '04_pixel',
          '05_antyweb',
          '06_techplayer',
          '07_nowy-marketing',
          '03_mlodzi',
          '10_marketinglink'
        ],
        partners: [
          '01_politechnika',
          {
            name: '03_lodz-kreuje',
            url: 'http://www.lodz.pl/'
          },
          '04_larr',
          '05_gamedev',
          '06_off',
          '07_dom',
          '08_derendarz'
        ],
        sponsors: [
          '04_microsoft',
          '02_vrizzmo',
          '01_techland',
          '03_businesslink'
        ]
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXJzL3BhcnRuZXJzLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7O0tBRkE7QUFBQSxFQVNBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDQSxDQUFDLFVBREQsQ0FDWSxjQURaLEVBQzRCLFNBQUMsTUFBRCxHQUFBO1dBRTFCLE1BQU0sQ0FBQyxRQUFQLEdBRUU7QUFBQSxNQUFBLE1BQUEsRUFBUSxDQUNOLHFCQURNLEVBRU4sY0FGTSxDQUFSO0FBQUEsTUFLQSxLQUFBLEVBQU8sQ0FDTCxXQURLLEVBRUwsU0FGSyxFQUdMLFNBSEssRUFJTCxhQUpLLEVBS0wsVUFMSyxFQU1MLFlBTkssRUFPTCxlQVBLLEVBUUwsbUJBUkssRUFTTCxXQVRLLEVBVUwsa0JBVkssQ0FMUDtBQUFBLE1Ba0JBLFFBQUEsRUFBVTtRQUNSLGlCQURRLEVBRVI7QUFBQSxVQUNFLElBQUEsRUFBTSxnQkFEUjtBQUFBLFVBRUUsR0FBQSxFQUFLLHFCQUZQO1NBRlEsRUFNUixTQU5RLEVBT1IsWUFQUSxFQVFSLFFBUlEsRUFTUixRQVRRLEVBVVIsY0FWUTtPQWxCVjtBQUFBLE1BK0JBLFFBQUEsRUFBVSxDQUNSLGNBRFEsRUFFUixZQUZRLEVBR1IsYUFIUSxFQUlSLGlCQUpRLENBL0JWO01BSndCO0VBQUEsQ0FENUIsQ0FUQSxDQUFBO0FBQUEiLCJmaWxlIjoiY29udHJvbGxlcnMvcGFydG5lcnMuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZ1bmN0aW9uXG4gIyBAbmFtZSB2cmNoYWxsZW5nZWlvQXBwLmNvbnRyb2xsZXI6UGFydG5lcnNDdHJsXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgUGFydG5lcnNDdHJsXG4gIyBDb250cm9sbGVyIG9mIHRoZSB2cmNoYWxsZW5nZWlvQXBwXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuLmNvbnRyb2xsZXIgJ1BhcnRuZXJzQ3RybCcsICgkc2NvcGUpIC0+XG5cbiAgJHNjb3BlLnBhcnRuZXJzID1cblxuICAgIGhvbm91cjogW1xuICAgICAgJzAxX3ByZXp5ZGVudC1taWFzdGEnXG4gICAgICAnMDJfbWFyc3phbGVrJ1xuICAgIF1cblxuICAgIG1lZGlhOiBbXG4gICAgICAnMDFfZ2F6ZXRhJ1xuICAgICAgJzAyX3R2cDMnXG4gICAgICAnMDlfZXNrYSdcbiAgICAgICcwOF9tZWRpYXJ1bidcbiAgICAgICcwNF9waXhlbCdcbiAgICAgICcwNV9hbnR5d2ViJ1xuICAgICAgJzA2X3RlY2hwbGF5ZXInXG4gICAgICAnMDdfbm93eS1tYXJrZXRpbmcnXG4gICAgICAnMDNfbWxvZHppJ1xuICAgICAgJzEwX21hcmtldGluZ2xpbmsnXG4gICAgXVxuXG4gICAgcGFydG5lcnM6IFtcbiAgICAgICcwMV9wb2xpdGVjaG5pa2EnLFxuICAgICAge1xuICAgICAgICBuYW1lOiAnMDNfbG9kei1rcmV1amUnXG4gICAgICAgIHVybDogXCJodHRwOi8vd3d3LmxvZHoucGwvXCJcbiAgICAgIH0sXG4gICAgICAnMDRfbGFycicsXG4gICAgICAnMDVfZ2FtZWRldicsXG4gICAgICAnMDZfb2ZmJyxcbiAgICAgICcwN19kb20nLFxuICAgICAgJzA4X2RlcmVuZGFyeidcbiAgICBdXG5cbiAgICBzcG9uc29yczogW1xuICAgICAgJzA0X21pY3Jvc29mdCdcbiAgICAgICcwMl92cml6em1vJ1xuICAgICAgJzAxX3RlY2hsYW5kJ1xuICAgICAgJzAzX2J1c2luZXNzbGluaydcbiAgICBdXG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:vrcMessageBox
    * @description
    * # vrcMessageBox
   */
  angular.module('vrchallengeioApp').directive('vrcMessageBox', [
    '$location',
    function ($location) {
      return {
        restrict: 'A',
        templateUrl: 'partials/vrc-message-box.html',
        link: function (scope, element, attrs) {
          var c;
          c = window.location.search.indexOf('action=confirm-contestant') !== -1;
          scope.messageBox = { isOpen: c };
          scope.close = function () {
            console.log('closing');
            scope.messageBox.isOpen = false;
            return $location.search('action', null);
          };
        }
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvdnJjLW1lc3NhZ2UtYm94LmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7S0FGQTtBQUFBLEVBUUEsT0FBTyxDQUFDLE1BQVIsQ0FBZSxrQkFBZixDQUNFLENBQUMsU0FESCxDQUNhLGVBRGIsRUFDOEIsU0FBQyxTQUFELEdBQUE7V0FDMUI7QUFBQSxNQUFBLFFBQUEsRUFBVSxHQUFWO0FBQUEsTUFDQSxXQUFBLEVBQWEsK0JBRGI7QUFBQSxNQUVBLElBQUEsRUFBTSxTQUFDLEtBQUQsRUFBUSxPQUFSLEVBQWlCLEtBQWpCLEdBQUE7QUFFSixZQUFBLENBQUE7QUFBQSxRQUFBLENBQUEsR0FBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUF2QixDQUErQiwyQkFBL0IsQ0FBQSxLQUErRCxDQUFBLENBQW5FLENBQUE7QUFBQSxRQUVBLEtBQUssQ0FBQyxVQUFOLEdBQ0U7QUFBQSxVQUFBLE1BQUEsRUFBUSxDQUFSO1NBSEYsQ0FBQTtBQUFBLFFBS0EsS0FBSyxDQUFDLEtBQU4sR0FBYyxTQUFBLEdBQUE7QUFDWixVQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksU0FBWixDQUFBLENBQUE7QUFBQSxVQUNBLEtBQUssQ0FBQyxVQUFVLENBQUMsTUFBakIsR0FBMEIsS0FEMUIsQ0FBQTtpQkFFQSxTQUFTLENBQUMsTUFBVixDQUFpQixRQUFqQixFQUEyQixJQUEzQixFQUhZO1FBQUEsQ0FMZCxDQUZJO01BQUEsQ0FGTjtNQUQwQjtFQUFBLENBRDlCLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvdnJjLW1lc3NhZ2UtYm94LmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBkaXJlY3RpdmVcbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuZGlyZWN0aXZlOnZyY01lc3NhZ2VCb3hcbiAjIEBkZXNjcmlwdGlvblxuICMgIyB2cmNNZXNzYWdlQm94XG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuZGlyZWN0aXZlICd2cmNNZXNzYWdlQm94JywgKCRsb2NhdGlvbikgLT5cbiAgICByZXN0cmljdDogJ0EnXG4gICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy92cmMtbWVzc2FnZS1ib3guaHRtbCdcbiAgICBsaW5rOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSAtPlxuXG4gICAgICBjID0gd2luZG93LmxvY2F0aW9uLnNlYXJjaC5pbmRleE9mKCdhY3Rpb249Y29uZmlybS1jb250ZXN0YW50JykgIT0gLTFcblxuICAgICAgc2NvcGUubWVzc2FnZUJveCA9XG4gICAgICAgIGlzT3BlbjogY1xuXG4gICAgICBzY29wZS5jbG9zZSA9IC0+XG4gICAgICAgIGNvbnNvbGUubG9nICdjbG9zaW5nJ1xuICAgICAgICBzY29wZS5tZXNzYWdlQm94LmlzT3BlbiA9IGZhbHNlXG4gICAgICAgICRsb2NhdGlvbi5zZWFyY2ggJ2FjdGlvbicsIG51bGxcblxuICAgICAgcmV0dXJuXG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc directive
    * @name vrchallengeioApp.directive:gaEventClick
    * @description
    * # gaEventClick
   */
  angular.module('vrchallengeioApp').directive('gaEventClick', function () {
    return {
      restrict: 'A',
      scope: {
        'ga-category': '@',
        'ga-action': '@',
        'ga-label': '@',
        'ga-value': '@'
      },
      link: function (scope, element, attrs) {
        console.log('************ ga', scope['ga-category'], scope['ga-action'], scope['ga-label'], scope['ga-value']);
        element.$on('click', function () {
          return console.log('clicked!');
        });
      }
    };
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZ2EtZXZlbnQtY2xpY2suY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7OztLQUZBO0FBQUEsRUFRQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxTQURILENBQ2EsY0FEYixFQUM2QixTQUFBLEdBQUE7V0FDekI7QUFBQSxNQUFBLFFBQUEsRUFBVSxHQUFWO0FBQUEsTUFDQSxLQUFBLEVBQ0U7QUFBQSxRQUFBLGFBQUEsRUFBZSxHQUFmO0FBQUEsUUFDQSxXQUFBLEVBQWEsR0FEYjtBQUFBLFFBRUEsVUFBQSxFQUFZLEdBRlo7QUFBQSxRQUdBLFVBQUEsRUFBWSxHQUhaO09BRkY7QUFBQSxNQU1BLElBQUEsRUFBTSxTQUFDLEtBQUQsRUFBUSxPQUFSLEVBQWlCLEtBQWpCLEdBQUE7QUFFSixRQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksaUJBQVosRUFBK0IsS0FBTSxDQUFBLGFBQUEsQ0FBckMsRUFBcUQsS0FBTSxDQUFBLFdBQUEsQ0FBM0QsRUFDRSxLQUFNLENBQUEsVUFBQSxDQURSLEVBQ3FCLEtBQU0sQ0FBQSxVQUFBLENBRDNCLENBQUEsQ0FBQTtBQUFBLFFBR0EsT0FBTyxDQUFDLEdBQVIsQ0FBWSxPQUFaLEVBQXFCLFNBQUEsR0FBQTtpQkFDbkIsT0FBTyxDQUFDLEdBQVIsQ0FBWSxVQUFaLEVBRG1CO1FBQUEsQ0FBckIsQ0FIQSxDQUZJO01BQUEsQ0FOTjtNQUR5QjtFQUFBLENBRDdCLENBUkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImRpcmVjdGl2ZXMvZ2EtZXZlbnQtY2xpY2suanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGRpcmVjdGl2ZVxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5kaXJlY3RpdmU6Z2FFdmVudENsaWNrXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgZ2FFdmVudENsaWNrXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuZGlyZWN0aXZlICdnYUV2ZW50Q2xpY2snLCAtPlxuICAgIHJlc3RyaWN0OiAnQSdcbiAgICBzY29wZTpcbiAgICAgICdnYS1jYXRlZ29yeSc6ICdAJ1xuICAgICAgJ2dhLWFjdGlvbic6ICdAJ1xuICAgICAgJ2dhLWxhYmVsJzogJ0AnXG4gICAgICAnZ2EtdmFsdWUnOiAnQCdcbiAgICBsaW5rOiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSAtPlxuXG4gICAgICBjb25zb2xlLmxvZyAnKioqKioqKioqKioqIGdhJywgc2NvcGVbJ2dhLWNhdGVnb3J5J10sIHNjb3BlWydnYS1hY3Rpb24nXSxcbiAgICAgICAgc2NvcGVbJ2dhLWxhYmVsJ10sIHNjb3BlWydnYS12YWx1ZSddXG5cbiAgICAgIGVsZW1lbnQuJG9uICdjbGljaycsIC0+XG4gICAgICAgIGNvbnNvbGUubG9nICdjbGlja2VkISdcblxuICAgICAgcmV0dXJuXG4iXX0=

(function () {
  'use strict';
  /**
    * @ngdoc filter
    * @name vrchallengeioApp.filter:partnerClass
    * @function
    * @description
    * # partnerClass
    * Filter in the vrchallengeioApp.
   */
  angular.module('vrchallengeioApp').filter('partnerClass', function () {
    var GLOBAL_PREFIX;
    GLOBAL_PREFIX = '_partner_';
    return function (input, prefix) {
      if (prefix == null) {
        return '';
      }
      return '' + GLOBAL_PREFIX + prefix + input;
    };
  });
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlcnMvcGFydG5lcmNsYXNzLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLEVBQUEsWUFBQSxDQUFBO0FBRUE7QUFBQTs7Ozs7OztLQUZBO0FBQUEsRUFVQSxPQUFPLENBQUMsTUFBUixDQUFlLGtCQUFmLENBQ0UsQ0FBQyxNQURILENBQ1UsY0FEVixFQUMwQixTQUFBLEdBQUE7QUFFdEIsUUFBQSxhQUFBO0FBQUEsSUFBQSxhQUFBLEdBQWdCLFdBQWhCLENBQUE7V0FFQSxTQUFDLEtBQUQsRUFBUSxNQUFSLEdBQUE7QUFFRSxNQUFBLElBQUksY0FBSjtBQUNFLGVBQU8sRUFBUCxDQURGO09BQUE7QUFHQSxhQUFPLEVBQUEsR0FBRyxhQUFILEdBQW1CLE1BQW5CLEdBQTRCLEtBQW5DLENBTEY7SUFBQSxFQUpzQjtFQUFBLENBRDFCLENBVkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImZpbHRlcnMvcGFydG5lcmNsYXNzLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5cbiMjIypcbiAjIEBuZ2RvYyBmaWx0ZXJcbiAjIEBuYW1lIHZyY2hhbGxlbmdlaW9BcHAuZmlsdGVyOnBhcnRuZXJDbGFzc1xuICMgQGZ1bmN0aW9uXG4gIyBAZGVzY3JpcHRpb25cbiAjICMgcGFydG5lckNsYXNzXG4gIyBGaWx0ZXIgaW4gdGhlIHZyY2hhbGxlbmdlaW9BcHAuXG4jIyNcbmFuZ3VsYXIubW9kdWxlKCd2cmNoYWxsZW5nZWlvQXBwJylcbiAgLmZpbHRlciAncGFydG5lckNsYXNzJywgLT5cblxuICAgIEdMT0JBTF9QUkVGSVggPSBcIl9wYXJ0bmVyX1wiXG5cbiAgICAoaW5wdXQsIHByZWZpeCkgLT5cblxuICAgICAgaWYgIXByZWZpeD9cbiAgICAgICAgcmV0dXJuIFwiXCJcblxuICAgICAgcmV0dXJuIFwiI3tHTE9CQUxfUFJFRklYfSN7cHJlZml4fSN7aW5wdXR9XCJcbiJdfQ==

(function () {
  'use strict';
  /**
    * @ngdoc filter
    * @name vrchallengeioApp.filter:trust
    * @function
    * @description
    * # trust
    * Filter in the vrchallengeioApp.
   */
  angular.module('vrchallengeioApp').filter('trust', [
    '$sce',
    function ($sce) {
      return function (input) {
        return $sce.trustAsHtml(input);
      };
    }
  ]);
}.call(this));  //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlcnMvdHJ1c3QuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsRUFBQSxZQUFBLENBQUE7QUFFQTtBQUFBOzs7Ozs7O0tBRkE7QUFBQSxFQVVBLE9BQU8sQ0FBQyxNQUFSLENBQWUsa0JBQWYsQ0FDRSxDQUFDLE1BREgsQ0FDVSxPQURWLEVBQ21CLFNBQUMsSUFBRCxHQUFBO1dBQ2YsU0FBQyxLQUFELEdBQUE7YUFDRSxJQUFJLENBQUMsV0FBTCxDQUFpQixLQUFqQixFQURGO0lBQUEsRUFEZTtFQUFBLENBRG5CLENBVkEsQ0FBQTtBQUFBIiwiZmlsZSI6ImZpbHRlcnMvdHJ1c3QuanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8iLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCdcblxuIyMjKlxuICMgQG5nZG9jIGZpbHRlclxuICMgQG5hbWUgdnJjaGFsbGVuZ2Vpb0FwcC5maWx0ZXI6dHJ1c3RcbiAjIEBmdW5jdGlvblxuICMgQGRlc2NyaXB0aW9uXG4gIyAjIHRydXN0XG4gIyBGaWx0ZXIgaW4gdGhlIHZyY2hhbGxlbmdlaW9BcHAuXG4jIyNcbmFuZ3VsYXIubW9kdWxlICd2cmNoYWxsZW5nZWlvQXBwJ1xuICAuZmlsdGVyICd0cnVzdCcsICgkc2NlKSAtPlxuICAgIChpbnB1dCkgLT5cbiAgICAgICRzY2UudHJ1c3RBc0h0bWwgaW5wdXRcbiJdfQ==
